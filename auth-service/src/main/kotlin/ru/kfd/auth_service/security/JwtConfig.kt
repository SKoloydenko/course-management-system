package ru.kfd.auth_service.security

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration
data class JwtConfig(
    @Value("\${jwt.name}") val name: String,
    @Value("\${jwt.expiration}") val expiration: Int,
    @Value("\${jwt.secret}") val secret: String,
)
