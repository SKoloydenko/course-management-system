package ru.kfd.auth_service.security

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component
import ru.kfd.auth_service.clients.UsersServiceClient
import ru.kfd.auth_service.dto.requests.UserValidationRequestDTO
import ru.kfd.auth_service.dto.responses.UserValidationResponseDTO

@Component
class UsernameAndPasswordAuthenticationProvider
@Autowired
constructor(
    private val usersServiceClient: UsersServiceClient,
) : AuthenticationProvider {

    override fun authenticate(auth: Authentication): Authentication {
        val email = auth.principal.toString()
        val password = auth.credentials.toString()

        return validateCredentials(email, password)
    }

    override fun supports(auth: Class<*>): Boolean {
        return auth == UsernamePasswordAuthenticationToken::class.java
    }

    fun validateCredentials(email: String, password: String): Authentication {
        try {
            val user: UserValidationResponseDTO =
                usersServiceClient.validateCredentials(UserValidationRequestDTO(email, password))
            return UsernamePasswordAuthenticationToken(user.id, password)
        } catch (e: Exception) {
            throw BadCredentialsException("Invalid credentials")
        }
    }
}
