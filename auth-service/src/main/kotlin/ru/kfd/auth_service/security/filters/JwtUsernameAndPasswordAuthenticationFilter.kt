package ru.kfd.auth_service.security.filters

import com.fasterxml.jackson.databind.ObjectMapper
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import ru.kfd.auth_service.constants.UnauthorizedErrorType
import ru.kfd.auth_service.dto.requests.UserValidationRequestDTO
import ru.kfd.auth_service.dto.responses.ErrorResponseDTO
import ru.kfd.auth_service.security.JwtConfig
import ru.kfd.auth_service.security.UsernameAndPasswordAuthenticationProvider
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtUsernameAndPasswordAuthenticationFilter(
    private val authenticationProvider: UsernameAndPasswordAuthenticationProvider,
    private val jwtConfig: JwtConfig,
    private val objectMapper: ObjectMapper,
) : UsernamePasswordAuthenticationFilter() {

    init {
        setRequiresAuthenticationRequestMatcher(AntPathRequestMatcher("/api/auth/login", "POST"))
    }

    override fun attemptAuthentication(
        request: HttpServletRequest,
        response: HttpServletResponse
    ): Authentication {
        val userCredentials: UserValidationRequestDTO
        try {
            userCredentials =
                objectMapper.readValue(request.inputStream, UserValidationRequestDTO::class.java)
        } catch (e: Exception) {
            throw BadCredentialsException("Invalid credentials")
        }

        val authToken =
            UsernamePasswordAuthenticationToken(userCredentials.email, userCredentials.password)

        return authenticationProvider.authenticate(authToken)
    }

    override fun successfulAuthentication(
        request: HttpServletRequest,
        response: HttpServletResponse,
        chain: FilterChain,
        authResult: Authentication,
    ) {
        val now = System.currentTimeMillis()
        val token =
            Jwts.builder()
                .setSubject(authResult.name)
                .setIssuedAt(Date(now))
                .setExpiration(Date(now + jwtConfig.expiration))
                .signWith(SignatureAlgorithm.HS512, jwtConfig.secret)
                .compact()

        val cookie = Cookie(jwtConfig.name, token)
        cookie.path = "/"
        cookie.isHttpOnly = true
        response.addCookie(cookie)
    }

    override fun unsuccessfulAuthentication(
        request: HttpServletRequest,
        response: HttpServletResponse,
        ex: AuthenticationException
    ) {
        response.status = HttpStatus.UNAUTHORIZED.value()
        response.contentType = MediaType.APPLICATION_JSON.toString()

        val body =
            ErrorResponseDTO(
                HttpStatus.UNAUTHORIZED.value(),
                UnauthorizedErrorType.UNAUTHORIZED.name,
                ex.message
            )

        response.outputStream.println(objectMapper.writeValueAsString(body))
    }
}
