package ru.kfd.auth_service.security.config

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler
import ru.kfd.auth_service.security.JwtConfig
import ru.kfd.auth_service.security.UsernameAndPasswordAuthenticationProvider
import ru.kfd.auth_service.security.filters.JwtUsernameAndPasswordAuthenticationFilter

@EnableWebSecurity
class SecurityConfig
@Autowired
constructor(
    private val authenticationProvider: UsernameAndPasswordAuthenticationProvider,
    private val jwtConfig: JwtConfig,
    private val objectMapper: ObjectMapper,
) : WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity) {
        http.csrf()
            .disable()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .addFilter(
                JwtUsernameAndPasswordAuthenticationFilter(
                    authenticationProvider,
                    jwtConfig,
                    objectMapper
                )
            )
            .authorizeRequests()
            .antMatchers(HttpMethod.POST, "/api/auth/**")
            .permitAll()
            .anyRequest()
            .authenticated()
            .and()
            .logout()
            .logoutUrl("/api/auth/logout")
            .deleteCookies("jwt")
            .logoutSuccessHandler(HttpStatusReturningLogoutSuccessHandler(HttpStatus.OK))
    }
}
