package ru.kfd.auth_service.config

import org.springframework.boot.web.servlet.error.DefaultErrorAttributes
import org.springframework.cloud.client.loadbalancer.LoadBalanced
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.client.RestTemplate
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import java.nio.charset.StandardCharsets

@Configuration
class AuthServiceConfig : WebMvcConfigurer {

    @Bean @LoadBalanced fun restTemplate(): RestTemplate = RestTemplate()

    @Bean
    fun errorAttributes() =
        object : DefaultErrorAttributes() {
            override fun getErrorAttributes(
                webRequest: WebRequest,
                includeStackTrace: Boolean
            ): MutableMap<String, Any> {
                val errorAttributes = super.getErrorAttributes(webRequest, false)
                errorAttributes.remove("timestamp")
                errorAttributes.remove("error")
                errorAttributes.remove("path")
                return errorAttributes
            }
        }

    override fun configureMessageConverters(converters: MutableList<HttpMessageConverter<*>>) {
        converters.filterIsInstance<MappingJackson2HttpMessageConverter>().first().defaultCharset =
            StandardCharsets.UTF_8
    }
}
