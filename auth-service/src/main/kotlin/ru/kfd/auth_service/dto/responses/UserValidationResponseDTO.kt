package ru.kfd.auth_service.dto.responses

data class UserValidationResponseDTO(
    val id: Long,
)
