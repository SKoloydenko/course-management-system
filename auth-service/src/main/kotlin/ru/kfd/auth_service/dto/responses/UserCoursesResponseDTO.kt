package ru.kfd.auth_service.dto.responses

data class UserCoursesResponseDTO(
    val appliedCourses: Set<Long>,
    val attendedCourses: Set<Long>,
    val supervisedCourses: Set<Long>,
    val finishedCourses: Set<Long>,
)
