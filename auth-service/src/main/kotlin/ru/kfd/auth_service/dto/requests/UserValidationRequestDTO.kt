package ru.kfd.auth_service.dto.requests

import javax.validation.constraints.Email
import javax.validation.constraints.Size

data class UserValidationRequestDTO(
    @field:Email(
        regexp = "^[a-zA-Z0-9_!#\$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+\$",
        message = "Email is incorrect"
    )
    val email: String,
    @field:Size(min = 8, max = 64, message = "Password length must be between 8 and 64 symbols")
    val password: String,
)
