package ru.kfd.auth_service.dto.responses

data class UserResponseDTO(
    val email: String,
    val firstname: String,
    val lastname: String,
    val universityId: Long,
    val userCourses: UserCoursesResponseDTO,
)
