package ru.kfd.auth_service.dto.responses

data class ErrorResponseDTO(
    val status: Int,
    val type: String,
    val message: String?,
)
