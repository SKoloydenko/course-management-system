package ru.kfd.auth_service.clients

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.postForObject
import ru.kfd.auth_service.dto.requests.UserRegisterRequestDTO
import ru.kfd.auth_service.dto.requests.UserValidationRequestDTO
import ru.kfd.auth_service.dto.responses.UserResponseDTO
import ru.kfd.auth_service.dto.responses.UserValidationResponseDTO

@Component
class UsersServiceClient @Autowired constructor(private val restTemplate: RestTemplate) {

    fun registerUser(user: UserRegisterRequestDTO): UserResponseDTO =
        restTemplate.postForObject("http://USERS-SERVICE/api/users/", user)

    fun validateCredentials(userCredentials: UserValidationRequestDTO): UserValidationResponseDTO =
        restTemplate.postForObject("http://USERS-SERVICE/api/users/validation/", userCredentials)
}
