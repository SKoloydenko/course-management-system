package ru.kfd.auth_service.constants

enum class UnauthorizedErrorType {
    UNAUTHORIZED,
}
