package ru.kfd.auth_service.controllers

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import ru.kfd.auth_service.clients.UsersServiceClient
import ru.kfd.auth_service.dto.requests.UserRegisterRequestDTO

@RestController
@RequestMapping("/api/auth")
class AuthController
@Autowired
constructor(
    private val usersServiceClient: UsersServiceClient,
) {

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    fun registerUser(@RequestBody user: UserRegisterRequestDTO) =
        usersServiceClient.registerUser(user)
}
