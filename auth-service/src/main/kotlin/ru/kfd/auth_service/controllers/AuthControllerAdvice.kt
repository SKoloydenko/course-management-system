package ru.kfd.auth_service.controllers

import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.client.HttpStatusCodeException

@ControllerAdvice
class AuthControllerAdvice {

    @ExceptionHandler(HttpStatusCodeException::class)
    fun handleHttpStatusCodeException(ex: HttpStatusCodeException): ResponseEntity<Any> {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        return ResponseEntity.status(ex.statusCode).headers(headers).body(ex.responseBodyAsString)
    }
}
