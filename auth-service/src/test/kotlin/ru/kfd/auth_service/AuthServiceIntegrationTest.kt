package ru.kfd.auth_service

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.post
import ru.kfd.auth_service.clients.UsersServiceClient
import ru.kfd.auth_service.dto.requests.UserRegisterRequestDTO
import ru.kfd.auth_service.dto.requests.UserValidationRequestDTO
import ru.kfd.auth_service.dto.responses.UserCoursesResponseDTO
import ru.kfd.auth_service.dto.responses.UserResponseDTO
import ru.kfd.auth_service.dto.responses.UserValidationResponseDTO

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AuthServiceIntegrationTest {

    @Autowired lateinit var mockMvc: MockMvc

    @MockkBean lateinit var usersServiceClient: UsersServiceClient

    companion object {
        const val userId = 1L
        const val universityId = 1L
    }

    @Nested
    inner class LoginUser {

        @Test
        fun `when correct credentials then login user`() {
            // given
            val request =
                UserValidationRequestDTO(
                    email = "ii@mail.ru",
                    password = "12345678",
                )
            every { usersServiceClient.validateCredentials(any()) } returns
                UserValidationResponseDTO(userId)
            // when
            mockMvc
                .post("/api/auth/login") {
                    contentType = MediaType.APPLICATION_JSON
                    content = jacksonObjectMapper().writeValueAsString(request)
                    accept = MediaType.APPLICATION_JSON
                }
                // then
                .andExpect { status { isOk } }
        }
    }

    @Nested
    inner class RegisterUser {

        @Test
        fun `when correct user then register user`() {
            // given
            val request =
                UserRegisterRequestDTO(
                    email = "ii@mail.ru",
                    password = "12345678",
                    firstname = "Иван",
                    lastname = "Иванов",
                    universityId = universityId,
                )
            val userCourses =
                UserCoursesResponseDTO(
                    appliedCourses = mutableSetOf(),
                    attendedCourses = mutableSetOf(),
                    supervisedCourses = mutableSetOf(),
                    finishedCourses = mutableSetOf(),
                )
            val response =
                UserResponseDTO(
                    email = "ii@mail.ru",
                    firstname = "Иван",
                    lastname = "Иванов",
                    university = "НИЯУ МИФИ",
                    userCourses = userCourses,
                )
            every { usersServiceClient.registerUser(any()) } returns response
            // when
            mockMvc
                .post("/api/auth/register") {
                    contentType = MediaType.APPLICATION_JSON
                    content = jacksonObjectMapper().writeValueAsString(request)
                    accept = MediaType.APPLICATION_JSON
                }
                // then
                .andExpect {
                    status { isCreated }
                    content { string(jacksonObjectMapper().writeValueAsString(response)) }
                }
        }
    }
}
