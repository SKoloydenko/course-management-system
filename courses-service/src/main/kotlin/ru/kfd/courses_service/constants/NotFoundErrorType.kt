package ru.kfd.courses_service.constants

enum class NotFoundErrorType {
    COURSE_DOES_NOT_EXIST,
    TAG_DOES_NOT_EXIST,
    ACTIVITY_DOES_NOT_EXIST,
    ACTIVITY_REPLY_DOES_NOT_EXIST,
    USER_COURSES_DO_NOT_EXIST,
    NOTE_DOES_NOT_EXIST,
    FILE_DOES_NOT_EXIST,
}
