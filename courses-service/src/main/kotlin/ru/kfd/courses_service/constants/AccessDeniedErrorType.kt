package ru.kfd.courses_service.constants

enum class AccessDeniedErrorType {
    APPLICANT_ONLY,
    SUPERVISOR_ONLY,
    STUDENT_ONLY,
    COURSE_MEMBER_ONLY,
    USER_ONLY,
    ACTIVITY_REPLY_OWNER_ONLY,
}
