package ru.kfd.courses_service.models

import java.time.ZonedDateTime
import javax.persistence.*

@Entity
@Table(name = "activities")
class Activity(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
    val type: ActivityType,
    var title: String,
    var description: String,
    val startingTime: ZonedDateTime,
    val finishingTime: ZonedDateTime,
    val maxGrade: Long,
    @OneToMany(cascade = [CascadeType.ALL], orphanRemoval = true)
    @JoinColumn(name = "activity_id")
    val replies: MutableSet<ActivityReply> = mutableSetOf(),
    @ManyToOne val course: Course,
) {

    fun isAvailable(): Boolean {
        val now = ZonedDateTime.now(finishingTime.zone)
        return now.isAfter(startingTime) && now.isBefore(finishingTime)
    }

    fun containsReply(studentId: Long) = replies.none { it.isOwner(studentId) }

    fun getReply(replyId: Long) = replies.find { it.id == replyId }

    fun addReply(activityReply: ActivityReply) = replies.add(activityReply)

    fun deleteReply(replyId: Long) = replies.removeIf { it.id == replyId }
}
