package ru.kfd.courses_service.models

import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name = "attachments")
class Attachment(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
    val name: String,
    val date: LocalDate,
)
