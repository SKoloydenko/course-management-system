package ru.kfd.courses_service.models

import java.time.DayOfWeek
import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name = "courses")
class Course(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
    val title: String,
    val supervisor: Long,
    var status: CourseStatus,
    val startingDate: LocalDate,
    val finishingDate: LocalDate,
    @ElementCollection val lessonsDays: MutableSet<DayOfWeek>,
    val open: Boolean,
    @OneToOne @JoinColumn(name = "tag_id") val tag: Tag,
    val description: String,
    @ElementCollection val applicants: MutableSet<Long> = mutableSetOf(),
    @ElementCollection val students: MutableSet<Long> = mutableSetOf(),
    @OneToMany(cascade = [CascadeType.ALL], orphanRemoval = true, mappedBy = "course")
    val notes: MutableSet<Note> = mutableSetOf(),
    @OneToMany
    @JoinTable(
        name = "course_attachments",
        joinColumns = [JoinColumn(name = "course_id")],
        inverseJoinColumns = [JoinColumn(name = "attachment_id")]
    )
    val attachments: MutableSet<Attachment> = mutableSetOf(),
    @OneToMany(cascade = [CascadeType.ALL], orphanRemoval = true, mappedBy = "course")
    val activities: MutableSet<Activity> = mutableSetOf(),
) {

    fun isSupervisor(userId: Long) = supervisor == userId

    fun isApplicant(userId: Long) = applicants.contains(userId)

    fun isStudent(userId: Long) = students.contains(userId)

    fun isOpen() = open

    fun shouldStart() = LocalDate.now() == startingDate

    fun shouldFinish() = LocalDate.now() == finishingDate

    fun isCreated() = status == CourseStatus.CREATED

    fun isActive() = status == CourseStatus.ACTIVE

    fun isFinished() = status == CourseStatus.FINISHED

    fun start() {
        status = CourseStatus.ACTIVE
    }

    fun finish() {
        status = CourseStatus.FINISHED
    }

    fun addApplicant(userId: Long) = applicants.add(userId)

    fun addStudent(userId: Long) = students.add(userId)

    fun deleteApplication(userId: Long) = applicants.remove(userId)

    fun deleteStudent(userId: Long) = students.remove(userId)

    fun addNote(note: Note) = notes.add(note)

    fun deleteNote(noteId: Long) = notes.removeIf { it.id == noteId }

    fun addAttachment(attachment: Attachment) = attachments.add(attachment)

    fun deleteAttachment(resourceId: Long) = attachments.removeIf { it.id == resourceId }

    fun getActivity(activityId: Long) = activities.find { it.id == activityId }

    fun addActivity(activity: Activity) = activities.add(activity)

    fun deleteActivity(activityId: Long) = activities.removeIf { it.id == activityId }

    // fun initWorkDays() {
    //    val date = startingDate
    //    while (!finishingDate.isAfter(date)) {
    //        val dayOfWeek = date.dayOfWeek
    //        if (lessonsDays.contains(dayOfWeek))
    //            date.plusDays(1)
    //    }
    // }
}
