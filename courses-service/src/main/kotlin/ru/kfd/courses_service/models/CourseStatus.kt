package ru.kfd.courses_service.models

enum class CourseStatus {
    CREATED,
    ACTIVE,
    FINISHED,
}
