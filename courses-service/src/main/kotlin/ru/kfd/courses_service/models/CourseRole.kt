package ru.kfd.courses_service.models

enum class CourseRole {
    USER,
    APPLICANT,
    STUDENT,
    MEMBER,
    SUPERVISOR
}
