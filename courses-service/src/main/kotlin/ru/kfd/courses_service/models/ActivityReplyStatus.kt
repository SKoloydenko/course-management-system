package ru.kfd.courses_service.models

enum class ActivityReplyStatus {
    SENT,
    GRADED,
}
