package ru.kfd.courses_service.models

import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name = "notes")
class Note(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
    var text: String,
    val date: LocalDate,
    @ManyToOne val course: Course,
)
