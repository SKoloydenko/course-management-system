package ru.kfd.courses_service.models

import javax.persistence.*

@Entity
@Table(name = "tags")
class Tag(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
    @Column(unique = true) val title: String,
)
