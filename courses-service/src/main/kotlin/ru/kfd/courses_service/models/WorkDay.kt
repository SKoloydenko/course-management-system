package ru.kfd.courses_service.models

import java.time.LocalDate
import javax.persistence.*

@Entity
class WorkDay(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
    val date: LocalDate,
    val type: String,
    @ElementCollection val attendingStudents: MutableSet<Long>,
)
