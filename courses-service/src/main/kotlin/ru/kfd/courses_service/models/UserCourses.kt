package ru.kfd.courses_service.models

import javax.persistence.*

@Entity
@Table(name = "user_courses")
class UserCourses(
    @Id val id: Long,
    @OneToMany
    @JoinTable(
        name = "user_courses_applied_courses",
        joinColumns = [JoinColumn(name = "user_id")],
        inverseJoinColumns = [JoinColumn(name = "course_id")]
    )
    val appliedCourses: MutableSet<Course> = mutableSetOf(),
    @OneToMany
    @JoinTable(
        name = "user_courses_attended_courses",
        joinColumns = [JoinColumn(name = "user_id")],
        inverseJoinColumns = [JoinColumn(name = "course_id")]
    )
    val attendedCourses: MutableSet<Course> = mutableSetOf(),
    @OneToMany
    @JoinTable(
        name = "user_courses_supervised_courses",
        joinColumns = [JoinColumn(name = "user_id")],
        inverseJoinColumns = [JoinColumn(name = "course_id")]
    )
    val supervisedCourses: MutableSet<Course> = mutableSetOf(),
    @OneToMany
    @JoinTable(
        name = "user_courses_finished_courses",
        joinColumns = [JoinColumn(name = "user_id")],
        inverseJoinColumns = [JoinColumn(name = "course_id")]
    )
    val finishedCourses: MutableSet<Course> = mutableSetOf(),
) {

    fun addAppliedCourse(course: Course) = appliedCourses.add(course)

    fun removeAppliedCourse(course: Course) = appliedCourses.remove(course)

    fun addSupervisedCourse(course: Course) = supervisedCourses.add(course)

    fun removeSupervisedCourse(course: Course) = supervisedCourses.remove(course)

    fun addAttendedCourse(course: Course) = attendedCourses.add(course)

    fun removeAttendedCourse(course: Course) = attendedCourses.remove(course)

    fun addFinishedCourse(course: Course) = finishedCourses.add(course)

    fun removeFinishedCourse(course: Course) = finishedCourses.remove(course)
}
