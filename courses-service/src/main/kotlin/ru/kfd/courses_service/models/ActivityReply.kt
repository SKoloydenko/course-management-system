package ru.kfd.courses_service.models

import javax.persistence.*

@Entity
class ActivityReply(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
    val student: Long,
    var status: ActivityReplyStatus = ActivityReplyStatus.SENT,
    @OneToOne(cascade = [CascadeType.ALL], orphanRemoval = true)
    @JoinColumn(name = "resource_id")
    val attachment: Attachment,
    var grade: Long? = null,
    @ManyToOne val activity: Activity,
) {

    fun isOwner(userId: Long) = student == userId

    fun isResolved() = status != ActivityReplyStatus.SENT

    fun grade(grade: Long) {
        this.grade = grade
        status = ActivityReplyStatus.GRADED
    }
}
