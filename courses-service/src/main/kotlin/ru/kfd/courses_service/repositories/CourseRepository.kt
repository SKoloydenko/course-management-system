package ru.kfd.courses_service.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.kfd.courses_service.models.Course

@Repository
interface CourseRepository : JpaRepository<Course, Long> {

    fun findCoursesBySupervisorAndTitle(supervisorId: Long, title: String): Collection<Course>
}
