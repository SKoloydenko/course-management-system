package ru.kfd.courses_service.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.kfd.courses_service.models.UserCourses

@Repository interface UserCoursesRepository : JpaRepository<UserCourses, Long>
