package ru.kfd.courses_service.exceptions

import ru.kfd.courses_service.constants.ErrorType

class DomainException(val type: ErrorType, message: String) : RuntimeException(message)
