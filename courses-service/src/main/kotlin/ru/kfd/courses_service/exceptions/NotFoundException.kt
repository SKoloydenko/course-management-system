package ru.kfd.courses_service.exceptions

import ru.kfd.courses_service.constants.NotFoundErrorType

class NotFoundException(val type: NotFoundErrorType, message: String) : RuntimeException(message)
