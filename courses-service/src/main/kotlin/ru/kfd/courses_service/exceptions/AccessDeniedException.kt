package ru.kfd.courses_service.exceptions

import org.springframework.security.access.AccessDeniedException
import ru.kfd.courses_service.constants.AccessDeniedErrorType

class AccessDeniedException(val type: AccessDeniedErrorType, message: String) :
    AccessDeniedException(message)
