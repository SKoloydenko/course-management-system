package ru.kfd.courses_service.dto.responses

data class UserCoursesResponseDTO(
    val appliedCourses: Collection<CourseResponseDTO>,
    val attendedCourses: Collection<CourseResponseDTO>,
    val supervisedCourses: Collection<CourseResponseDTO>,
    val finishedCourses: Collection<CourseResponseDTO>,
) {

    companion object {
        fun toDTO(
            appliedCourses: Collection<CourseResponseDTO>,
            attendedCourses: Collection<CourseResponseDTO>,
            supervisedCourses: Collection<CourseResponseDTO>,
            finishedCourses: Collection<CourseResponseDTO>,
        ) =
            UserCoursesResponseDTO(
                appliedCourses = appliedCourses,
                attendedCourses = attendedCourses,
                supervisedCourses = supervisedCourses,
                finishedCourses = finishedCourses,
            )
    }
}
