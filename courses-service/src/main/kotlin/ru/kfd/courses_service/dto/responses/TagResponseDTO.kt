package ru.kfd.courses_service.dto.responses

import ru.kfd.courses_service.models.Tag

data class TagResponseDTO(
    val tagId: Long,
    val title: String,
) {

    companion object {
        fun toDTO(tag: Tag) =
            TagResponseDTO(
                tagId = tag.id as Long,
                title = tag.title,
            )
    }
}
