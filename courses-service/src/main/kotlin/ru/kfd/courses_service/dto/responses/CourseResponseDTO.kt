package ru.kfd.courses_service.dto.responses

import ru.kfd.courses_service.models.Course
import ru.kfd.courses_service.models.CourseRole

open class CourseResponseDTO(
    val courseId: Long,
    val title: String,
    val supervisor: UserResponseDTO,
    val startingDate: String,
    val finishingDate: String,
    val lessonsDays: Collection<Int>,
    val open: Boolean,
    val tagId: Long,
    val description: String,
    val roles: Collection<CourseRole>,
) {

    companion object {
        fun toDTO(course: Course, supervisor: UserResponseDTO, roles: Collection<CourseRole>) =
            CourseResponseDTO(
                courseId = course.id as Long,
                title = course.title,
                supervisor = supervisor,
                startingDate = course.startingDate.toString(),
                finishingDate = course.finishingDate.toString(),
                lessonsDays = course.lessonsDays.map { it.value }.sorted(),
                open = course.open,
                tagId = course.tag.id as Long,
                description = course.description,
                roles = roles,
            )
    }
}
