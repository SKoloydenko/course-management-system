package ru.kfd.courses_service.dto.requests

import ru.kfd.courses_service.models.Course
import ru.kfd.courses_service.models.CourseStatus
import ru.kfd.courses_service.models.Tag
import java.time.DayOfWeek
import java.time.LocalDate
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

data class CourseCreateRequestDTO(
    @field:NotEmpty(message = "Title must not be empty")
    @field:Size(max = 40, message = "Title length must be not greater than 40 symbols")
    val title: String,
    @field:NotNull(message = "Course accessibility must not be null") val open: Boolean,
    @field:NotNull(message = "Starting date must not be null") val startingDate: String,
    @field:NotNull(message = "Finishing date must not be null") val finishingDate: String,
    @field:NotNull(message = "Lesson days must not be null") val lessonsDays: Collection<Int>,
    @field:NotNull(message = "Tag id must not be null") val tagId: Long,
    @field:NotNull(message = "Description must not be null")
    @field:Size(max = 255, message = "Description length must be not greater than 255 symbols")
    val description: String,
) {

    companion object {
        fun toEntity(userDTO: CourseCreateRequestDTO, supervisor: Long, tag: Tag) =
            Course(
                title = userDTO.title,
                supervisor = supervisor,
                status =
                    if (LocalDate.now() == LocalDate.parse(userDTO.startingDate))
                        CourseStatus.CREATED
                    else CourseStatus.ACTIVE,
                startingDate = LocalDate.parse(userDTO.startingDate),
                finishingDate = LocalDate.parse(userDTO.finishingDate),
                lessonsDays = userDTO.lessonsDays.map { DayOfWeek.of(it) }.toMutableSet(),
                open = userDTO.open,
                tag = tag,
                description = userDTO.description,
            )
    }
}
