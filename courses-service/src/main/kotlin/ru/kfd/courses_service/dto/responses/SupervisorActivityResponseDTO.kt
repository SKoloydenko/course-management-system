package ru.kfd.courses_service.dto.responses

import ru.kfd.courses_service.models.Activity

data class SupervisorActivityResponseDTO(
    val activityId: Long,
    val type: String,
    val title: String,
    val description: String,
    val startingTime: String,
    val finishingTime: String,
    val maxGrade: Long,
    val replies: Collection<ActivityReplyResponseDTO>
) {

    companion object {
        fun toDTO(activity: Activity) =
            SupervisorActivityResponseDTO(
                activityId = activity.id as Long,
                type = activity.type.name,
                title = activity.title,
                description = activity.description,
                startingTime = activity.startingTime.toString(),
                finishingTime = activity.finishingTime.toString(),
                maxGrade = activity.maxGrade,
                replies = activity.replies.map { ActivityReplyResponseDTO.toDTO(it) }
            )
    }
}
