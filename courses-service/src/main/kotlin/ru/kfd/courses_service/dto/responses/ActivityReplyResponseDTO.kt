package ru.kfd.courses_service.dto.responses

import ru.kfd.courses_service.models.ActivityReply

data class ActivityReplyResponseDTO(
    val activityReplyId: Long,
    val student: Long,
    val status: String,
    val resource: AttachmentResponseDTO,
    val grade: Long?
) {

    companion object {
        fun toDTO(activityReply: ActivityReply) =
            ActivityReplyResponseDTO(
                activityReplyId = activityReply.id as Long,
                student = activityReply.student,
                status = activityReply.status.name,
                resource = AttachmentResponseDTO.toDTO(activityReply.attachment),
                grade = activityReply.grade,
            )
    }
}
