package ru.kfd.courses_service.dto.responses

data class UserResponseDTO(
    val userId: Long?,
    val firstname: String,
    val lastname: String,
    val universityId: Long,
) {

    companion object {
        fun toDTO(userId: Long, userDTO: UserResponseDTO) =
            UserResponseDTO(
                userId = userId,
                firstname = userDTO.firstname,
                lastname = userDTO.lastname,
                universityId = userDTO.universityId,
            )
    }
}
