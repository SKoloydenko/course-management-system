package ru.kfd.courses_service.dto.requests

import ru.kfd.courses_service.models.Activity
import ru.kfd.courses_service.models.ActivityType
import ru.kfd.courses_service.models.Course
import java.time.ZonedDateTime
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class ActivityCreateRequestDTO(
    @field:NotEmpty(message = "Title must not be empty") val title: String,
    @field:NotEmpty(message = "Activity type must not be empty") val type: ActivityType,
    @field:NotNull(message = "Description type must not be null") val description: String,
    @field:NotEmpty(message = "Starting time must not be empty") val startingTime: String,
    @field:NotEmpty(message = "Finishing time must not be empty") val finishingTime: String,
    @field:Min(value = 0, message = "Grade must be greater than zero") val maxGrade: Long,
) {

    companion object {
        fun toEntity(activityDTO: ActivityCreateRequestDTO, course: Course) =
            Activity(
                type = activityDTO.type,
                title = activityDTO.title,
                description = activityDTO.description,
                startingTime = ZonedDateTime.parse(activityDTO.startingTime),
                finishingTime = ZonedDateTime.parse(activityDTO.finishingTime),
                maxGrade = activityDTO.maxGrade,
                course = course,
            )
    }
}
