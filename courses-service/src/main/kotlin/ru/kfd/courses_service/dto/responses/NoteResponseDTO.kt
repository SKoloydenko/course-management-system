package ru.kfd.courses_service.dto.responses

import ru.kfd.courses_service.models.Note

data class NoteResponseDTO(
    val noteId: Long,
    val text: String,
    val date: String,
) {

    companion object {
        fun toDTO(note: Note) =
            NoteResponseDTO(
                noteId = note.id as Long,
                text = note.text,
                date = note.date.toString(),
            )
    }
}
