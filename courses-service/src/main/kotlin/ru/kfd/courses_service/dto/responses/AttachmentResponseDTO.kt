package ru.kfd.courses_service.dto.responses

import ru.kfd.courses_service.models.Attachment

data class AttachmentResponseDTO(
    val fileId: Long,
    val name: String,
    val date: String,
) {

    companion object {
        fun toDTO(attachment: Attachment) =
            AttachmentResponseDTO(
                fileId = attachment.id as Long,
                name = attachment.name,
                date = attachment.date.toString()
            )
    }
}
