package ru.kfd.courses_service.dto.responses

import ru.kfd.courses_service.models.Course
import ru.kfd.courses_service.models.CourseRole

class StudentCourseResponseDTO(
    courseId: Long,
    title: String,
    supervisor: UserResponseDTO,
    startingDate: String,
    finishingDate: String,
    lessonsDays: Collection<Int>,
    open: Boolean,
    tagId: Long,
    description: String,
    roles: Collection<CourseRole> = setOf(CourseRole.STUDENT, CourseRole.MEMBER),
    val students: Collection<UserResponseDTO>,
    val notes: Collection<NoteResponseDTO>,
    val attachments: Collection<AttachmentResponseDTO>,
    val activities: Collection<StudentActivityResponseDTO>
) :
    CourseResponseDTO(
        courseId,
        title,
        supervisor,
        startingDate,
        finishingDate,
        lessonsDays,
        open,
        tagId,
        description,
        roles,
    ) {

    companion object {
        fun toDTO(
            course: Course,
            supervisor: UserResponseDTO,
            students: Collection<UserResponseDTO>,
            userId: Long,
        ) =
            StudentCourseResponseDTO(
                courseId = course.id as Long,
                title = course.title,
                supervisor = supervisor,
                startingDate = course.startingDate.toString(),
                finishingDate = course.finishingDate.toString(),
                lessonsDays = course.lessonsDays.map { it.value }.sorted(),
                open = course.open,
                tagId = course.tag.id as Long,
                description = course.description,
                students = students,
                notes = course.notes.map { NoteResponseDTO.toDTO(it) },
                attachments = course.attachments.map { AttachmentResponseDTO.toDTO(it) },
                activities = course.activities.map { StudentActivityResponseDTO.toDTO(it, userId) },
            )
    }
}
