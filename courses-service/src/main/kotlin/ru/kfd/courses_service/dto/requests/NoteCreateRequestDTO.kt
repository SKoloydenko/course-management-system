package ru.kfd.courses_service.dto.requests

import ru.kfd.courses_service.models.Course
import ru.kfd.courses_service.models.Note
import java.time.LocalDate
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size

data class NoteCreateRequestDTO(
    @field:NotEmpty(message = "Text must not be empty")
    @field:Size(max = 255, message = "Text length must be not greater than 255 symbols")
    val text: String,
    val date: String,
) {

    companion object {
        fun toEntity(noteDTO: NoteCreateRequestDTO, course: Course) =
            Note(text = noteDTO.text, course = course, date = LocalDate.parse(noteDTO.date))
    }
}
