package ru.kfd.courses_service.dto.requests

data class ActivityReplyResolveRequestDTO(
    val grade: Long,
)
