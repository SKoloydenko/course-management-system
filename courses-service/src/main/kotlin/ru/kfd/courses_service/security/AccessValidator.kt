package ru.kfd.courses_service.security

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.kfd.courses_service.constants.AccessDeniedErrorType
import ru.kfd.courses_service.exceptions.AccessDeniedException
import ru.kfd.courses_service.services.CourseAccessValidationService

@Component
class AccessValidator
@Autowired
constructor(
    private val courseAccessValidationService: CourseAccessValidationService,
) {

    fun isApplicant(courseId: Long, userId: String): Boolean {
        val isValid = courseAccessValidationService.isApplicant(courseId, userId.toLong())
        if (!isValid) {
            throw AccessDeniedException(
                AccessDeniedErrorType.APPLICANT_ONLY,
                "Only applicant has access"
            )
        }
        return true
    }

    fun isSupervisor(courseId: Long, userId: String): Boolean {
        val isValid = courseAccessValidationService.isSupervisor(courseId, userId.toLong())
        if (!isValid) {
            throw AccessDeniedException(
                AccessDeniedErrorType.SUPERVISOR_ONLY,
                "Only supervisor has access"
            )
        }
        return true
    }

    fun isStudent(courseId: Long, userId: String): Boolean {
        val isValid = courseAccessValidationService.isStudent(courseId, userId.toLong())
        if (!isValid) {
            throw AccessDeniedException(
                AccessDeniedErrorType.STUDENT_ONLY,
                "Only student has access"
            )
        }
        return true
    }

    fun isCourseMember(courseId: Long, userId: String): Boolean {
        val isValid =
            courseAccessValidationService.isStudent(courseId, userId.toLong()) ||
                courseAccessValidationService.isSupervisor(courseId, userId.toLong())
        if (!isValid) {
            throw AccessDeniedException(
                AccessDeniedErrorType.COURSE_MEMBER_ONLY,
                "Only course member has access"
            )
        }
        return true
    }

    fun isUser(courseId: Long, userId: String): Boolean {
        val isValid = courseAccessValidationService.isUser(courseId, userId.toLong())
        if (!isValid) {
            throw AccessDeniedException(AccessDeniedErrorType.USER_ONLY, "Only user has access")
        }
        return true
    }

    fun isActivityReplyOwner(
        courseId: Long,
        activityId: Long,
        activityReplyId: Long,
        userId: String
    ): Boolean {
        val isValid =
            courseAccessValidationService.isActivityReplyOwner(
                courseId,
                activityId,
                activityReplyId,
                userId.toLong()
            )
        if (!isValid) {
            throw AccessDeniedException(
                AccessDeniedErrorType.ACTIVITY_REPLY_OWNER_ONLY,
                "Only activity reply owner has access"
            )
        }
        return true
    }
}
