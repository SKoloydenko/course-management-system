package ru.kfd.courses_service

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.scheduling.annotation.EnableScheduling

@EnableEurekaClient @EnableScheduling @SpringBootApplication class CoursesServiceApplication

fun main(args: Array<String>) {
    runApplication<CoursesServiceApplication>(*args)
}
