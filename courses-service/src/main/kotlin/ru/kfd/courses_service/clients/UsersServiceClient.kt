package ru.kfd.courses_service.clients

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForObject
import ru.kfd.courses_service.dto.responses.UserResponseDTO

@Component
class UsersServiceClient
@Autowired
constructor(
    private val restTemplate: RestTemplate,
) {

    fun getUserById(userId: Long): UserResponseDTO {
        val user: UserResponseDTO =
            restTemplate.getForObject("http://USERS-SERVICE/api/users/$userId")
        return UserResponseDTO.toDTO(userId, user)
    }

    fun getUsersByIds(userIds: Collection<Long>): Collection<UserResponseDTO> =
        userIds.map { getUserById(it) }.toMutableSet()
}
