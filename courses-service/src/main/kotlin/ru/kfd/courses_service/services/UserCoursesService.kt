package ru.kfd.courses_service.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.kfd.courses_service.clients.UsersServiceClient
import ru.kfd.courses_service.constants.ErrorType
import ru.kfd.courses_service.constants.NotFoundErrorType
import ru.kfd.courses_service.dto.responses.CourseResponseDTO
import ru.kfd.courses_service.dto.responses.UserCoursesResponseDTO
import ru.kfd.courses_service.exceptions.DomainException
import ru.kfd.courses_service.exceptions.NotFoundException
import ru.kfd.courses_service.models.Course
import ru.kfd.courses_service.models.CourseRole
import ru.kfd.courses_service.models.UserCourses
import ru.kfd.courses_service.repositories.UserCoursesRepository
import javax.transaction.Transactional

@Service
class UserCoursesService
@Autowired
constructor(
    private val userCoursesRepository: UserCoursesRepository,
    private val usersServiceClient: UsersServiceClient,
) {

    private fun findUserCourses(userId: Long) =
        userCoursesRepository.findById(userId).orElseThrow {
            NotFoundException(
                NotFoundErrorType.USER_COURSES_DO_NOT_EXIST,
                "Courses of user with this id do not exist"
            )
        }

    fun getUserCoursesByUserId(userId: Long): UserCoursesResponseDTO {
        val userCourses = findUserCourses(userId)
        val appliedCourses =
            userCourses.appliedCourses.map {
                CourseResponseDTO.toDTO(
                    it,
                    usersServiceClient.getUserById(it.supervisor),
                    setOf(CourseRole.USER)
                )
            }
        val attendedCourses =
            userCourses.attendedCourses.map {
                CourseResponseDTO.toDTO(
                    it,
                    usersServiceClient.getUserById(it.supervisor),
                    setOf(CourseRole.USER)
                )
            }
        val supervisedCourses =
            userCourses.supervisedCourses.map {
                CourseResponseDTO.toDTO(
                    it,
                    usersServiceClient.getUserById(it.supervisor),
                    setOf(CourseRole.USER)
                )
            }
        val finishedCourses =
            userCourses.finishedCourses.map {
                CourseResponseDTO.toDTO(
                    it,
                    usersServiceClient.getUserById(it.supervisor),
                    setOf(CourseRole.USER)
                )
            }
        return UserCoursesResponseDTO.toDTO(
            appliedCourses,
            attendedCourses,
            supervisedCourses,
            finishedCourses
        )
    }

    fun createUserCourses(userId: Long): UserCoursesResponseDTO {
        if (userCoursesRepository.findById(userId).isPresent) {
            throw DomainException(
                ErrorType.USER_COURSES_ALREADY_EXIST,
                "Courses of user with this id already exist"
            )
        }
        userCoursesRepository.save(UserCourses(userId))
        return UserCoursesResponseDTO.toDTO(
            mutableSetOf(),
            mutableSetOf(),
            mutableSetOf(),
            mutableSetOf()
        )
    }

    @Transactional
    fun deleteUserCourses(userId: Long) {
        val userCourses = findUserCourses(userId)

        userCourses.supervisedCourses.forEach {
            if (it.isFinished()) {
                removeFinishedCourse(it)
            } else {
                removeCourse(it)
            }
        }
        userCoursesRepository.delete(userCourses)
    }

    @Transactional
    fun addAppliedCourse(course: Course, userId: Long) {
        val userCourses = findUserCourses(userId)
        userCourses.addAppliedCourse(course)
    }

    @Transactional
    fun removeAppliedCourse(course: Course, userId: Long) {
        val userCourses = findUserCourses(userId)
        userCourses.removeAppliedCourse(course)
    }

    @Transactional
    fun addSupervisedCourse(course: Course, supervisorId: Long) {
        val supervisorCourses = findUserCourses(supervisorId)
        supervisorCourses.addSupervisedCourse(course)
    }

    @Transactional
    fun moveCourseFromAppliedToAttended(course: Course, userId: Long) {
        val userCourses = findUserCourses(userId)
        userCourses.removeAppliedCourse(course)
        userCourses.addAttendedCourse(course)
    }

    @Transactional
    fun addAttendedCourse(course: Course, userId: Long) {
        val userCourses = findUserCourses(userId)
        userCourses.addAttendedCourse(course)
    }

    @Transactional
    fun removeAttendedCourse(course: Course, userId: Long) {
        val userCourses = findUserCourses(userId)
        userCourses.removeAttendedCourse(course)
    }

    @Transactional
    fun removeCourse(course: Course) {
        val supervisorCourses = findUserCourses(course.supervisor)
        supervisorCourses.removeSupervisedCourse(course)

        val applicantsCourses = course.applicants.map { findUserCourses(it) }
        applicantsCourses.forEach { it.removeAppliedCourse(course) }

        val studentsCourses = course.students.map { findUserCourses(it) }
        studentsCourses.forEach { it.removeAttendedCourse(course) }
    }

    @Transactional
    fun removeFinishedCourse(course: Course) {
        val supervisorCourses = findUserCourses(course.supervisor)
        supervisorCourses.removeFinishedCourse(course)

        val applicantsCourses = course.applicants.map { findUserCourses(it) }
        applicantsCourses.forEach { it.removeFinishedCourse(course) }

        val studentsCourses = course.students.map { findUserCourses(it) }
        studentsCourses.forEach { it.removeFinishedCourse(course) }
    }

    @Transactional
    fun finishCourse(course: Course) {
        val supervisorCourses = findUserCourses(course.supervisor)
        supervisorCourses.removeSupervisedCourse(course)
        supervisorCourses.addFinishedCourse(course)

        val applicantsCourses = course.applicants.map { findUserCourses(it) }
        applicantsCourses.forEach { it.removeAppliedCourse(course) }

        val studentsCourses = course.students.map { findUserCourses(it) }
        studentsCourses.forEach {
            it.removeAttendedCourse(course)
            it.addFinishedCourse(course)
        }
    }
}
