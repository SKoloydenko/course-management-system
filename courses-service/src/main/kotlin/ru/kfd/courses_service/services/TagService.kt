package ru.kfd.courses_service.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.kfd.courses_service.dto.responses.TagResponseDTO
import ru.kfd.courses_service.repositories.TagRepository

@Service
class TagService
@Autowired
constructor(
    private val tagRepository: TagRepository,
) {

    fun getTags(): Collection<TagResponseDTO> {
        val tags = tagRepository.findAll()
        return tags.map { TagResponseDTO.toDTO(it) }
    }
}
