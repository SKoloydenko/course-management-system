package ru.kfd.courses_service.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.kfd.courses_service.constants.NotFoundErrorType
import ru.kfd.courses_service.exceptions.NotFoundException
import ru.kfd.courses_service.repositories.CourseRepository

@Service
class CourseAccessValidationService
@Autowired
constructor(
    private val courseRepository: CourseRepository,
) {

    private fun findCourse(courseId: Long) =
        courseRepository.findById(courseId).orElseThrow {
            NotFoundException(
                NotFoundErrorType.COURSE_DOES_NOT_EXIST,
                "Course with this id does not exist"
            )
        }

    fun isSupervisor(courseId: Long, userId: Long): Boolean {
        val course = findCourse(courseId)
        return course.isSupervisor(userId)
    }

    fun isApplicant(courseId: Long, userId: Long): Boolean {
        val course = findCourse(courseId)
        return course.isApplicant(userId)
    }

    fun isStudent(courseId: Long, userId: Long): Boolean {
        val course = findCourse(courseId)
        return course.isStudent(userId)
    }

    fun isUser(courseId: Long, userId: Long): Boolean {
        val course = findCourse(courseId)
        return !course.isSupervisor(userId) &&
            !course.isApplicant(userId) &&
            !course.isStudent(userId)
    }

    fun isActivityReplyOwner(
        courseId: Long,
        activityId: Long,
        activityReplyId: Long,
        userId: Long
    ): Boolean {
        val course = findCourse(courseId)
        val activity =
            course.getActivity(activityId)
                ?: throw NotFoundException(
                    NotFoundErrorType.ACTIVITY_DOES_NOT_EXIST,
                    "Activity with this id does not exist",
                )
        val activityReply =
            activity.getReply(activityReplyId)
                ?: throw NotFoundException(
                    NotFoundErrorType.ACTIVITY_REPLY_DOES_NOT_EXIST,
                    "Activity reply with this id does not exist",
                )
        return activityReply.isOwner(userId)
    }
}
