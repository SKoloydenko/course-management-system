package ru.kfd.courses_service.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.Resource
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import ru.kfd.courses_service.clients.UsersServiceClient
import ru.kfd.courses_service.constants.ErrorType
import ru.kfd.courses_service.constants.NotFoundErrorType
import ru.kfd.courses_service.dto.requests.ActivityCreateRequestDTO
import ru.kfd.courses_service.dto.requests.ActivityReplyResolveRequestDTO
import ru.kfd.courses_service.dto.requests.CourseCreateRequestDTO
import ru.kfd.courses_service.dto.requests.NoteCreateRequestDTO
import ru.kfd.courses_service.dto.responses.*
import ru.kfd.courses_service.exceptions.DomainException
import ru.kfd.courses_service.exceptions.NotFoundException
import ru.kfd.courses_service.models.ActivityReply
import ru.kfd.courses_service.models.Course
import ru.kfd.courses_service.models.CourseRole
import ru.kfd.courses_service.repositories.CourseRepository
import ru.kfd.courses_service.repositories.TagRepository
import javax.transaction.Transactional

@Service
class CourseService
@Autowired
constructor(
    private val courseRepository: CourseRepository,
    private val tagRepository: TagRepository,
    private val userCoursesService: UserCoursesService,
    private val attachmentService: AttachmentService,
    private val usersServiceClient: UsersServiceClient,
) {

    private fun findCourse(courseId: Long) =
        courseRepository.findById(courseId).orElseThrow {
            NotFoundException(
                NotFoundErrorType.COURSE_DOES_NOT_EXIST,
                "Course with this id does not exist"
            )
        }

    private fun findTag(tagId: Long) =
        tagRepository.findById(tagId).orElseThrow {
            NotFoundException(
                NotFoundErrorType.TAG_DOES_NOT_EXIST,
                "Tag with this id does not exist"
            )
        }

    fun getCourses(): Collection<CourseResponseDTO> {
        val courses = courseRepository.findAll()
        return courses.map {
            CourseResponseDTO.toDTO(
                it,
                usersServiceClient.getUserById(it.supervisor),
                setOf(CourseRole.USER)
            )
        }
    }

    fun getCourseById(courseId: Long, userId: Long?): CourseResponseDTO {
        val course = findCourse(courseId)

        // rest call
        val supervisor = usersServiceClient.getUserById(course.supervisor)

        if (userId == null) {
            return CourseResponseDTO.toDTO(course, supervisor, setOf(CourseRole.USER))
        }

        if (course.isApplicant(userId)) {
            return CourseResponseDTO.toDTO(course, supervisor, setOf(CourseRole.APPLICANT))
        }

        // rest call
        val applicants = usersServiceClient.getUsersByIds(course.applicants)
        val students = usersServiceClient.getUsersByIds(course.students)

        return if (course.isSupervisor(userId)) {
            SupervisorCourseResponseDTO.toDTO(course, supervisor, students, applicants)
        } else if (course.isStudent(userId)) {
            StudentCourseResponseDTO.toDTO(course, supervisor, students, userId)
        } else {
            return CourseResponseDTO.toDTO(course, supervisor, setOf(CourseRole.USER))
        }
    }

    @Transactional
    fun createCourse(supervisorId: Long, courseDTO: CourseCreateRequestDTO): CourseResponseDTO {
        val tag = findTag(courseDTO.tagId)
        val coursePrototype = CourseCreateRequestDTO.toEntity(courseDTO, supervisorId, tag)
        if (courseRepository
                .findCoursesBySupervisorAndTitle(supervisorId, coursePrototype.title)
                .isNotEmpty()
        ) {
            throw DomainException(
                ErrorType.COURSE_ALREADY_EXISTS,
                "Course with this title already exists for this user"
            )
        }

        // rest call
        val supervisor = usersServiceClient.getUserById(supervisorId)

        val course = courseRepository.save(coursePrototype)
        userCoursesService.addSupervisedCourse(course, supervisorId)

        return SupervisorCourseResponseDTO.toDTO(course, supervisor, setOf(), setOf())
    }

    @Transactional
    fun applyOnCourse(courseId: Long, userId: Long) {
        val course = findCourse(courseId)
        if (course.isOpen()) {
            course.addStudent(userId)
            userCoursesService.addAttendedCourse(course, userId)
        } else {
            course.addApplicant(userId)
            userCoursesService.addAppliedCourse(course, userId)
        }
    }

    private fun findOpenCourse(courseId: Long): Course {
        val course = findCourse(courseId)
        if (course.isOpen()) {
            throw DomainException(
                ErrorType.OPEN_COURSES_DO_NOT_HAVE_APPLICATIONS,
                "Open course does not have applications"
            )
        }
        return course
    }

    @Transactional
    fun deleteApplicationOnCourse(courseId: Long, userId: Long) {
        val course = findOpenCourse(courseId)
        if (!course.isApplicant(userId)) {
            throw DomainException(ErrorType.USER_IS_NOT_APPLICANT, "User is not applicant")
        }
        course.deleteApplication(userId)
        userCoursesService.removeAppliedCourse(course, userId)
    }

    @Transactional
    fun confirmApplicationOnCourse(courseId: Long, supervisorId: Long, userId: Long) {
        val course = findOpenCourse(courseId)
        if (!course.isApplicant(userId)) {
            throw DomainException(ErrorType.USER_IS_NOT_APPLICANT, "User is not applicant")
        }
        course.deleteApplication(userId)
        course.addStudent(userId)
        userCoursesService.moveCourseFromAppliedToAttended(course, userId)
    }

    @Transactional
    fun rejectApplicationOnCourse(courseId: Long, supervisorId: Long, userId: Long) {
        val course = findOpenCourse(courseId)
        if (!course.isApplicant(userId)) {
            throw DomainException(ErrorType.USER_IS_NOT_APPLICANT, "User is not applicant")
        }
        course.deleteApplication(userId)
        userCoursesService.removeAppliedCourse(course, userId)
    }

    @Transactional
    fun deleteCourse(courseId: Long, supervisorId: Long) {
        val course = findCourse(courseId)
        if (course.isFinished()) {
            userCoursesService.removeFinishedCourse(course)
        } else {
            userCoursesService.removeCourse(course)
        }
        courseRepository.delete(course)
    }

    @Transactional
    fun deleteStudentFromCourse(courseId: Long, userId: Long) {
        val course = findCourse(courseId)
        if (!course.isStudent(userId)) {
            throw DomainException(ErrorType.USER_IS_NOT_STUDENT, "User is not student")
        }
        course.deleteStudent(userId)
        userCoursesService.removeAttendedCourse(course, userId)
    }

    private fun findActiveCourse(courseId: Long): Course {
        val course = findCourse(courseId)
        if (course.isFinished()) {
            throw DomainException(ErrorType.COURSE_IS_FINISHED, "Course is finished")
        }
        return course
    }

    @Transactional
    fun createNote(courseId: Long, noteDTO: NoteCreateRequestDTO): Collection<NoteResponseDTO> {
        val course = findActiveCourse(courseId)
        val note = NoteCreateRequestDTO.toEntity(noteDTO, course)
        course.addNote(note)
        return courseRepository.save(course).notes.map { NoteResponseDTO.toDTO(it) }.sortedBy {
            it.noteId
        }
    }

    @Transactional
    fun deleteNote(courseId: Long, noteId: Long) {
        val course = findActiveCourse(courseId)
        if (!course.deleteNote(noteId)) {
            throw NotFoundException(
                NotFoundErrorType.NOTE_DOES_NOT_EXIST,
                "Note with this id does not exist"
            )
        }
    }

    fun getAttachment(courseId: Long, attachmentId: Long): Resource {
        val course = findCourse(courseId)
        return attachmentService.getFile(attachmentId)
    }

    @Transactional
    fun addAttachment(
        courseId: Long,
        file: MultipartFile,
        date: String
    ): Collection<AttachmentResponseDTO> {
        val course = findCourse(courseId)
        val attachment = attachmentService.addFile(courseId, file, date)
        course.addAttachment(attachment)
        return courseRepository
            .save(course)
            .attachments
            .map { AttachmentResponseDTO.toDTO(it) }
            .sortedBy { it.fileId }
    }

    @Transactional
    fun deleteAttachment(courseId: Long, fileId: Long) {
        val course = findCourse(courseId)
        attachmentService.deleteFile(courseId, fileId)
        if (!course.deleteAttachment(fileId)) {
            throw NotFoundException(
                NotFoundErrorType.FILE_DOES_NOT_EXIST,
                "File with this id does not exist"
            )
        }
    }

    private fun findActivity(courseId: Long, activityId: Long) =
        findActiveCourse(courseId).getActivity(activityId)
            ?: throw NotFoundException(
                NotFoundErrorType.ACTIVITY_DOES_NOT_EXIST,
                "Activity with this id does not exist"
            )

    @Transactional
    fun createCourseActivity(courseId: Long, activityDTO: ActivityCreateRequestDTO) {
        val course = findCourse(courseId)
        val activity = ActivityCreateRequestDTO.toEntity(activityDTO, course)
        course.addActivity(activity)
    }

    @Transactional
    fun deleteCourseActivity(courseId: Long, activityId: Long) {
        val course = findCourse(courseId)
        if (!course.deleteActivity(activityId)) {
            throw NotFoundException(
                NotFoundErrorType.ACTIVITY_DOES_NOT_EXIST,
                "Activity with this id does not exist"
            )
        }
    }

    @Transactional
    fun createCourseActivityReply(
        courseId: Long,
        activityId: Long,
        userId: Long,
        file: MultipartFile,
        date: String,
    ) {
        val activity = findActivity(courseId, activityId)
        if (!activity.isAvailable()) {
            throw DomainException(ErrorType.ACTIVITY_IS_NOT_AVAILABLE, "Activity is not available")
        }
        val activityReply =
            ActivityReply(
                student = userId,
                activity = activity,
                attachment = attachmentService.addFile(courseId, file, date)
            )
        if (activity.containsReply(activityReply.student)) {
            throw DomainException(
                ErrorType.ACTIVITY_REPLY_ALREADY_EXISTS,
                "Activity reply for this student already exists"
            )
        }
        activity.addReply(activityReply)
    }

    @Transactional
    fun resolveCourseActivityReply(
        courseId: Long,
        activityId: Long,
        activityReplyId: Long,
        activityReplyResolveDTO: ActivityReplyResolveRequestDTO,
    ) {
        val activity = findActivity(courseId, activityId)
        val activityReply =
            activity.getReply(activityReplyId)
                ?: throw NotFoundException(
                    NotFoundErrorType.ACTIVITY_REPLY_DOES_NOT_EXIST,
                    "Activity reply with this id does not exist"
                )
        if (activityReply.isResolved()) {
            throw DomainException(
                ErrorType.ACTIVITY_REPLY_ALREADY_RESOLVED,
                "Activity reply is already resolved"
            )
        }
        val grade = activityReplyResolveDTO.grade
        if (grade > activity.maxGrade) {
            throw DomainException(
                ErrorType.INVALID_GRADE,
                "Grade can not be greater than max grade"
            )
        }
        activityReply.grade(grade)
    }

    @Transactional
    fun deleteCourseActivityReply(courseId: Long, activityId: Long, activityReplyId: Long) {
        val activity = findActivity(courseId, activityId)
        val activityReply =
            activity.getReply(activityReplyId)
                ?: throw NotFoundException(
                    NotFoundErrorType.ACTIVITY_REPLY_DOES_NOT_EXIST,
                    "Activity reply with this id does not exist"
                )
        if (activityReply.isResolved()) {
            throw DomainException(
                ErrorType.RESOLVED_ACTIVITY_REPLY_CAN_NOT_BE_DELETED,
                "Resolved activity reply can not be deleted"
            )
        }
        activity.deleteReply(activityReplyId)
    }
}
