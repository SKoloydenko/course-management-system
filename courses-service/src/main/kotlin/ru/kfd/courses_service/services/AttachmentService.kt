package ru.kfd.courses_service.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.FileSystemResource
import org.springframework.core.io.Resource
import org.springframework.stereotype.Service
import org.springframework.util.StringUtils
import org.springframework.web.multipart.MultipartFile
import ru.kfd.courses_service.constants.ErrorType
import ru.kfd.courses_service.constants.NotFoundErrorType
import ru.kfd.courses_service.exceptions.DomainException
import ru.kfd.courses_service.exceptions.NotFoundException
import ru.kfd.courses_service.models.Attachment
import ru.kfd.courses_service.repositories.AttachmentRepository
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths
import java.time.LocalDate
import javax.transaction.Transactional
import kotlin.io.path.exists

@Service
class AttachmentService
@Autowired
constructor(
    private val attachmentRepository: AttachmentRepository,
) {

    private final val uploadDir = "/attachments"

    private var fileStorageLocation = Paths.get(uploadDir).toAbsolutePath().normalize()

    init {
        try {
            Files.createDirectories(fileStorageLocation)
        } catch (ex: IOException) {
            throw RuntimeException("Could not create directory")
        }
    }

    fun getFile(attachmentId: Long): Resource {
        val attachment =
            attachmentRepository.findById(attachmentId).orElseThrow {
                NotFoundException(
                    NotFoundErrorType.FILE_DOES_NOT_EXIST,
                    "File with this id does not exist"
                )
            }
        val path = fileStorageLocation.resolve("${attachment.id}-${attachment.name}")
        if (!path.exists()) {
            throw NotFoundException(
                NotFoundErrorType.FILE_DOES_NOT_EXIST,
                "File with this id does not exist"
            )
        }
        return FileSystemResource(path)
    }

    @Transactional
    fun addFile(courseId: Long, resource: MultipartFile, date: String): Attachment {
        val fileName =
            StringUtils.cleanPath(
                resource.originalFilename
                    ?: throw DomainException(ErrorType.INVALID_FILENAME, "Invalid filename")
            )
        val attachment =
            attachmentRepository.save(Attachment(name = fileName, date = LocalDate.parse(date)))
        val path = fileStorageLocation.resolve("${attachment.id}-$fileName")
        Files.write(path, resource.bytes)
        return attachment
    }

    fun deleteFile(courseId: Long, fileId: Long) {
        val file =
            attachmentRepository.findById(fileId).orElseThrow {
                NotFoundException(
                    NotFoundErrorType.FILE_DOES_NOT_EXIST,
                    "File with this id does not exist"
                )
            }
        val path = fileStorageLocation.resolve("${file.id}-${file.name}")
        Files.delete(path)
        attachmentRepository.delete(file)
    }
}
