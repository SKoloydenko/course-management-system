package ru.kfd.courses_service.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import ru.kfd.courses_service.repositories.CourseRepository
import javax.transaction.Transactional

@Service
class CourseScheduledEventsService
@Autowired
constructor(
    private val courseRepository: CourseRepository,
    private val userCoursesService: UserCoursesService,
) {

    @Scheduled(cron = "0 4 * * * ?")
    @Transactional
    fun startCourses() {
        val courses = courseRepository.findAll()
        courses.forEach { course ->
            if (course.isCreated() && course.shouldStart()) {
                course.start()
            }
        }
    }

    @Scheduled(cron = "0 4 * * * ?")
    @Transactional
    fun finishCourses() {
        val courses = courseRepository.findAll()
        courses.forEach { course ->
            if (course.shouldFinish() && course.isActive()) {
                course.finish()
                userCoursesService.finishCourse(course)
            }
        }
    }
}
