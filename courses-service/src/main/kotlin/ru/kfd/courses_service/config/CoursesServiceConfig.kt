package ru.kfd.courses_service.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes
import org.springframework.cloud.client.loadbalancer.LoadBalanced
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.client.RestTemplate
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import ru.kfd.courses_service.security.JwtConfig
import ru.kfd.courses_service.security.filters.AuthenticationFilter
import java.nio.charset.StandardCharsets.UTF_8

@Configuration
class CoursesServiceConfig
@Autowired
constructor(
    private val jwtConfig: JwtConfig,
) : WebMvcConfigurer {

    @Bean @LoadBalanced fun restTemplate() = RestTemplate()

    @Bean fun authenticationFilter() = AuthenticationFilter(jwtConfig)

    @Bean
    fun errorAttributes() =
        object : DefaultErrorAttributes() {
            override fun getErrorAttributes(
                webRequest: WebRequest,
                includeStackTrace: Boolean
            ): MutableMap<String, Any> {
                val errorAttributes = super.getErrorAttributes(webRequest, false)
                errorAttributes.remove("timestamp")
                errorAttributes.remove("error")
                errorAttributes.remove("path")
                return errorAttributes
            }
        }

    override fun configureMessageConverters(converters: MutableList<HttpMessageConverter<*>>) {
        converters.filterIsInstance<MappingJackson2HttpMessageConverter>().first().defaultCharset =
            UTF_8
    }
}
