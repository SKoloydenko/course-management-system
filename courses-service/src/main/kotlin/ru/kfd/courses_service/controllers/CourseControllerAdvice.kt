package ru.kfd.courses_service.controllers

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.client.HttpStatusCodeException
import ru.kfd.courses_service.constants.ErrorType
import ru.kfd.courses_service.dto.responses.ErrorResponseDTO
import ru.kfd.courses_service.exceptions.AccessDeniedException
import ru.kfd.courses_service.exceptions.DomainException
import ru.kfd.courses_service.exceptions.NotFoundException

@RestControllerAdvice
class CourseControllerAdvice {

    @ExceptionHandler(DomainException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleDomainException(ex: DomainException) =
        ErrorResponseDTO(HttpStatus.BAD_REQUEST.value(), ex.type.name, ex.message)

    @ExceptionHandler(NotFoundException::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun handleNotFoundException(ex: NotFoundException) =
        ErrorResponseDTO(HttpStatus.NOT_FOUND.value(), ex.type.name, ex.message)

    @ExceptionHandler(AccessDeniedException::class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    fun handleAccessDeniedException(ex: AccessDeniedException) =
        ErrorResponseDTO(HttpStatus.FORBIDDEN.value(), ex.type.name, ex.message)

    @ExceptionHandler(MethodArgumentNotValidException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleArgumentNotValidException(ex: MethodArgumentNotValidException): ErrorResponseDTO {
        val message = ex.bindingResult.fieldErrors.map { it.defaultMessage }[0].toString()
        return ErrorResponseDTO(
            HttpStatus.BAD_REQUEST.value(),
            ErrorType.INVALID_FIELD.name,
            message
        )
    }

    @ExceptionHandler(HttpStatusCodeException::class)
    fun handleHttpStatusCodeException(ex: HttpStatusCodeException): ResponseEntity<Any> {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        return ResponseEntity.status(ex.statusCode).headers(headers).body(ex.responseBodyAsString)
    }
}
