package ru.kfd.courses_service.controllers

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import ru.kfd.courses_service.services.TagService

@RestController
@RequestMapping("/api/tags")
class TagController
@Autowired
constructor(
    private val tagService: TagService,
) {

    @GetMapping("/") @ResponseStatus(HttpStatus.OK) fun getTags() = tagService.getTags()
}
