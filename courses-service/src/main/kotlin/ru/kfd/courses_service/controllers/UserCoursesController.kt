package ru.kfd.courses_service.controllers

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import ru.kfd.courses_service.services.UserCoursesService

@RestController
@RequestMapping("/api/courses/users")
class UserCoursesController
@Autowired
constructor(
    private val userCoursesService: UserCoursesService,
) {

    @GetMapping("/{userId}")
    @ResponseStatus(HttpStatus.OK)
    fun getUserCoursesByUserId(@PathVariable userId: Long) =
        userCoursesService.getUserCoursesByUserId(userId)

    @PostMapping("/{userId}")
    @ResponseStatus(HttpStatus.CREATED)
    fun createUserCourses(@PathVariable userId: Long) = userCoursesService.createUserCourses(userId)

    @DeleteMapping("/{userId}")
    @ResponseStatus(HttpStatus.OK)
    fun deleteUserCoursesById(@PathVariable userId: Long) =
        userCoursesService.deleteUserCourses(userId)
}
