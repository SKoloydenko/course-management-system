package ru.kfd.courses_service.controllers

import org.apache.commons.lang.StringEscapeUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.InputStreamResource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import ru.kfd.courses_service.dto.requests.ActivityCreateRequestDTO
import ru.kfd.courses_service.dto.requests.ActivityReplyResolveRequestDTO
import ru.kfd.courses_service.dto.requests.CourseCreateRequestDTO
import ru.kfd.courses_service.dto.requests.NoteCreateRequestDTO
import ru.kfd.courses_service.services.CourseService
import java.io.FileInputStream
import java.security.Principal
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid

@RestController
@RequestMapping("/api/courses")
class CourseController
@Autowired
constructor(
    private val courseService: CourseService,
) {

    @GetMapping("/") @ResponseStatus(HttpStatus.OK) fun getCourses() = courseService.getCourses()

    @GetMapping("/{courseId}")
    @ResponseStatus(HttpStatus.OK)
    fun getCourseById(
        @PathVariable courseId: Long,
        principal: Principal?,
    ) = courseService.getCourseById(courseId, principal?.name?.toLong())

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    fun createCourse(
        @RequestBody @Valid course: CourseCreateRequestDTO,
        principal: Principal,
    ) = courseService.createCourse(principal.name.toLong(), course)

    @PostMapping("/{courseId}/applications/")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@accessValidator.isUser(#courseId, #principal.name)")
    fun applyOnCourse(
        @PathVariable courseId: Long,
        principal: Principal,
    ) = courseService.applyOnCourse(courseId, principal.name.toLong())

    @DeleteMapping("/{courseId}/applications/")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@accessValidator.isApplicant(#courseId, #principal.name)")
    fun deleteApplicationOnCourse(
        @PathVariable courseId: Long,
        principal: Principal,
    ) = courseService.deleteApplicationOnCourse(courseId, principal.name.toLong())

    @PostMapping("/{courseId}/applications/{userId}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@accessValidator.isSupervisor(#courseId, #principal.name)")
    fun confirmApplicationOnCourse(
        @PathVariable courseId: Long,
        @PathVariable userId: Long,
        principal: Principal,
    ) = courseService.confirmApplicationOnCourse(courseId, principal.name.toLong(), userId)

    @DeleteMapping("/{courseId}/applications/{userId}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@accessValidator.isSupervisor(#courseId, #principal.name)")
    fun rejectApplicationOnCourse(
        @PathVariable courseId: Long,
        @PathVariable userId: Long,
        principal: Principal,
    ) = courseService.rejectApplicationOnCourse(courseId, principal.name.toLong(), userId)

    @DeleteMapping("/{courseId}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@accessValidator.isSupervisor(#courseId, #principal.name)")
    fun deleteCourse(
        @PathVariable courseId: Long,
        principal: Principal,
    ) = courseService.deleteCourse(courseId, principal.name.toLong())

    @DeleteMapping("/{courseId}/student/")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@accessValidator.isStudent(#courseId, #principal.name)")
    fun leaveCourse(
        @PathVariable courseId: Long,
        principal: Principal,
    ) = courseService.deleteStudentFromCourse(courseId, principal.name.toLong())

    @DeleteMapping("/{courseId}/student/{userId}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@accessValidator.isSupervisor(#courseId, #principal.name)")
    fun deleteStudentFromCourse(
        @PathVariable courseId: Long,
        @PathVariable userId: Long,
        principal: Principal,
    ) = courseService.deleteStudentFromCourse(courseId, userId)

    @PostMapping("/{courseId}/notes/")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("@accessValidator.isSupervisor(#courseId, #principal.name)")
    fun createNote(
        @PathVariable courseId: Long,
        @RequestBody @Valid note: NoteCreateRequestDTO,
        principal: Principal,
    ) = courseService.createNote(courseId, note)

    @DeleteMapping("/{courseId}/notes/{noteId}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@accessValidator.isSupervisor(#courseId, #principal.name)")
    fun deleteNote(
        @PathVariable courseId: Long,
        @PathVariable noteId: Long,
        principal: Principal,
    ) = courseService.deleteNote(courseId, noteId)

    @GetMapping("/{courseId}/attachments/{attachmentId}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@accessValidator.isCourseMember(#courseId, #principal.name)")
    fun getAttachment(
        @PathVariable courseId: Long,
        @PathVariable attachmentId: Long,
        principal: Principal,
        request: HttpServletRequest,
    ): ResponseEntity<*> {
        val resource = courseService.getAttachment(courseId, attachmentId)
        val contentType = request.servletContext.getMimeType(resource.file.absolutePath)
        val filename = resource.filename?.substringAfter("-") as String
        return ResponseEntity.status(HttpStatus.OK)
            .contentType(MediaType.parseMediaType(contentType))
            .contentLength(resource.file.length())
            .header(
                HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename*=UTF-8''${StringEscapeUtils.escapeJava(filename)}"
            )
            .body(InputStreamResource(FileInputStream(resource.file)))
    }

    @PostMapping("/{courseId}/attachments/")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("@accessValidator.isSupervisor(#courseId, #principal.name)")
    fun addAttachment(
        @PathVariable courseId: Long,
        @RequestParam("file") file: MultipartFile,
        @RequestParam("date") date: String,
        principal: Principal,
    ) = courseService.addAttachment(courseId, file, date)

    @DeleteMapping("/{courseId}/attachments/{attachmentId}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@accessValidator.isSupervisor(#courseId, #principal.name)")
    fun deleteAttachment(
        @PathVariable courseId: Long,
        @PathVariable attachmentId: Long,
        principal: Principal,
    ) = courseService.deleteAttachment(courseId, attachmentId)

    @PostMapping("/{courseId}/activities/")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("@accessValidator.isSupervisor(#courseId, #principal.name)")
    fun createCourseActivity(
        @PathVariable courseId: Long,
        @RequestBody @Valid activity: ActivityCreateRequestDTO,
        principal: Principal,
    ) = courseService.createCourseActivity(courseId, activity)

    @DeleteMapping("/{courseId}/activities/{activityId}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@accessValidator.isSupervisor(#courseId, #principal.name)")
    fun deleteCourseActivity(
        @PathVariable courseId: Long,
        @PathVariable activityId: Long,
        principal: Principal,
    ) = courseService.deleteCourseActivity(courseId, activityId)

    @PostMapping("/{courseId}/activities/{activityId}/replies/")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("@accessValidator.isStudent(#courseId, #principal.name)")
    fun createCourseActivityReply(
        @PathVariable courseId: Long,
        @PathVariable activityId: Long,
        @RequestParam(name = "file") file: MultipartFile,
        @RequestParam(name = "date") date: String,
        principal: Principal,
    ) =
        courseService.createCourseActivityReply(
            courseId,
            activityId,
            principal.name.toLong(),
            file,
            date
        )

    @PutMapping("/{courseId}/activities/{activityId}/replies/{activityReplyId}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@accessValidator.isSupervisor(#courseId, #principal.name)")
    fun resolveCourseActivityReply(
        @PathVariable courseId: Long,
        @PathVariable activityId: Long,
        @PathVariable activityReplyId: Long,
        @RequestBody @Valid activityReplyResolveRequest: ActivityReplyResolveRequestDTO,
        principal: Principal,
    ) =
        courseService.resolveCourseActivityReply(
            courseId,
            activityId,
            activityReplyId,
            activityReplyResolveRequest
        )

    @DeleteMapping("/{courseId}/activities/{activityId}/replies/{activityReplyId}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize(
        "@accessValidator.isActivityReplyOwner(#courseId, #activityId, #activityReplyId, #principal.name)"
    )
    fun deleteCourseActivityReply(
        @PathVariable courseId: Long,
        @PathVariable activityId: Long,
        @PathVariable activityReplyId: Long,
        principal: Principal,
    ) = courseService.deleteCourseActivityReply(courseId, activityId, activityReplyId)
}
