\c courses;

set search_path to courses;

insert into tags (title)
values ('Программирование');
insert into tags (title)
values ('Математика');
insert into tags (title)
values ('Биология');
insert into tags (title)
values ('Иностранный язык');
insert into tags (title)
values ('Физика');
insert into tags (title)
values ('Химия');
insert into tags (title)
values ('Экономика');

insert into courses (title, supervisor, status, starting_date, finishing_date, open, tag_id, description)
values ('ООП', 3, 1, '2022-04-01', '2022-06-02', false, 1, 'Курс по ООП');
insert into courses (title, supervisor, status, starting_date, finishing_date, open, tag_id, description)
values ('Дифференциальные уравнения', 1, 1, '2022-03-01', '2022-07-30', false, 2,
        'Курс по дифференциальным уравнениям');
insert into courses (title, supervisor, status, starting_date, finishing_date, open, tag_id, description)
values ('Биоинженерия', 5, 1, '2022-05-01', '2022-09-01', false, 3, 'Курс по биоинженерии');
insert into courses (title, supervisor, status, starting_date, finishing_date, open, tag_id, description)
values ('Английский язык', 4, 1, '2022-01-30', '2022-06-01', false, 4, 'Курс по английскому языку');
insert into courses (title, supervisor, status, starting_date, finishing_date, open, tag_id, description)
values ('Волны и оптика', 2, 1, '2021-12-01', '2022-06-01', true, 5, 'Курс по волнам и оптике');
insert into courses (title, supervisor, status, starting_date, finishing_date, open, tag_id, description)
values ('Органическая химия', 5, 1, '2022-04-01', '2022-08-01', true, 6, 'Курс по органической химии');
insert into courses (title, supervisor, status, starting_date, finishing_date, open, tag_id, description)
values ('Микроэкономика', 6, 1, '2022-03-30', '2022-08-01', true, 7, 'Курс по микроэкономике');
insert into courses (title, supervisor, status, starting_date, finishing_date, open, tag_id, description)
values ('Макроэкономика', 6, 2, '2021-10-01', '2022-03-01', true, 7, 'Курс по макроэкономике');

insert into user_courses
values (1);
insert into user_courses
values (2);
insert into user_courses
values (3);
insert into user_courses
values (4);
insert into user_courses
values (5);
insert into user_courses
values (6);
insert into user_courses
values (7);

insert into course_lessons_days
values (1, 0);
insert into course_lessons_days
values (1, 2);
insert into course_lessons_days
values (1, 4);
insert into course_lessons_days
values (2, 1);
insert into course_lessons_days
values (3, 2);
insert into course_lessons_days
values (4, 5);
insert into course_lessons_days
values (5, 2);
insert into course_lessons_days
values (5, 5);
insert into course_lessons_days
values (6, 2);
insert into course_lessons_days
values (6, 4);
insert into course_lessons_days
values (7, 1);
insert into course_lessons_days
values (7, 3);
insert into course_lessons_days
values (7, 0);
insert into course_lessons_days
values (7, 3);

insert into course_applicants
values (1, 4);
insert into course_applicants
values (2, 3);
insert into course_applicants
values (4, 1);
insert into course_applicants
values (4, 6);

insert into course_students
values (1, 1);
insert into course_students
values (3, 4);
insert into course_students
values (5, 5);
insert into course_students
values (6, 7);
insert into course_students
values (7, 3);
insert into course_students
values (8, 1);

insert into user_courses_applied_courses
values (2, 1);
insert into user_courses_applied_courses
values (3, 2);
insert into user_courses_applied_courses
values (1, 4);
insert into user_courses_applied_courses
values (6, 4);

insert into user_courses_attended_courses
values (1, 1);
insert into user_courses_attended_courses
values (4, 3);
insert into user_courses_attended_courses
values (5, 5);
insert into user_courses_attended_courses
values (7, 6);
insert into user_courses_attended_courses
values (3, 7);

insert into user_courses_supervised_courses
values (1, 2);
insert into user_courses_supervised_courses
values (2, 5);
insert into user_courses_supervised_courses
values (3, 1);
insert into user_courses_supervised_courses
values (4, 4);
insert into user_courses_supervised_courses
values (5, 3);
insert into user_courses_supervised_courses
values (5, 6);
insert into user_courses_supervised_courses
values (6, 7);
insert into user_courses_supervised_courses
values (6, 8);

insert into user_courses_finished_courses
values (1, 8);