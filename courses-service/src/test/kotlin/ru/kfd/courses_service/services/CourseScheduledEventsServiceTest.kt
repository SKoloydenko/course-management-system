package ru.kfd.courses_service.services

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.ext.ScriptUtils
import org.testcontainers.jdbc.JdbcDatabaseDelegate
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import ru.kfd.courses_service.models.Course
import ru.kfd.courses_service.models.CourseStatus
import ru.kfd.courses_service.models.UserCourses
import ru.kfd.courses_service.repositories.CourseRepository
import ru.kfd.courses_service.repositories.TagRepository
import ru.kfd.courses_service.repositories.UserCoursesRepository
import java.time.DayOfWeek
import java.time.LocalDate
import kotlin.test.assertEquals

@Testcontainers
@SpringBootTest
internal class CourseScheduledEventsServiceTest {

    @Autowired lateinit var tagRepository: TagRepository

    @Autowired lateinit var courseRepository: CourseRepository

    @Autowired lateinit var userCoursesRepository: UserCoursesRepository

    @Autowired lateinit var courseScheduledEventsService: CourseScheduledEventsService

    companion object {
        @Container
        val postgreSQLContainer =
            PostgreSQLContainer<Nothing>("postgres:latest").apply {
                withUsername("root")
                withPassword("root")
                withDatabaseName("courses-test")
                withUrlParam("currentSchema", "courses-test")
            }

        @JvmStatic
        @DynamicPropertySource
        fun properties(registry: DynamicPropertyRegistry) {
            registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl)
            registry.add("spring.datasource.password", postgreSQLContainer::getPassword)
            registry.add("spring.datasource.username", postgreSQLContainer::getUsername)
            registry.add("spring.jpa.properties.hibernate.enable_lazy_load_no_trans") { "true" }
        }

        const val tagId1 = 1L
    }

    @BeforeEach
    fun beforeEach() {
        val delegate = JdbcDatabaseDelegate(postgreSQLContainer, "")
        ScriptUtils.runInitScript(delegate, "schema.sql")
        ScriptUtils.runInitScript(delegate, "data.sql")
    }

    @Nested
    inner class StartCourse {

        @Test
        fun `when course is created and should start then start course`() {
            // given
            val supervisorId = 1L
            val course =
                Course(
                    title = "Kotlin",
                    supervisor = supervisorId,
                    status = CourseStatus.CREATED,
                    startingDate = LocalDate.now(),
                    finishingDate = LocalDate.parse("2020-01-01"),
                    lessonsDays =
                        mutableSetOf(DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY),
                    open = true,
                    tag = tagRepository.findById(tagId1).get(),
                    description = "Description",
                )
            val courseId = courseRepository.save(course).id as Long
            // when
            courseScheduledEventsService.startCourses()
            // then
            val updatedCourse = courseRepository.findById(courseId).get()
            assertEquals(CourseStatus.ACTIVE, updatedCourse.status)
        }

        @Test
        fun `when course should not start then nothing`() {
            // given
            val supervisorId = 1L
            val course =
                Course(
                    title = "Kotlin",
                    supervisor = supervisorId,
                    status = CourseStatus.CREATED,
                    startingDate = LocalDate.now().plusDays(1),
                    finishingDate = LocalDate.parse("2020-01-01"),
                    lessonsDays =
                        mutableSetOf(DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY),
                    open = true,
                    tag = tagRepository.findById(tagId1).get(),
                    description = "Description",
                )
            val courseId = courseRepository.save(course).id as Long
            // when
            courseScheduledEventsService.startCourses()
            // then
            val updatedCourse = courseRepository.findById(courseId).get()
            assertEquals(CourseStatus.CREATED, updatedCourse.status)
        }
    }

    @Nested
    inner class FinishCourse {

        @Test
        fun `when course is in progress and should finish then finish course`() {
            // given
            val supervisorId = 1L
            val course =
                Course(
                    title = "OOP",
                    supervisor = supervisorId,
                    status = CourseStatus.ACTIVE,
                    startingDate = LocalDate.parse("2020-01-01"),
                    finishingDate = LocalDate.now(),
                    lessonsDays =
                        mutableSetOf(DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY),
                    open = true,
                    tag = tagRepository.findById(tagId1).get(),
                    description = "Description",
                )
            val courseId = courseRepository.save(course).id as Long
            val supervisorCourses =
                UserCourses(
                    user = supervisorId,
                    supervisedCourses = mutableSetOf(course),
                )
            userCoursesRepository.save(supervisorCourses)
            // when
            courseScheduledEventsService.finishCourses()
            // then
            val updatedCourse = courseRepository.findById(courseId).get()
            assertEquals(CourseStatus.FINISHED, updatedCourse.status)
            val updatedUserCourses = userCoursesRepository.findById(supervisorId).get()
            val supervisedCoursesIds =
                updatedUserCourses.supervisedCourses.map { it.id as Long }.toMutableSet()
            val finishedCoursesIds =
                updatedUserCourses.finishedCourses.map { it.id as Long }.toMutableSet()
            assertEquals(mutableSetOf(), supervisedCoursesIds)
            assertEquals(mutableSetOf(courseId), finishedCoursesIds)
        }

        @Test
        fun `when course should not end then nothing`() {
            // given
            val supervisorId = 1L
            val course =
                Course(
                    title = "OOP",
                    supervisor = supervisorId,
                    status = CourseStatus.ACTIVE,
                    startingDate = LocalDate.parse("2020-01-01"),
                    finishingDate = LocalDate.now().plusDays(1),
                    lessonsDays =
                        mutableSetOf(DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY),
                    open = true,
                    tag = tagRepository.findById(tagId1).get(),
                    description = "Description",
                )
            val courseId = courseRepository.save(course).id as Long
            val supervisorCourses =
                UserCourses(
                    user = supervisorId,
                    supervisedCourses = mutableSetOf(course),
                )
            userCoursesRepository.save(supervisorCourses)
            // when
            courseScheduledEventsService.finishCourses()
            // then
            val updatedCourse = courseRepository.findById(courseId).get()
            assertEquals(CourseStatus.ACTIVE, updatedCourse.status)
            val updatedUserCourses = userCoursesRepository.findById(supervisorId).get()
            val supervisedCoursesIds =
                updatedUserCourses.supervisedCourses.map { it.id as Long }.toMutableSet()
            val finishedCoursesIds =
                updatedUserCourses.finishedCourses.map { it.id as Long }.toMutableSet()
            assertEquals(mutableSetOf(courseId), supervisedCoursesIds)
            assertEquals(mutableSetOf(), finishedCoursesIds)
        }
    }
}
