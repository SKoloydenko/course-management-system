package ru.kfd.courses_service

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.springframework.test.web.servlet.*
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.ext.ScriptUtils
import org.testcontainers.jdbc.JdbcDatabaseDelegate
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import ru.kfd.courses_service.clients.UsersServiceClient
import ru.kfd.courses_service.dto.requests.CourseCreateRequestDTO
import ru.kfd.courses_service.dto.responses.*
import ru.kfd.courses_service.models.Attachment
import ru.kfd.courses_service.repositories.AttachmentRepository
import ru.kfd.courses_service.repositories.CourseRepository
import ru.kfd.courses_service.repositories.UserCoursesRepository
import java.nio.file.Files
import java.time.DayOfWeek
import java.time.LocalDate
import kotlin.io.path.Path
import kotlin.test.assertEquals

@Testcontainers
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
internal class CoursesServiceIntegrationTest {

    @Autowired lateinit var courseRepository: CourseRepository

    @Autowired lateinit var userCoursesRepository: UserCoursesRepository

    @Autowired lateinit var attachmentRepository: AttachmentRepository

    @MockkBean lateinit var usersServiceClient: UsersServiceClient

    @Autowired lateinit var mockMvc: MockMvc

    companion object {
        @Container
        val postgreSQLContainer =
            PostgreSQLContainer<Nothing>("postgres:latest").apply {
                withUsername("root")
                withPassword("root")
                withDatabaseName("courses-test")
                withUrlParam("currentSchema", "courses-test")
            }

        @JvmStatic
        @DynamicPropertySource
        fun properties(registry: DynamicPropertyRegistry) {
            registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl)
            registry.add("spring.datasource.password", postgreSQLContainer::getPassword)
            registry.add("spring.datasource.username", postgreSQLContainer::getUsername)
            registry.add("spring.jpa.properties.hibernate.enable_lazy_load_no_trans") { "true" }
        }

        const val courseId1 = 1L
        const val courseId2 = 2L
        const val courseId3 = 3L

        const val userId1 = 1L
        val user1 =
            UserResponseDTO(
                userId = userId1,
                firstname = "User1",
                lastname = "User1",
                universityId = 1,
            )
        const val userId2 = 2L
        val user2 =
            UserResponseDTO(
                userId = userId2,
                firstname = "User2",
                lastname = "User2",
                universityId = 1,
            )
        const val userId3 = 3L
        val user3 =
            UserResponseDTO(
                userId = userId3,
                firstname = "User3",
                lastname = "User3",
                universityId = 1,
            )
        const val userId4 = 4L
        val user4 =
            UserResponseDTO(
                userId = userId4,
                firstname = "User4",
                lastname = "User4",
                universityId = 1,
            )
        const val userId5 = 5L
        val user5 =
            UserResponseDTO(
                userId = userId5,
                firstname = "User5",
                lastname = "User5",
                universityId = 1,
            )
        const val userId6 = 6L
        val user6 =
            UserResponseDTO(
                userId = userId6,
                firstname = "User6",
                lastname = "User6",
                universityId = 1,
            )
        const val tagId1 = 1L
        val tag1 = TagResponseDTO(tagId = tagId1, title = "Programing")
        const val tagId2 = 2L
        val tag2 = TagResponseDTO(tagId = tagId2, title = "Math")
        const val tagId3 = 3L
        val tag3 = TagResponseDTO(tagId = tagId3, title = "Biology")
        const val tagId4 = 4L
        val tag4 = TagResponseDTO(tagId = tagId4, title = "Chemistry")
    }

    @BeforeEach
    fun beforeEach() {
        val delegate = JdbcDatabaseDelegate(postgreSQLContainer, "")
        ScriptUtils.runInitScript(delegate, "schema.sql")
        ScriptUtils.runInitScript(delegate, "data.sql")
        every { usersServiceClient.getUserById(userId1) } returns user1
        every { usersServiceClient.getUserById(userId2) } returns user2
        every { usersServiceClient.getUserById(userId3) } returns user3
        every { usersServiceClient.getUserById(userId4) } returns user4
        every { usersServiceClient.getUserById(userId5) } returns user5
        every { usersServiceClient.getUserById(userId6) } returns user6
        every { usersServiceClient.getUsersByIds(any()) } answers { callOriginal() }
    }

    @Nested
    inner class GetCourse {

        @WithMockUser(userId3.toString())
        @Test
        fun `when course exists and user is supervisor then get course for supervisor`() {
            // given
            val courseId = courseId1
            val courseResponse =
                SupervisorCourseResponseDTO(
                    courseId = courseId,
                    title = "OOP",
                    supervisor = user3,
                    startingDate = "2019-01-01",
                    finishingDate = "2020-01-01",
                    lessonsDays =
                        setOf(
                            DayOfWeek.MONDAY.value,
                            DayOfWeek.TUESDAY.value,
                            DayOfWeek.WEDNESDAY.value
                        ),
                    open = true,
                    applicants = setOf(),
                    students = setOf(user4, user5),
                    tagId = tagId1,
                    description = "Description",
                    notes = setOf(),
                    attachments = setOf(),
                    activities = setOf(),
                )
            // when
            mockMvc.get("/api/courses/$courseId")
                // then
                .andExpect {
                    status { isOk }
                    content { string(jacksonObjectMapper().writeValueAsString(courseResponse)) }
                }
        }

        @WithMockUser(userId4.toString())
        @Test
        fun `when course exists and user is student then get course for student`() {
            // given
            val courseId = courseId1
            val courseResponse =
                StudentCourseResponseDTO(
                    courseId = courseId,
                    title = "OOP",
                    supervisor = user3,
                    startingDate = "2019-01-01",
                    finishingDate = "2020-01-01",
                    lessonsDays =
                        setOf(
                            DayOfWeek.MONDAY.value,
                            DayOfWeek.TUESDAY.value,
                            DayOfWeek.WEDNESDAY.value
                        ),
                    open = true,
                    students = setOf(user4, user5),
                    tagId = tagId1,
                    description = "Description",
                    notes = setOf(),
                    attachments = setOf(),
                    activities = setOf(),
                )
            // when
            mockMvc.get("/api/courses/$courseId")
                // then
                .andExpect {
                    status { isOk }
                    content { string(jacksonObjectMapper().writeValueAsString(courseResponse)) }
                }
        }

        @WithMockUser(userId1.toString())
        @Test
        fun `when course does not exist then not found`() {
            // given
            val courseId = 0
            // when
            mockMvc.get("/api/courses/$courseId")
                // then
                .andExpect { status { isNotFound } }
        }
    }

    @Nested
    inner class CreateCourse {

        @WithMockUser(userId1.toString())
        @Test
        fun `when course with this title does not exist for this user then create course`() {
            // given
            val courseId = 5L
            val courseRequest =
                CourseCreateRequestDTO(
                    title = "Новый курс",
                    startingDate = "2019-01-01",
                    finishingDate = "2020-01-01",
                    lessonsDays =
                        mutableSetOf(
                            DayOfWeek.MONDAY.value,
                            DayOfWeek.WEDNESDAY.value,
                            DayOfWeek.FRIDAY.value
                        ),
                    open = true,
                    tagId = tagId1,
                    description = "Описание"
                )
            val courseResponse =
                SupervisorCourseResponseDTO(
                    courseId = courseId,
                    title = "Новый курс",
                    supervisor = user1,
                    startingDate = "2019-01-01",
                    finishingDate = "2020-01-01",
                    lessonsDays =
                        mutableSetOf(
                            DayOfWeek.MONDAY.value,
                            DayOfWeek.WEDNESDAY.value,
                            DayOfWeek.FRIDAY.value
                        ),
                    open = true,
                    applicants = mutableSetOf(),
                    students = mutableSetOf(),
                    tagId = tagId1,
                    description = "Описание",
                    notes = setOf(),
                    attachments = mutableSetOf(),
                    activities = mutableSetOf(),
                )
            mockMvc
                .post("/api/courses/") {
                    contentType = MediaType.APPLICATION_JSON
                    content = jacksonObjectMapper().writeValueAsString(courseRequest)
                    accept = MediaType.APPLICATION_JSON
                }
                // then
                .andExpect {
                    status { isCreated }
                    content { string(jacksonObjectMapper().writeValueAsString(courseResponse)) }
                }
        }

        @WithMockUser(userId3.toString())
        @Test
        fun `when course with this title for this user exists then bad request`() {
            // given
            val courseRequest =
                CourseCreateRequestDTO(
                    title = "OOP",
                    startingDate = "2019-01-01",
                    finishingDate = "2020-01-01",
                    lessonsDays = mutableSetOf(1, 3, 5),
                    open = true,
                    tagId = tagId1,
                    description = "Description",
                )
            // when
            mockMvc
                .post("/api/courses/") {
                    contentType = MediaType.APPLICATION_JSON
                    content = jacksonObjectMapper().writeValueAsString(courseRequest)
                    accept = MediaType.APPLICATION_JSON
                }
                // then
                .andExpect { status { isBadRequest } }
        }
    }

    @Nested
    inner class ApplyOnCourse {

        @WithMockUser(userId2.toString())
        @Test
        fun `when course is open and user is correct then attend course`() {
            // given
            val courseId = courseId1
            val userId = userId2
            // when
            mockMvc.post("/api/courses/$courseId/applications/")
                // then
                .andExpect { status { isOk } }

            val updatedUserCourses = userCoursesRepository.findById(userId).get()
            val attendedCoursesIds =
                updatedUserCourses.attendedCourses.map { it.id as Long }.toMutableSet()
            assertEquals(mutableSetOf(courseId), attendedCoursesIds)
        }

        @WithMockUser(userId2.toString())
        @Test
        fun `when course is closed and user is correct then apply on course`() {
            // given
            val courseId = courseId3
            val userId = userId2
            // when
            mockMvc.post("/api/courses/$courseId/applications/")
                // then
                .andExpect { status { isOk } }

            val updatedUserCourses = userCoursesRepository.findById(userId).get()
            val appliedCoursesIds =
                updatedUserCourses.appliedCourses.map { it.id as Long }.toMutableSet()
            assertEquals(mutableSetOf(courseId), appliedCoursesIds)
        }

        @WithMockUser(userId3.toString())
        @Test
        fun `when user is supervisor then forbidden`() {
            // given
            val courseId = courseId1
            // when
            mockMvc.post("/api/courses/$courseId/applications/")
                // then
                .andExpect { status { isForbidden } }
        }

        @WithMockUser(userId6.toString())
        @Test
        fun `when user is applicant then forbidden`() {
            // given
            val courseId = courseId2
            // when
            mockMvc.post("/api/courses/$courseId/applications/")
                // then
                .andExpect { status { isForbidden } }
        }

        @WithMockUser(userId4.toString())
        @Test
        fun `when user is student then forbidden`() {
            // given
            val courseId = courseId1
            // when
            mockMvc.post("/api/courses/$courseId/applications/")
                // then
                .andExpect { status { isForbidden } }
        }
    }

    @Nested
    inner class DeleteApplicationOnCourse {

        @WithMockUser(userId6.toString())
        @Test
        fun `when course is closed and user is applicant then delete application`() {
            // given
            val courseId = courseId2
            val userId = userId6
            // when
            mockMvc.delete("/api/courses/$courseId/applications/")
                // then
                .andExpect { status { isOk } }

            val updatedUserCourses = userCoursesRepository.findById(userId).get()
            val appliedCoursesIds =
                updatedUserCourses.appliedCourses.map { it.id as Long }.toMutableSet()
            assertEquals(mutableSetOf(), appliedCoursesIds)
        }

        @WithMockUser(userId4.toString())
        @Test
        fun `when user is student then forbidden`() {
            // given
            val courseId = courseId1
            // when
            mockMvc.delete("/api/courses/$courseId/applications/")
                // then
                .andExpect { status { isForbidden } }
        }
    }

    @Nested
    inner class ConfirmApplicationOnCourse {

        @WithMockUser(userId2.toString())
        @Test
        fun `when user is supervisor then confirm application`() {
            // given
            val courseId = courseId2
            val userId = userId6
            // when
            mockMvc.post("/api/courses/$courseId/applications/$userId")
                // then
                .andExpect { status { isOk } }

            val updatedUserCourses = userCoursesRepository.findById(userId).get()
            val appliedCoursesIds =
                updatedUserCourses.appliedCourses.map { it.id as Long }.toMutableSet()
            val attendedCoursesIds =
                updatedUserCourses.attendedCourses.map { it.id as Long }.toMutableSet()
            assertEquals(mutableSetOf(), appliedCoursesIds)
            assertEquals(mutableSetOf(courseId), attendedCoursesIds)
        }

        @WithMockUser(userId3.toString())
        @Test
        fun `when user is not supervisor then forbidden`() {
            // given
            val courseId = courseId2
            val userId = userId6
            // when
            mockMvc.post("/api/courses/$courseId/applications/$userId")
                // then
                .andExpect { status { isForbidden } }
        }
    }

    @Nested
    inner class RejectApplicationOnCourse {

        @WithMockUser(userId2.toString())
        @Test
        fun `when user is supervisor then reject application`() {
            // given
            val courseId = courseId2
            val userId = userId6
            // when
            mockMvc.delete("/api/courses/$courseId/applications/$userId")
                // then
                .andExpect { status { isOk } }

            val updatedUserCourses = userCoursesRepository.findById(userId).get()
            val appliedCoursesIds =
                updatedUserCourses.appliedCourses.map { it.id as Long }.toMutableSet()
            val attendedCoursesIds =
                updatedUserCourses.attendedCourses.map { it.id as Long }.toMutableSet()
            assertEquals(mutableSetOf(), appliedCoursesIds)
            assertEquals(mutableSetOf(), attendedCoursesIds)
        }

        @WithMockUser(userId2.toString())
        @Test
        fun `when user is not supervisor then forbidden`() {
            // given
            val courseId = courseId1
            val userId = userId6
            // when
            mockMvc.delete("/api/courses/$courseId/applications/$userId")
                // then
                .andExpect { status { isForbidden } }
        }
    }

    @Nested
    inner class DeleteCourse {

        @WithMockUser(userId3.toString())
        @Test
        fun `when user is supervisor then delete course`() {
            // given
            val courseId = courseId1
            val userId = userId3
            // when
            mockMvc.delete("/api/courses/$courseId")
                // then
                .andExpect { status { isOk } }

            val updatedSupervisorCourses = userCoursesRepository.findById(userId).get()
            val supervisedCoursesIds =
                updatedSupervisorCourses.supervisedCourses.map { it.id as Long }.toMutableSet()
            assertEquals(mutableSetOf(), supervisedCoursesIds)
            val updatedUserCourses = userCoursesRepository.findById(userId).get()
            val attendedCoursesIds =
                updatedUserCourses.attendedCourses.map { it.id as Long }.toMutableSet()
            assertEquals(mutableSetOf(), attendedCoursesIds)
        }

        @WithMockUser(userId2.toString())
        @Test
        fun `when user is not supervisor then forbidden`() {
            // given
            val courseId = courseId1
            // when
            mockMvc.delete("/api/courses/$courseId/")
                // then
                .andExpect { status { isForbidden } }
        }
    }

    @Nested
    inner class LeaveCourse {

        @WithMockUser(userId4.toString())
        @Test
        fun `when user is student then leave course`() {
            // given
            val courseId = courseId1
            val userId = userId4
            // when
            mockMvc.delete("/api/courses/$courseId/student/")
                // then
                .andExpect { status { isOk } }

            val updatedUserCourses = userCoursesRepository.findById(userId).get()
            val appliedCoursesIds =
                updatedUserCourses.appliedCourses.map { it.id as Long }.toMutableSet()
            val attendedCoursesIds =
                updatedUserCourses.attendedCourses.map { it.id as Long }.toMutableSet()
            assertEquals(mutableSetOf(), appliedCoursesIds)
            assertEquals(mutableSetOf(courseId2), attendedCoursesIds)
        }

        @WithMockUser(userId3.toString())
        @Test
        fun `when user is supervisor then forbidden`() {
            // given
            val courseId = courseId1
            // when
            mockMvc.delete("/api/courses/$courseId/student/")
                // then
                .andExpect { status { isForbidden } }
        }
    }

    @Nested
    inner class DeleteStudentFromCourse {

        @WithMockUser(userId3.toString())
        @Test
        fun `when user is supervisor then delete student from course`() {
            // given
            val courseId = courseId1
            val userId = userId4
            // when
            mockMvc.delete("/api/courses/$courseId/student/$userId")
                // then
                .andExpect { status { isOk } }

            val updatedUserCourses = userCoursesRepository.findById(userId).get()
            val appliedCoursesIds =
                updatedUserCourses.appliedCourses.map { it.id as Long }.toMutableSet()
            val attendedCoursesIds =
                updatedUserCourses.attendedCourses.map { it.id as Long }.toMutableSet()
            assertEquals(mutableSetOf(), appliedCoursesIds)
            assertEquals(mutableSetOf(courseId2), attendedCoursesIds)
        }

        @WithMockUser(userId2.toString())
        @Test
        fun `when user is not supervisor then forbidden`() {
            // given
            val courseId = courseId1
            val userId = userId4
            // when
            mockMvc.delete("/api/courses/$courseId/student/$userId")
                // then
                .andExpect { status { isForbidden } }
        }
    }

    @Nested
    inner class GetAttachment {

        @WithMockUser(userId3.toString())
        @Test
        fun `when file exists then get attachment`() {
            // given
            val courseId = courseId1
            val attachment = Attachment(name = "test.txt", date = LocalDate.now())
            val attachmentId = attachmentRepository.save(attachment).id
            val path = Path("/attachments/$attachmentId-${attachment.name}")
            Files.write(path, "test".toByteArray())
            // when
            mockMvc.get("/api/courses/$courseId/attachments/$attachmentId")
                // then
                .andExpect {
                    status { isOk }
                    content { string("test") }
                }
        }
    }

    @Nested
    inner class AddAttachment {
        @WithMockUser(userId3.toString())
        @Test
        fun `when request has file then add attachment`() {
            // given
            val courseId = courseId1
            val file =
                MockMultipartFile(
                    "file",
                    "test.txt",
                    MediaType.TEXT_PLAIN_VALUE,
                    "test".toByteArray()
                )
            val response =
                AttachmentResponseDTO(
                    fileId = 1,
                    name = "test.txt",
                    date = LocalDate.now().toString(),
                )
            // when
            mockMvc
                .multipart("/api/courses/$courseId/attachments/?date=${LocalDate.now()}") {
                    file(file)
                }
                // then
                .andExpect {
                    status { isCreated }
                    content { string(jacksonObjectMapper().writeValueAsString(setOf(response))) }
                }
        }
    }

    @Nested
    inner class DeleteAttachment {
        @WithMockUser(userId3.toString())
        @Test
        fun `when file id is correct then delete attachment`() {
            // given
            val courseId = courseId1
            val attachment =
                Attachment(
                    name = "test.txt",
                    date = LocalDate.now(),
                )
            val attachmentId = attachmentRepository.save(attachment).id
            val path = Path("/attachments/$attachmentId-${attachment.name}")
            Files.write(path, "test".toByteArray())
            val course = courseRepository.findById(courseId).get()
            course.addAttachment(attachment)
            courseRepository.save(course)
            // when
            mockMvc.delete("/api/courses/$courseId/attachments/$attachmentId")
                // then
                .andExpect {
                    // status { isOk }
                    content { string("") }
                }
        }
    }
}
