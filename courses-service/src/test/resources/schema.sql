drop schema if exists "courses-test" cascade;
create schema "courses-test";

create table tags
(
    id    bigserial primary key,
    title varchar(255) not null
);

create table courses
(
    id             serial primary key,
    title          varchar(40)  not null,
    supervisor     bigint       not null,
    status         int          not null,
    starting_date  date         not null,
    finishing_date date         not null,
    open           boolean      not null,
    tag_id         bigint       not null,
    description    varchar(255) not null,
    foreign key (tag_id) references tags (id)
);

create table course_lessons_days
(
    course_id    bigint not null,
    lessons_days int    not null,
    foreign key (course_id) references courses (id)
);

create table course_applicants
(
    course_id  bigint not null,
    applicants bigint not null,
    foreign key (course_id) references courses (id)
);

create table course_students
(
    course_id bigint not null,
    students  bigint not null,
    foreign key (course_id) references courses (id)
);

create table notes
(
    id        serial primary key,
    text      varchar(255) not null,
    date      date         not null,
    course_id bigint       not null,
    foreign key (course_id) references courses (id)
);

create table attachments
(
    id   bigserial primary key,
    name varchar(255) null,
    date date         not null
);

create table course_attachments
(
    course_id     bigint not null,
    attachment_id bigint not null,
    primary key (course_id, attachment_id),
    foreign key (attachment_id) references attachments (id),
    foreign key (course_id) references courses (id)
);

create table activities
(
    id             bigserial primary key,
    description    varchar(255) not null,
    finishing_time timestamp(6) not null,
    max_grade      bigint       not null,
    starting_time  timestamp(6) not null,
    title          varchar(255) not null,
    type           int          not null,
    course_id      bigint       not null,
    foreign key (course_id) references courses (id)
);

create table activity_reply
(
    id          bigserial primary key,
    grade       bigint null,
    status      int    null,
    student     bigint not null,
    activity_id bigint null,
    resource_id bigint null,
    foreign key (activity_id) references activities (id),
    foreign key (resource_id) references attachments (id)
);

create table user_courses
(
    "user" bigint primary key
);

create table user_courses_applied_courses
(
    user_id   bigint not null,
    course_id bigint not null,
    primary key (user_id, course_id),
    foreign key (user_id) references user_courses ("user"),
    foreign key (course_id) references courses (id)
);

create table user_courses_attended_courses
(
    user_id   bigint not null,
    course_id bigint not null,
    primary key (user_id, course_id),
    foreign key (user_id) references user_courses ("user"),
    foreign key (course_id) references courses (id)
);

create table user_courses_supervised_courses
(
    user_id   bigint not null,
    course_id bigint not null,
    primary key (user_id, course_id),
    foreign key (user_id) references user_courses ("user"),
    foreign key (course_id) references courses (id)
);

create table user_courses_finished_courses
(
    user_id   bigint not null,
    course_id bigint not null,
    primary key (user_id, course_id),
    foreign key (user_id) references user_courses ("user"),
    foreign key (course_id) references courses (id)
);
