set search_path to "courses-test";

insert into tags (title)
values ('Programing');
insert into tags (title)
values ('Math');
insert into tags (title)
values ('Biology');
insert into tags (title)
values ('Chemistry');

insert into courses (title, supervisor, status, starting_date, finishing_date, open, tag_id, description)
values ('OOP', 3, 1, '2019-01-01', '2020-01-01', true, 1, 'Description');
insert into courses (title, supervisor, status, starting_date, finishing_date, open, tag_id, description)
values ('Math', 2, 1, '2019-01-01', '2020-01-01', false, 2, 'Description');
insert into courses (title, supervisor, status, starting_date, finishing_date, open, tag_id, description)
values ('Biology', 1, 1, '2019-01-01', '2020-01-01', false, 3, 'Description');
insert into courses (title, supervisor, status, starting_date, finishing_date, open, tag_id, description)
values ('Chemistry', 1, 2, '2019-01-01', '2020-01-01', false, 4, 'Description');

insert into course_applicants
values (2, 6);

insert into course_students
values (1, 4);
insert into course_students
values (1, 5);

insert into course_lessons_days
values (1, 0);
insert into course_lessons_days
values (1, 1);
insert into course_lessons_days
values (1, 2);
insert into course_lessons_days
values (2, 0);
insert into course_lessons_days
values (2, 1);
insert into course_lessons_days
values (2, 2);
insert into course_lessons_days
values (3, 0);

insert into user_courses
values (1);
insert into user_courses
values (2);
insert into user_courses
values (3);
insert into user_courses
values (4);
insert into user_courses
values (5);
insert into user_courses
values (6);

insert into user_courses_applied_courses
values (6, 2);

insert into user_courses_attended_courses
values (4, 1);
insert into user_courses_attended_courses
values (4, 2);
insert into user_courses_attended_courses
values (5, 1);

insert into user_courses_supervised_courses
values (1, 3);
insert into user_courses_supervised_courses
values (2, 2);
insert into user_courses_supervised_courses
values (3, 1);

insert into user_courses_finished_courses
values (1, 4);
insert into user_courses_finished_courses
values (4, 4);
insert into user_courses_finished_courses
values (5, 4);
