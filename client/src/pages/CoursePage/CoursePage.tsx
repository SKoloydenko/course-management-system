import React, { useEffect } from "react";
import { Button, CourseView, Header, Loader, Spacer } from "components";
import { ButtonTheme } from "components/Button/Button";
import { useTypedSelector } from "hooks/useTypedSelector";
import { useParams } from "react-router-dom";
import { useActions } from "hooks/useActions";
import { SpacerAxis } from "components/Spacer/Spacer";
import { CourseTab } from "redux/types/courseTypes";
import style from "./CoursePage.module.scss";

const CoursePage: React.FC = () => {
	const { user } = useTypedSelector((state) => state.user);
	const { currentCourse, loading } = useTypedSelector((state) => state.course);
	const { getCourse, setTab } = useActions();
	const { courseId } = useParams();

	useEffect(() => {
		setTab(CourseTab.GENERAL);
		getCourse(Number(courseId) || 0);
	}, []);

	if (loading) {
		return (
			<div className={style.loader}>
				<Loader />
			</div>
		);
	}

	return (
		<div className={style.container}>
			<Header>
				{!user && (
					<div className={style.button}>
						<Button theme={ButtonTheme.DARK} text="Войти" link="/login" />
					</div>
				)}
				{!user && (
					<div className={style.button}>
						<Button
							theme={ButtonTheme.LIGHT}
							text="Регистрация"
							link="/register"
						/>
					</div>
				)}
				{/* {user && (
					<div className={style.button}>
						<Button
							theme={ButtonTheme.LIGHT}
							text="Личный кабинет"
							link="/self"
						/>
					</div>
				)} */}
			</Header>

			{currentCourse ? (
				<div className={style.course}>
					<CourseView course={currentCourse} />
					<Spacer size={120} axis={SpacerAxis.VERTICAL} />
				</div>
			) : (
				<div className={style.not_found}>
					К сожалению, такого курса не существует
				</div>
			)}
		</div>
	);
};

export default CoursePage;
