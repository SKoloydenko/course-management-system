import React, { useEffect, useState } from "react";
import {
	Button,
	CourseBoard,
	CreateCourseModal,
	Header,
	Input,
	Spacer,
} from "components";
import { ButtonTheme } from "components/Button/Button";
import { useActions } from "hooks/useActions";
import { useTypedSelector } from "hooks/useTypedSelector";
import { SpacerAxis } from "components/Spacer/Spacer";
import style from "./LandingPage.module.scss";

const LandingPage: React.FC = () => {
	const [query, setQuery] = useState<string>("");
	const { user } = useTypedSelector((state) => state.user);
	const { logout, closeModal } = useActions();

	useEffect(() => {
		closeModal();
	}, []);

	return (
		<div className={style.container}>
			<Header>
				{!user && (
					<div className={style.button}>
						<Button theme={ButtonTheme.DARK} text="Войти" link="/login" />
					</div>
				)}
				{!user && (
					<div className={style.button}>
						<Button
							theme={ButtonTheme.LIGHT}
							text="Регистрация"
							link="/register"
						/>
					</div>
				)}
				{/* {user && (
					<div className={style.button}>
						<Button
							theme={ButtonTheme.LIGHT}
							text="Личный кабинет"
							link="/self"
						/>
					</div>
				)} */}
				{user && (
					<div className={style.button}>
						<Button theme={ButtonTheme.DARK} text="Выйти" onClick={logout} />
					</div>
				)}
			</Header>

			<div className={style.section}>
				<div className={style.search_bar}>
					<div className={style.input}>
						<Input
							type="text"
							placeholder="Поиск"
							value={query}
							setValue={(event) => setQuery(event.target.value)}
						/>
					</div>
				</div>
				<Spacer size={30} axis={SpacerAxis.VERTICAL} />
				<div className={style.create_course}>
					<CreateCourseModal />
				</div>
				<Spacer size={30} axis={SpacerAxis.VERTICAL} />
				<div className={style.board}>
					<CourseBoard query={query} />
				</div>
				<Spacer size={120} axis={SpacerAxis.VERTICAL} />
			</div>
		</div>
	);
};

export default LandingPage;
