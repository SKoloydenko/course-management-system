export { default as LandingPage } from "./LandingPage";
export { default as LoginPage } from "./LoginPage";
export { default as RegisterPage } from "./RegisterPage";
export { default as CoursePage } from "./CoursePage";
export { default as ProfilePage } from "./ProfilePage";
export { default as NotFoundPage } from "./NotFoundPage";
