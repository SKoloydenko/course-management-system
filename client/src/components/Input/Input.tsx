import React from "react";
import style from "./Input.module.scss";

interface InputProps {
	type: string;
	placeholder?: string;
	value?: string;
	setValue: (event: any) => void;
	required?: boolean;
	minLength?: number;
	maxLength?: number;
}

const Input: React.FC<InputProps> = ({
	type,
	placeholder,
	value,
	setValue,
	required = false,
	minLength,
	maxLength,
}) => {
	const pattern =
		type === "email"
			? "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$"
			: undefined;

	if (type === "file") {
		return (
			<input
				type={type}
				className={style.file}
				placeholder={placeholder}
				autoCorrect="off"
				autoComplete="off"
				spellCheck="false"
				onChange={setValue}
				required={required}
			/>
		);
	} else {
		return (
			<input
				type={type}
				className={style.input}
				placeholder={placeholder}
				autoCorrect="off"
				autoComplete="off"
				spellCheck="false"
				value={value}
				onChange={setValue}
				required={required}
				pattern={pattern}
				minLength={minLength}
				maxLength={maxLength}
			/>
		);
	}
};

export default Input;
