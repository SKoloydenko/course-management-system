import React, { useState } from "react";
import { ButtonTheme } from "components/Button/Button";
import {
	Button,
	DateSelector,
	Input,
	Spacer,
	DayOfWeekSelector,
	TagSelector,
	TextArea,
	Message,
} from "components";
import { SpacerAxis } from "components/Spacer/Spacer";
import { CourseCreateRequest } from "services/courseService";
import { useActions } from "hooks/useActions";
import { useTypedSelector } from "hooks/useTypedSelector";
import { oneDay } from "constants/date";
import style from "./CreateCourseModal.module.scss";

interface CourseCreateForm {
	title: string;
	open: boolean;
	startingDate: Date;
	finishingDate: Date;
	lessonsDays: number[];
	tagId: number;
	description: string;
}

const initialFormState = {
	title: "",
	open: true,
	startingDate: new Date(),
	finishingDate: new Date(new Date().getTime() + oneDay),
	lessonsDays: [],
	tagId: 0,
	description: "",
};

const CreateCourseModal: React.FC = () => {
	const [form, setForm] = useState<CourseCreateForm>(initialFormState);
	const { error } = useTypedSelector((state) => state.course);
	const { open } = useTypedSelector((state) => state.modal);
	const { createCourse, openModal, closeModal } = useActions();

	const handleCourseCreation = (event: any) => {
		event.preventDefault();
		const courseCreateRequest: CourseCreateRequest = {
			title: form.title,
			open: form.open,
			startingDate: form.startingDate
				.toLocaleDateString()
				.split(".")
				.reverse()
				.join("-"),
			finishingDate: form.finishingDate
				.toLocaleDateString()
				.split(".")
				.reverse()
				.join("-"),
			lessonsDays: form.lessonsDays,
			tagId: form.tagId,
			description: form.description,
		};
		createCourse(courseCreateRequest);
	};

	return (
		<div className={style.container}>
			<div className={style.title}>Создайте свой курс!</div>
			<Spacer size={30} axis={SpacerAxis.VERTICAL} />
			<div className={style.button}>
				<Button
					theme={ButtonTheme.DARK}
					text="Создать"
					onClick={() => {
						openModal();
					}}
				/>
			</div>

			{open && (
				<div className={style.modal}>
					<div className={style.modal_content}>
						<div className={style.modal_header}>
							<div className={style.modal_title}>Создайте курс</div>
							<img
								src="/decline-dark.png"
								alt=""
								className={style.close}
								onClick={() => {
									setForm(initialFormState);
									closeModal();
								}}
							/>
						</div>

						<form className={style.form} onSubmit={handleCourseCreation}>
							<Spacer size={20} axis={SpacerAxis.VERTICAL} />
							<div className={style.input}>
								<Input
									type="text"
									placeholder="Введите название курса"
									value={form.title}
									setValue={(event) =>
										setForm({ ...form, title: event.target.value })
									}
									required={true}
									maxLength={40}
								/>
							</div>
							<Spacer size={20} axis={SpacerAxis.VERTICAL} />
							<div className={style.button}>
								<Button
									type="button"
									theme={
										form.open ? ButtonTheme.DARK : ButtonTheme.LIGHT_BORDER
									}
									text={form.open ? "Открытый" : "Закрытый"}
									onClick={() => setForm({ ...form, open: !form.open })}
								/>
							</div>
							<Spacer size={20} axis={SpacerAxis.VERTICAL} />
							<div className={style.date}>
								<DateSelector
									date={form.startingDate}
									setDate={(value: Date) =>
										setForm({
											...form,
											startingDate: value,
										})
									}
									placeholder="Дата начала курса"
									minDate={new Date()}
								/>
							</div>
							<Spacer size={20} axis={SpacerAxis.VERTICAL} />
							<div className={style.date}>
								<DateSelector
									date={form.finishingDate}
									setDate={(value: Date) =>
										setForm({
											...form,
											finishingDate: value,
										})
									}
									placeholder="Дата конца курса"
									minDate={new Date(form.startingDate.getTime() + oneDay)}
								/>
							</div>
							<Spacer size={20} axis={SpacerAxis.VERTICAL} />
							<DayOfWeekSelector
								value={form.lessonsDays}
								setValue={(value: number[]) =>
									setForm({ ...form, lessonsDays: value })
								}
								placeholder="Выберите учебные дни недели"
							/>
							<Spacer size={20} axis={SpacerAxis.VERTICAL} />
							<TagSelector
								value={form.tagId}
								setValue={(value: number) => setForm({ ...form, tagId: value })}
							/>
							<Spacer size={20} axis={SpacerAxis.VERTICAL} />
							<div className={style.textarea}>
								<TextArea
									placeholder="Введите описание"
									value={form.description}
									setValue={(value) => setForm({ ...form, description: value })}
									required={false}
									maxLength={255}
								/>
							</div>
							<Message message={error} size={60} />
							<div className={style.button}>
								<Button type="submit" theme={ButtonTheme.DARK} text="Создать" />
							</div>
						</form>
					</div>
				</div>
			)}
		</div>
	);
};

export default CreateCourseModal;
