import React, { useState } from "react";
import {
	Button,
	Input,
	Message,
	Select,
	Spacer,
	MatchingInputs,
} from "components";
import { ButtonTheme } from "../Button/Button";
import { SpacerAxis } from "../Spacer/Spacer";
import { useActions } from "hooks/useActions";
import { useTypedSelector } from "hooks/useTypedSelector";
import { RegisterRequest } from "services/authService";
import style from "./RegisterForm.module.scss";

const RegisterForm: React.FC = () => {
	const { error } = useTypedSelector((state) => state.auth);
	const { universities } = useTypedSelector((state) => state.university);
	const { register } = useActions();
	const [form, setForm] = useState<RegisterRequest>({
		email: "",
		firstname: "",
		lastname: "",
		password: "",
		universityId: 0,
	});
	const [confirmPassword, setConfirmPassword] = useState<string>("");

	const handleRegister = (event: any) => {
		event.preventDefault();
		register(form);
	};

	return (
		<form
			className={`${style.container} ${error ? style.error : ""}`}
			onSubmit={(event) => handleRegister(event)}
		>
			<div className={style.title}>Создайте аккаунт</div>
			<Spacer size={45} axis={SpacerAxis.VERTICAL} />
			<Input
				type="text"
				placeholder="Введите имя"
				value={form.firstname}
				setValue={(event) =>
					setForm({ ...form, firstname: event.target.value })
				}
				required={true}
			/>
			<Spacer size={45} axis={SpacerAxis.VERTICAL} />
			<Input
				type="text"
				placeholder="Введите фамилию"
				value={form.lastname}
				setValue={(event) => setForm({ ...form, lastname: event.target.value })}
				required={true}
			/>
			<Spacer size={45} axis={SpacerAxis.VERTICAL} />
			<Input
				type="email"
				placeholder="Введите email"
				value={form.email}
				setValue={(event) => setForm({ ...form, email: event.target.value })}
				required={true}
			/>
			<Spacer size={45} axis={SpacerAxis.VERTICAL} />
			<MatchingInputs
				type="password"
				placeholders={["Введите пароль", "Подтвердите пароль"]}
				input={form.password}
				changeInput={(event) =>
					setForm({ ...form, password: event.target.value })
				}
				matchingInput={confirmPassword}
				changeMatchingInput={(event) => setConfirmPassword(event.target.value)}
				message="Пароли должны совпадать"
				required={true}
			/>
			<Spacer size={45} axis={SpacerAxis.VERTICAL} />
			<Select
				value={form.universityId}
				options={universities.map((university) => ({
					id: university.universityId,
					value: university.title,
				}))}
				onChange={(event) =>
					setForm({
						...form,
						universityId: event.target.value,
					})
				}
				placeholder={"Выберите университет"}
				required={true}
			/>
			<Message message={error} size={60} />
			<Button
				type="submit"
				theme={ButtonTheme.DARK}
				text="Зарегистрироваться"
			/>
		</form>
	);
};

export default RegisterForm;
