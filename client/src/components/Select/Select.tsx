import React from "react";
import style from "./Select.module.scss";

interface SelectProps {
	value: number;
	options: {
		id: number;
		value: string;
	}[];
	onChange: (event: any) => void;
	placeholder: string;
	required: boolean;
}

const Select: React.FC<SelectProps> = ({
	value,
	options,
	onChange,
	placeholder,
	required,
}) => {
	return (
		<select
			value={value ? value : undefined}
			className={style.select}
			onChange={onChange}
			required={required}
			defaultValue=""
		>
			<option value="" disabled hidden>
				{placeholder}
			</option>
			{options.map((option) => (
				<option className={style.option} key={option.id} value={option.id}>
					{option.value}
				</option>
			))}
		</select>
	);
};

export default Select;
