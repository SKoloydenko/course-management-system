import React, { useEffect } from "react";
import { Loader } from "components";
import { useActions } from "hooks/useActions";
import { useTypedSelector } from "hooks/useTypedSelector";
import CourseCard from "./CourseCard";
import style from "./CourseBoard.module.scss";
import UserCourses from "./UserCourses";

interface CourseBoardProps {
	query: string;
}

const CourseBoard: React.FC<CourseBoardProps> = ({ query }) => {
	const { user } = useTypedSelector((state) => state.user);
	const { courses, loading } = useTypedSelector((state) => state.course);
	const { getCourses } = useActions();

	useEffect(() => {
		if (!courses.length) {
			getCourses();
		}
	}, []);

	const filteredCourses = courses.filter(
		(course) =>
			course.title.toLocaleLowerCase().includes(query.toLocaleLowerCase()) &&
			new Date() < new Date(course.finishingDate)
	);

	if (loading) {
		return <Loader />;
	}

	return (
		<div className={style.container}>
			{user?.userCourses && (
				<UserCourses userCourses={user.userCourses} query={query} />
			)}
			<div className={style.board}>
				{filteredCourses.map((course) => (
					<CourseCard
						key={course.courseId}
						courseId={course.courseId}
						title={course.title}
						supervisor={course.supervisor}
						open={course.open}
						tagId={course.tagId}
					/>
				))}
			</div>
		</div>
	);
};

export default CourseBoard;
