import React from "react";
import Spacer from "components/Spacer";
import { SpacerAxis } from "components/Spacer/Spacer";
import { Course } from "redux/types/courseTypes";
import CourseCard from "../CourseCard";
import style from "./UserCourses.module.scss";

interface UserCoursesProps {
	userCourses: {
		appliedCourses: Course[];
		attendedCourses: Course[];
		supervisedCourses: Course[];
		finishedCourses: Course[];
	};
	query: string;
}

const filterCourses = (courses: Course[], query: string): Course[] => {
	return courses.filter((course) =>
		course.title.toLocaleLowerCase().includes(query.toLocaleLowerCase())
	);
};

const UserCourses: React.FC<UserCoursesProps> = ({ userCourses, query }) => {
	const appliedCourses = filterCourses(userCourses.appliedCourses, query);
	const attendedCourses = filterCourses(userCourses.attendedCourses, query);
	const supervisedCourses = filterCourses(userCourses.supervisedCourses, query);

	const showBar =
		appliedCourses.length || attendedCourses.length || supervisedCourses.length;

	return (
		<div className={style.container}>
			{filterCourses(userCourses.appliedCourses, query).length ? (
				<div className={style.container}>
					<div className={style.title}>Заявки на курсы</div>
					<Spacer size={20} axis={SpacerAxis.VERTICAL} />
					<div className={style.board}>
						{filterCourses(userCourses.appliedCourses, query).map((course) => (
							<CourseCard
								key={course.courseId}
								courseId={course.courseId}
								title={course.title}
								supervisor={course.supervisor}
								open={course.open}
								tagId={course.tagId}
							/>
						))}
					</div>
					<Spacer size={40} axis={SpacerAxis.VERTICAL} />
				</div>
			) : (
				<></>
			)}

			{filterCourses(userCourses.attendedCourses, query).length ? (
				<div className={style.container}>
					<div className={style.title}>Посещаемые курсы</div>
					<Spacer size={20} axis={SpacerAxis.VERTICAL} />
					<div className={style.board}>
						{filterCourses(userCourses.attendedCourses, query).map((course) => (
							<CourseCard
								key={course.courseId}
								courseId={course.courseId}
								title={course.title}
								supervisor={course.supervisor}
								open={course.open}
								tagId={course.tagId}
							/>
						))}
					</div>
					<Spacer size={40} axis={SpacerAxis.VERTICAL} />
				</div>
			) : (
				<></>
			)}
			{filterCourses(userCourses.supervisedCourses, query).length ? (
				<div className={style.container}>
					<div className={style.title}>Созданные курсы</div>
					<Spacer size={20} axis={SpacerAxis.VERTICAL} />
					<div className={style.board}>
						{filterCourses(userCourses.supervisedCourses, query).map(
							(course) => (
								<CourseCard
									key={course.courseId}
									courseId={course.courseId}
									title={course.title}
									supervisor={course.supervisor}
									open={course.open}
									tagId={course.tagId}
								/>
							)
						)}
					</div>
					<Spacer size={40} axis={SpacerAxis.VERTICAL} />
				</div>
			) : (
				<></>
			)}
			{showBar ? (
				<>
					<Spacer size={10} axis={SpacerAxis.VERTICAL} />
					<span className={style.line} />
					<Spacer size={50} axis={SpacerAxis.VERTICAL} />
				</>
			) : (
				<></>
			)}
		</div>
	);
};

export default UserCourses;
