import React from "react";
import { useNavigate } from "react-router-dom";
import { useTypedSelector } from "hooks/useTypedSelector";
import { UserResponse } from "redux/types/userTypes";
import style from "./CourseCard.module.scss";

interface CourseCardProps {
	courseId: number;
	title: string;
	supervisor: UserResponse;
	tagId: number;
	open: boolean;
}

const CourseCard: React.FC<CourseCardProps> = ({
	courseId,
	title,
	supervisor,
	tagId,
	open,
}) => {
	const navigate = useNavigate();
	const { tags } = useTypedSelector((state) => state.tag);
	const { universities } = useTypedSelector((state) => state.university);

	const tag = tags.find((tag) => tag.tagId === tagId) || null;
	const university =
		universities.find(
			(university) => university.universityId === supervisor.universityId
		) || null;

	return (
		<div
			key={courseId}
			className={style.container}
			onClick={() => navigate(`/courses/${courseId}`)}
		>
			<div className={style.top}>
				<div className={style.icon_wrapper} style={{ background: tag?.color }}>
					<img
						src={`/${tag?.icon}`}
						alt=""
						className={style.icon}
						style={{ background: tag?.color }}
					/>
				</div>

				<div className={style.text}>
					<div className={style.title}>{title}</div>
					<div
						className={style.supervisor}
					>{`${supervisor.firstname} ${supervisor.lastname} (${university?.title})`}</div>
				</div>
			</div>
			<div className={style.bottom}>
				{tag && (
					<div
						key={tag.tagId}
						className={style.tag}
						style={{ background: tag.color }}
					>
						{tag.title}
					</div>
				)}

				<div className={style.open}>{open ? "Открытый" : "Закрытый"}</div>
			</div>
		</div>
	);
};

export default CourseCard;
