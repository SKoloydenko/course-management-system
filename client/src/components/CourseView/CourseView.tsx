import React from "react";
import { Spacer } from "components";
import { SpacerAxis } from "components/Spacer/Spacer";
import { useTypedSelector } from "hooks/useTypedSelector";
import CourseGeneral from "./CourseGeneral";
import CourseCalendar from "./CourseCalendar";
import { Course, CourseRole, CourseTab } from "redux/types/courseTypes";
import { useActions } from "hooks/useActions";
import style from "./CourseView.module.scss";
import CourseSettings from "./CourseSettings";

const options = [
	{
		tab: CourseTab.CALENDAR,
		image: "/calendar.png",
	},
	// {
	// 	tab: CourseTab.PROGRESS,
	// 	image: "/progress.png",
	// },
	{
		tab: CourseTab.SETTINGS,
		image: "/settings.png",
	},
];

interface CourseViewProps {
	course: Course;
}

const CourseView: React.FC<CourseViewProps> = ({ course }) => {
	const { tags } = useTypedSelector((state) => state.tag);
	const { tab } = useTypedSelector((state) => state.course);
	const { setTab } = useActions();

	const selectContent = () => {
		switch (tab) {
			case CourseTab.GENERAL:
				return <CourseGeneral course={course} />;
			case CourseTab.CALENDAR:
				return <CourseCalendar course={course} />;
			case CourseTab.SETTINGS:
				return <CourseSettings course={course} />;
			default:
				<></>;
		}
	};

	const tag = tags.find((tag) => course.tagId === tag.tagId) || null;

	return (
		<div className={style.container}>
			<Spacer size={60} axis={SpacerAxis.VERTICAL} />
			<div className={style.header}>
				<img
					src={`/${tag?.icon}`}
					alt=""
					className={style.icon}
					style={{ background: tag?.color }}
				/>

				<div
					className={`${style.title} ${
						tab === CourseTab.GENERAL ? style.selected : ""
					}`}
					onClick={() => setTab(CourseTab.GENERAL)}
				>
					{course.title}
				</div>
				{course.roles.includes(CourseRole.MEMBER) && (
					<div className={style.options}>
						{options.map((option, index) => (
							<div key={index} className={style.option}>
								<img
									src={option.image}
									alt=""
									className={`${style.image} ${
										tab === option.tab ? style.selected : ""
									}`}
									onClick={() => setTab(option.tab)}
								/>
							</div>
						))}
					</div>
				)}
			</div>
			{selectContent()}
		</div>
	);
};

export default CourseView;
