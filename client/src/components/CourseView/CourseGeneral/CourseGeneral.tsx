import React, { useState } from "react";
import { Button, Spacer } from "components";
import { ButtonTheme } from "components/Button/Button";
import { SpacerAxis } from "components/Spacer/Spacer";
import { Course, CourseRole } from "redux/types/courseTypes";
import { useTypedSelector } from "hooks/useTypedSelector";
import { useActions } from "hooks/useActions";
import { useNavigate } from "react-router-dom";
import style from "./CourseGeneral.module.scss";

interface CourseGeneralProps {
	course: Course;
}

const CourseGeneral: React.FC<CourseGeneralProps> = ({ course }) => {
	const { user } = useTypedSelector((state) => state.user);
	const [editApplications, setEditApplications] = useState(false);
	const [editStudents, setEditStudents] = useState(false);
	const { universities } = useTypedSelector((state) => state.university);
	const {
		applyOnCourse,
		deleteApplicationOnCourse,
		confirmApplicationOnCourse,
		rejectApplicationOnCourse,
		deleteStudentFromCourse,
	} = useActions();
	const navigate = useNavigate();

	const startingDate = new Date(course.startingDate);
	const finishingDate = new Date(course.finishingDate);

	const getUniversity = (universityId: number) => {
		return (
			universities.find(
				(university) => university.universityId === universityId
			) || null
		);
	};

	const canApplyOnCourse =
		!course.roles.includes(CourseRole.MEMBER) &&
		!course.roles.includes(CourseRole.APPLICANT);
	const canDeleteApplicationOnCourse = course.roles.includes(
		CourseRole.APPLICANT
	);

	return (
		<div className={style.container}>
			<Spacer size={60} axis={SpacerAxis.VERTICAL} />
			<div className={style.block}>
				<div className={style.label}>Руководитель:</div>
				<div
					className={style.text}
				>{`${course.supervisor.firstname} ${course.supervisor.lastname}`}</div>
			</div>
			<Spacer size={20} axis={SpacerAxis.VERTICAL} />
			<div className={style.block}>
				<div className={style.label}>Даты проведения:</div>
				<div className={style.text}>
					{`${startingDate.getDate()}.${startingDate.getMonth()}.${startingDate.getFullYear()}`}
					-
					{`${finishingDate.getDate()}.${finishingDate.getMonth()}.${finishingDate.getFullYear()}`}
				</div>
			</div>
			{course.description && (
				<>
					<Spacer size={20} axis={SpacerAxis.VERTICAL} />
					<div className={style.block}>
						<div className={style.label}>Описание:</div>
						<div className={style.text}>{course.description}</div>
					</div>
				</>
			)}
			{canApplyOnCourse && (
				<>
					<Spacer size={60} axis={SpacerAxis.VERTICAL} />
					<div className={style.button}>
						<Button
							theme={ButtonTheme.DARK}
							text="Подать заявку"
							onClick={
								user
									? () => applyOnCourse(course.courseId)
									: () => navigate("/login")
							}
						/>
					</div>
				</>
			)}
			{canDeleteApplicationOnCourse && (
				<>
					<Spacer size={60} axis={SpacerAxis.VERTICAL} />
					<div className={style.button}>
						<Button
							theme={ButtonTheme.DARK}
							text="Удалить заявку"
							onClick={() => deleteApplicationOnCourse(course.courseId)}
						/>
					</div>
				</>
			)}

			{course.applicants?.length ? (
				<>
					<Spacer size={20} axis={SpacerAxis.VERTICAL} />
					<div className={style.users}>
						<div className={style.block}>
							<div className={style.label}>Заявки</div>
							<img
								src="/edit.png"
								alt=""
								className={style.edit}
								onClick={() => setEditApplications(!editApplications)}
							/>
						</div>

						<ul className={style.list}>
							{course.applicants.map((applicant) => (
								<li key={applicant.firstname} className={style.list_item}>
									<div className={style.block}>
										<div>{`${applicant.firstname} ${applicant.lastname} (${
											getUniversity(applicant.universityId)?.title
										})`}</div>
										{editApplications && (
											<div className={style.icons}>
												<img
													src="/accept.png"
													alt=""
													className={style.accept}
													onClick={() =>
														confirmApplicationOnCourse(
															course.courseId,
															applicant.userId || 0
														)
													}
												/>
												<img
													src="/decline.png"
													alt=""
													className={style.decline}
													onClick={() =>
														rejectApplicationOnCourse(
															course.courseId,
															applicant.userId || 0
														)
													}
												/>
											</div>
										)}
									</div>
								</li>
							))}
						</ul>
					</div>
				</>
			) : (
				<></>
			)}

			{course.students?.length ? (
				<>
					<Spacer size={20} axis={SpacerAxis.VERTICAL} />
					<div className={style.users}>
						<div className={style.block}>
							<div className={style.label}>Студенты</div>
							{course.roles.includes(CourseRole.SUPERVISOR) && (
								<img
									src="/edit.png"
									alt=""
									className={style.edit}
									onClick={() => setEditStudents(!editStudents)}
								/>
							)}
						</div>

						<ul className={style.list}>
							{course.students.map((student) => (
								<li key={student.firstname} className={style.list_item}>
									<div className={style.block}>
										<div>{`${student.firstname} ${student.lastname} (${
											getUniversity(student.universityId)?.title
										})`}</div>
										<div className={style.icons}>
											{course.roles.includes(CourseRole.SUPERVISOR) &&
												editStudents && (
													<img
														src="/decline.png"
														alt=""
														className={style.decline}
														onClick={() =>
															deleteStudentFromCourse(
																course.courseId,
																student.userId || 0
															)
														}
													/>
												)}
										</div>
									</div>
								</li>
							))}
						</ul>
					</div>
				</>
			) : (
				<></>
			)}
		</div>
	);
};

export default CourseGeneral;
