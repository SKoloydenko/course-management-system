import React from "react";
import { Button, Spacer } from "components";
import { SpacerAxis } from "components/Spacer/Spacer";
import { Course, CourseRole } from "redux/types/courseTypes";
import { ButtonTheme } from "components/Button/Button";
import { useActions } from "hooks/useActions";
import style from "./CourseSettings.module.scss";

interface CourseSettingsProps {
	course: Course;
}

const CourseSettings: React.FC<CourseSettingsProps> = ({ course }) => {
	const { deleteCourse, leaveCourse } = useActions();

	return (
		<div className={style.container}>
			<Spacer size={60} axis={SpacerAxis.VERTICAL} />
			<div className={style.title}>Настройки</div>
			<Spacer size={60} axis={SpacerAxis.VERTICAL} />
			<div className={style.buttons}>
				{course.roles.includes(CourseRole.SUPERVISOR) && (
					<Button
						theme={ButtonTheme.DARK}
						text="Удалить курс"
						onClick={() => deleteCourse(course.courseId)}
					/>
				)}
				{course.roles.includes(CourseRole.STUDENT) && (
					<Button
						theme={ButtonTheme.DARK}
						text="Покинуть курс"
						onClick={() => leaveCourse(course.courseId)}
					/>
				)}
			</div>
		</div>
	);
};

export default CourseSettings;
