import React, { useState } from "react";
import { Button, Input, Message, Spacer, TextArea } from "components";
import { ButtonTheme } from "components/Button/Button";
import { useTypedSelector } from "hooks/useTypedSelector";
import { useActions } from "hooks/useActions";
import style from "./CreateActivityModal.module.scss";
import { SpacerAxis } from "components/Spacer/Spacer";
import { ActivityType } from "redux/types/courseTypes";

interface ActivityCreateForm {
	title: string;
	type: ActivityType;
	description: string;
	startingTime: Date;
	finishingTime: Date;
	maxGrade: number;
}

const initialFormState = {
	title: "",
	type: ActivityType.HOMEWORK,
	description: "",
	startingTime: new Date(),
	finishingTime: new Date(),
	maxGrade: 1,
};

const CreateActivityModal: React.FC = () => {
	const [form, setForm] = useState<ActivityCreateForm>(initialFormState);
	const { error } = useTypedSelector((state) => state.course);
	const { open } = useTypedSelector((state) => state.modal);
	const { openModal, closeModal } = useActions();

	const handleActivityCreation = (event: any) => {
		event.preventDefault();
		console.log(form);
	};

	return (
		<div className={style.container}>
			<div className={style.button}>
				<Button
					theme={ButtonTheme.DARK}
					text="Создайте задание"
					onClick={() => {
						openModal();
					}}
				/>
			</div>

			{open && (
				<div className={style.modal}>
					<div className={style.modal_content}>
						<div className={style.modal_header}>
							<div className={style.modal_title}>Создайте новое задание</div>
							<img
								src="/decline-dark.png"
								alt=""
								className={style.close}
								onClick={() => {
									setForm(initialFormState);
									closeModal();
								}}
							/>
						</div>
						<form className={style.form} onSubmit={handleActivityCreation}>
							<Spacer size={20} axis={SpacerAxis.VERTICAL} />
							<div className={style.input}>
								<Input
									type="text"
									placeholder="Введите название курса"
									value={form.title}
									setValue={(event) =>
										setForm({ ...form, title: event.target.value })
									}
									required={true}
									maxLength={40}
								/>
							</div>
							<Spacer size={20} axis={SpacerAxis.VERTICAL} />
							<div className={style.textarea}>
								<TextArea
									placeholder="Введите описание"
									value={form.description}
									setValue={(value) => setForm({ ...form, description: value })}
									required={false}
									maxLength={255}
								/>
							</div>
							<Message message={error} size={60} />
							<div className={style.button}>
								<Button type="submit" theme={ButtonTheme.DARK} text="Создать" />
							</div>
						</form>
					</div>
				</div>
			)}
		</div>
	);
};

export default CreateActivityModal;
