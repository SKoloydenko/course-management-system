import React, { useEffect, useState } from "react";
import { Button, Input, Spacer } from "components";
import { SpacerAxis } from "components/Spacer/Spacer";
import { oneDay } from "constants/date";
import { Course, CourseRole } from "redux/types/courseTypes";
import { ButtonTheme } from "components/Button/Button";
import CreateActivityModal from "./CreateActivityModal";
import TextArea from "components/TextArea";
import { useActions } from "hooks/useActions";
import { useTypedSelector } from "hooks/useTypedSelector";
import style from "./CourseCalendar.module.scss";

const areEqualDays = (date1: Date, date2: Date) => {
	return (
		date1.getFullYear() === date2.getFullYear() &&
		date1.getMonth() === date2.getMonth() &&
		date1.getDate() === date2.getDate()
	);
};

const getWeek = (selectedDate: Date, weekSize: number) => {
	const date = new Date(selectedDate);
	if (weekSize === 1) {
		return [date];
	}
	if (weekSize === 3) {
		return [
			date,
			new Date(date.getTime() + oneDay),
			new Date(date.getTime() + 2 * oneDay),
		];
	}
	const correctDay = date.getDay() === 0 ? 7 : date.getDay();
	let nextDay = new Date(date.setDate(date.getDate() - correctDay));
	const week = [];
	for (let i = 0; i < 7; ++i) {
		nextDay = new Date(nextDay.getTime() + oneDay);
		week.push(new Date(nextDay));
	}

	return week;
};

const fromPreviousWeek = (date: Date, startingDate: Date, weekSize: number) => {
	const newDate = new Date(date.getTime() - oneDay * weekSize);
	if (
		new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate()) <=
		new Date(
			startingDate.getFullYear(),
			startingDate.getMonth(),
			startingDate.getDate()
		)
	) {
		return startingDate;
	}
	return new Date(date.getTime() - oneDay * weekSize);
};

const fromNextWeek = (date: Date, finishingDate: Date, weekSize: number) => {
	const newDate = new Date(date.getTime() + oneDay * weekSize);
	console.log(newDate);
	if (
		new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate()) >=
		new Date(
			finishingDate.getFullYear(),
			finishingDate.getMonth(),
			finishingDate.getDate()
		)
	) {
		return finishingDate;
	}
	return new Date(date.getTime() + oneDay * weekSize);
};

const getWeekSize = () => {
	const width = window.screen.width;
	if (width > 768) {
		return 7;
	} else if (width > 480) {
		return 3;
	} else {
		return 1;
	}
};

interface CourseCalendarProps {
	course: Course;
}

const CourseCalendar: React.FC<CourseCalendarProps> = ({ course }) => {
	const [note, setNote] = useState("");
	const [file, setFile] = useState(null);
	const { selectedDate } = useTypedSelector((state) => state.course);
	const {
		setSelectedDate,
		createNote,
		deleteNote,
		getAttachment,
		addAttachment,
		deleteAttachment,
	} = useActions();
	const daysOfWeek = ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"];

	const [weekSize, setWeekSize] = useState(getWeekSize());

	useEffect(() => {
		const handleResize = () => {
			setWeekSize(getWeekSize());
		};

		window.addEventListener("resize", handleResize);
		return () => window.removeEventListener("resize", handleResize);
	}, []);

	const handleNoteCreate = (event: any) => {
		event.preventDefault();
		createNote(course.courseId, {
			text: note,
			date: selectedDate.toLocaleDateString().split(".").reverse().join("-"),
		});
		setNote("");
	};

	const handleNoteDelete = (event: any, noteId: number) => {
		event.preventDefault();
		deleteNote(course.courseId, noteId);
	};

	const week = getWeek(selectedDate, weekSize);
	const startingDate = new Date(course.startingDate);
	const finishingDate = new Date(course.finishingDate);

	const dayStyle = (date: Date) => {
		const isSelected = areEqualDays(date, selectedDate) ? style.selected : "";
		const isLessonDay = course.lessonsDays.includes(date.getDay())
			? style.lesson
			: "";
		const isUnreachable =
			date < startingDate || date > finishingDate ? style.unreachable : "";
		return `${isLessonDay} ${isUnreachable} ${isSelected}`;
	};

	return (
		<div className={style.container}>
			<Spacer size={60} axis={SpacerAxis.VERTICAL} />
			<div className={style.week_view}>
				<div
					className={style.arrow_left}
					onClick={() => {
						setSelectedDate(fromPreviousWeek(week[0], startingDate, weekSize));
					}}
				/>
				<div className={style.days}>
					{week.map((day) => (
						<div
							key={day.getTime()}
							className={`${style.day} ${dayStyle(day)}`}
							onClick={() => setSelectedDate(day)}
						>
							<div className={style.day_of_week}>
								{daysOfWeek[day.getDay()]}
							</div>
							<div className={style.date}>{day.getDate()}</div>
						</div>
					))}
				</div>
				<div
					className={style.arrow_right}
					onClick={() => {
						setSelectedDate(fromNextWeek(week[0], finishingDate, weekSize));
					}}
				/>
			</div>
			<Spacer size={60} axis={SpacerAxis.VERTICAL} />
			<div className={style.content_container}>
				<div className={style.big_date}>{`${selectedDate.getDate()}.${
					selectedDate.getMonth() + 1
				}.${selectedDate.getFullYear()}`}</div>

				{/* {course.roles.includes(CourseRole.SUPERVISOR) && (
					<>
						<Spacer size={50} axis={SpacerAxis.VERTICAL} />
						<CreateActivityModal />
					</>
				)}

				<div className={style.activities}></div> */}

				<Spacer size={60} axis={SpacerAxis.VERTICAL} />
				<form className={style.content} onSubmit={handleNoteCreate}>
					<div className={style.text}>Заметки</div>
					<Spacer size={20} axis={SpacerAxis.VERTICAL} />
					<div className={style.list}>
						{course.notes
							.filter((note) => areEqualDays(note.date, selectedDate))
							.map((note) => (
								<div key={note.noteId} className={style.element}>
									<div className={style.note_text}>{note.text}</div>
									<img
										src="/decline-dark.png"
										alt=""
										className={style.decline}
										onClick={(event) => handleNoteDelete(event, note.noteId)}
									/>
								</div>
							))}
					</div>

					<Spacer size={20} axis={SpacerAxis.VERTICAL} />
					<div className={style.textarea}>
						<TextArea
							placeholder="Введите заметку"
							value={note}
							setValue={(value) => setNote(value)}
							required={true}
							maxLength={255}
						/>
					</div>

					<Spacer size={10} axis={SpacerAxis.VERTICAL} />

					<div className={style.button}>
						<Button
							type="submit"
							theme={ButtonTheme.DARK}
							text="Добавить заметку"
						/>
					</div>
				</form>

				<Spacer size={50} axis={SpacerAxis.VERTICAL} />
				<div className={style.content}>
					<div className={style.text}>Файлы</div>
					<Spacer size={20} axis={SpacerAxis.VERTICAL} />
					<div className={style.list}>
						{course.attachments
							.filter((attachment) =>
								areEqualDays(attachment.date, selectedDate)
							)
							.map((attachment) => (
								<div key={attachment.fileId} className={style.element}>
									<div
										className={style.filename}
										onClick={() =>
											getAttachment(course.courseId, attachment.fileId)
										}
									>
										{attachment.name}
									</div>
									<img
										src="/decline-dark.png"
										alt=""
										className={style.decline}
										onClick={() => {
											deleteAttachment(course.courseId, attachment.fileId);
										}}
									/>
								</div>
							))}
					</div>
					<Spacer size={20} axis={SpacerAxis.VERTICAL} />
					<div className={style.upload}>
						<Input
							type="file"
							setValue={(event) => setFile(event.target.files[0])}
							required={true}
						/>
					</div>

					{file && (
						<div className={style.button}>
							<Spacer size={20} axis={SpacerAxis.VERTICAL} />
							<Button
								type="button"
								theme={ButtonTheme.LIGHT_BORDER}
								text="Отправить файл"
								onClick={() =>
									addAttachment(
										course.courseId,
										file,
										selectedDate
											.toLocaleDateString()
											.split(".")
											.reverse()
											.join("-")
									)
								}
							/>
						</div>
					)}
				</div>
			</div>
		</div>
	);
};

export default CourseCalendar;
