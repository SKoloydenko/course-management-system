import React, { useEffect } from "react";
import { useTypedSelector } from "hooks/useTypedSelector";
import { Loader } from "components";
import style from "./TagSelector.module.scss";

interface TagSelectorProps {
	value: number;
	setValue: (value: number) => void;
}

const validateTag = (value: number) => {
	const element: any = document.getElementById("tag_validation");
	if (!value) {
		element?.setCustomValidity("Выберите один тэг");
	} else {
		element?.setCustomValidity("");
	}
};

const TagSelector: React.FC<TagSelectorProps> = ({ value, setValue }) => {
	const { tags, loading } = useTypedSelector((state) => state.tag);

	useEffect(() => {
		validateTag(value);
	});

	if (loading) {
		return <Loader />;
	}

	return (
		<div className={style.container}>
			<div className={style.title}>Выберите тэги</div>
			<button id={"tag_validation"} className={style.tag_validation} />
			<div className={style.tags}>
				{tags.map((tag) => (
					<div
						key={tag.tagId}
						className={`${style.tag} ${
							value === tag.tagId ? style.active : ""
						}`}
						style={
							value === tag.tagId
								? { background: tag.color }
								: { border: `2px solid ${tag.color}`, color: tag.color }
						}
						onClick={() => setValue(tag.tagId)}
					>
						{tag.title}
					</div>
				))}
			</div>
		</div>
	);
};

export default TagSelector;
