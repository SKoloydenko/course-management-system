import React from "react";
import autosize from "autosize";
import style from "./TextArea.module.scss";

interface TextAreaProps {
	placeholder: string;
	value: string;
	setValue: (value: string) => void;
	required: boolean;
	minLength?: number;
	maxLength?: number;
}

const TextArea: React.FC<TextAreaProps> = ({
	placeholder,
	value,
	setValue,
	required,
	minLength,
	maxLength,
}) => {
	const textarea: any = document.querySelector("textarea");
	autosize(textarea);

	return (
		<textarea
			className={style.textarea}
			placeholder={placeholder}
			autoCorrect="off"
			autoComplete="off"
			spellCheck="false"
			value={value}
			onChange={(event) => setValue(event.target.value)}
			required={required}
			minLength={minLength}
			maxLength={maxLength}
		/>
	);
};

export default TextArea;
