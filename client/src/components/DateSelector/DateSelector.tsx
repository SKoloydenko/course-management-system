import React from "react";
import DatePicker from "react-date-picker";
import style from "./DateSelector.module.scss";
import "./DateSelector.css";
import "react-date-picker/dist/DatePicker.css";

interface PopupCalendarProps {
	date: Date;
	setDate: (value: Date) => void;
	placeholder?: string;
	minDate: Date;
}

const DateSelector: React.FC<PopupCalendarProps> = ({
	date,
	setDate,
	placeholder,
	minDate,
}) => {
	return (
		<div className={style.container}>
			<div className={style.placeholder}>{placeholder}</div>
			<DatePicker value={date} onChange={setDate} minDate={minDate} />
		</div>
	);
};

export default DateSelector;
