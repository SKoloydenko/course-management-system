import React, { useEffect } from "react";
import style from "./DayOfWeekSelector.module.scss";

interface DayOfWeekSelectorProps {
	value: number[];
	setValue: (value: number[]) => void;
	placeholder?: string;
}

const validateDayOfWeek = (value: number) => {
	const element: any = document.getElementById("day_of_week_validation");
	if (!value) {
		element?.setCustomValidity("Выберите хотя бы один учебный день");
	} else {
		element?.setCustomValidity("");
	}
};

const DayOfWeekSelector: React.FC<DayOfWeekSelectorProps> = ({
	value,
	setValue,
	placeholder,
}) => {
	const daysOfWeek = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"];

	const toggleDay = (id: number) => {
		if (value.includes(id)) {
			return setValue([...value.filter((it) => it !== id)]);
		}
		setValue([...value, id].sort());
	};

	useEffect(() => {
		validateDayOfWeek(value.length);
	});

	return (
		<div className={style.container}>
			<div className={style.placeholder}>{placeholder}</div>
			<button
				id={"day_of_week_validation"}
				className={style.day_of_week_validation}
			/>
			<div className={style.days}>
				{daysOfWeek.map((day, id) => (
					<div
						key={id}
						className={`${style.day} ${
							value.includes(id + 1) ? style.active : ""
						}`}
						onClick={() => toggleDay(id + 1)}
					>
						{day}
					</div>
				))}
			</div>
		</div>
	);
};

export default DayOfWeekSelector;
