export interface TagResponse {
	tagId: number;
	title: string;
}

export interface Tag {
	tagId: number;
	title: string;
	color: string;
	icon: string;
}

export interface TagState {
	tags: Tag[];
	loading: boolean;
	error: string | null;
}

export enum TagActionType {
	GET_TAGS = "GET_TAGS",
	GET_TAGS_SUCCESS = "GET_TAGS_SUCCESS",
	GET_TAGS_ERROR = "GET_TAGS_ERROR",
}

export interface GetTags {
	type: TagActionType.GET_TAGS;
}

export interface GetTagsSuccess {
	type: TagActionType.GET_TAGS_SUCCESS;
	payload: {
		tags: TagResponse[];
	};
}

export interface GetTagsError {
	type: TagActionType.GET_TAGS_ERROR;
	payload: { error: string };
}

export type TagAction = GetTags | GetTagsSuccess | GetTagsError;
