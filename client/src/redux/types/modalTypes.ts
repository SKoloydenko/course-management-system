export interface ModalState {
	open: boolean;
}

export enum ModalActionType {
	OPEN_MODAL = "OPEN_MODAL",
	CLOSE_MODAL = "CLOSE_MODAL",
}

export interface OpenModal {
	type: ModalActionType.OPEN_MODAL;
}

export interface CloseModal {
	type: ModalActionType.CLOSE_MODAL;
}

export type ModalAction = OpenModal | CloseModal;
