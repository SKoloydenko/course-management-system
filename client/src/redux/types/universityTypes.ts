export interface University {
	universityId: number;
	title: string;
}

export interface UniversityState {
	universities: University[];
	loading: boolean;
	error: string | null;
}

export enum UniversityActionType {
	GET_UNIVERSITIES = "GET_UNIVERSITIES",
	GET_UNIVERSITIES_SUCCESS = "GET_UNIVERSITIES_SUCCESS",
	GET_UNIVERSITIES_ERROR = "GET_UNIVERSITIES_ERROR",
}

export interface GetUniversities {
	type: UniversityActionType.GET_UNIVERSITIES;
}

export interface GetUniversitiesSuccess {
	type: UniversityActionType.GET_UNIVERSITIES_SUCCESS;
	payload: {
		universities: University[];
	};
}

export interface GetUniversitiesError {
	type: UniversityActionType.GET_UNIVERSITIES_ERROR;
	payload: { error: string };
}

export type UniversityAction =
	| GetUniversities
	| GetUniversitiesSuccess
	| GetUniversitiesError;
