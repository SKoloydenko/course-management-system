import { UserResponse } from "./userTypes";

export enum CourseTab {
	GENERAL,
	CALENDAR,
	PROGRESS,
	SETTINGS,
}

export enum CourseRole {
	USER = "USER",
	APPLICANT = "APPLICANT",
	STUDENT = "STUDENT",
	MEMBER = "MEMBER",
	SUPERVISOR = "SUPERVISOR",
}

export interface Course {
	courseId: number;
	title: string;
	supervisor: UserResponse;
	startingDate: string;
	finishingDate: string;
	lessonsDays: number[];
	open: boolean;
	tagId: number;
	description: string;
	applicants?: UserResponse[];
	students?: UserResponse[];
	notes: Note[];
	attachments: Attachment[];
	roles: CourseRole[];
}

export interface NoteResponse {
	noteId: number;
	text: string;
	date: string;
}

export interface Note {
	noteId: number;
	text: string;
	date: Date;
}

export interface AttachmentResponse {
	fileId: number;
	name: string;
	date: string;
}

export interface Attachment {
	fileId: number;
	name: string;
	date: Date;
}

export enum ActivityType {
	HOMEWORK = "HOMEWORK",
	TEST = "TEST",
}

export interface Activity {
	type: ActivityType;
}

export interface CourseState {
	currentCourse: Course | null;
	courses: Course[];
	tab: CourseTab;
	selectedDate: Date;
	loading: boolean;
	error: string | null;
}

export enum CourseActionType {
	RESET_ERROR = "RESET_ERROR",

	SET_TAB = "SET_TAB",

	SET_SELECTED_DATE = "SET_SELECTED_DATE",

	GET_COURSES = "GET_COURSES",
	GET_COURSES_SUCCESS = "GET_COURSES_SUCCESS",
	GET_COURSES_ERROR = "GET_COURSES_ERROR",

	GET_COURSE = "GET_COURSE",
	GET_COURSE_SUCCESS = "GET_COURSE_SUCCESS",
	GET_COURSE_ERROR = "GET_COURSE_ERROR",

	CREATE_COURSE = "CREATE_COURSE",
	CREATE_COURSE_SUCCESS = "CREATE_COURSE_SUCCESS",
	CREATE_COURSE_ERROR = "CREATE_COURSE_ERROR",

	APPLY_ON_COURSE = "APPLY_ON_COURSE",
	APPLY_ON_COURSE_SUCCESS = "APPLY_ON_COURSE_SUCCESS",
	APPLY_ON_COURSE_ERROR = "APPLY_ON_COURSE_ERROR",

	DELETE_APPLICATION = "DELETE_APPLICATION",
	DELETE_APPLICATION_SUCCESS = "DELETE_APPLICATION_SUCCESS",
	DELETE_APPLICATION_ERROR = "DELETE_APPLICATION_ERROR",

	CONFIRM_APPLICATION = "CONFIRM_APPLICATION",
	CONFIRM_APPLICATION_SUCCESS = "CONFIRM_APPLICATION_ON_COURSE_SUCCESS",
	CONFIRM_APPLICATION_ERROR = "CONFIRM_APPLICATION_ERROR",

	REJECT_APPLICATION = "REJECT_APPLICATION",
	REJECT_APPLICATION_SUCCESS = "REJECT_APPLICATION_SUCCESS",
	REJECT_APPLICATION_ERROR = "REJECT_APPLICATION_ERROR",

	DELETE_STUDENT = "DELETE_STUDENT",
	DELETE_STUDENT_SUCCESS = "DELETE_STUDENT_SUCCESS",
	DELETE_STUDENT_ERROR = "DELETE_STUDENT_ERROR",

	DELETE_COURSE = "DELETE_COURSE",
	DELETE_COURSE_SUCCESS = "DELETE_COURSE_SUCCESS",
	DELETE_COURSE_ERROR = "DELETE_COURSE_ERROR",

	LEAVE_COURSE = "LEAVE_COURSE",
	LEAVE_COURSE_SUCCESS = "LEAVE_COURSE_SUCCESS",
	LEAVE_COURSE_ERROR = "LEAVE_COURSE_ERROR",

	CREATE_NOTE = "CREATE_NOTE",
	CREATE_NOTE_SUCCESS = "CREATE_NOTE_SUCCESS",
	CREATE_NOTE_ERROR = "CREATE_NOTE_ERROR",

	DELETE_NOTE = "DELETE_NOTE",
	DELETE_NOTE_SUCCESS = "DELETE_NOTE_SUCCESS",
	DELETE_NOTE_ERROR = "DELETE_NOTE_ERROR",

	GET_ATTACHMENT = "GET_ATTACHMENT",
	GET_ATTACHMENT_SUCCESS = "GET_ATTACHMENT_SUCCESS",
	GET_ATTACHMENT_ERROR = "GET_ATTACHMENT_ERROR",

	ADD_ATTACHMENT = "ADD_ATTACHMENT",
	ADD_ATTACHMENT_SUCCESS = "ADD_ATTACHMENT_SUCCESS",
	ADD_ATTACHMENT_ERROR = "ADD_ATTACHMENT_ERROR",

	DELETE_ATTACHMENT = "DELETE_ATTACHMENT",
	DELETE_ATTACHMENT_SUCCESS = "DELETE_ATTACHMENT_SUCCESS",
	DELETE_ATTACHMENT_ERROR = "DELETE_ATTACHMENT_ERROR",
}

export interface ResetError {
	type: CourseActionType.RESET_ERROR;
}

export interface SetTab {
	type: CourseActionType.SET_TAB;
	payload: { tab: CourseTab };
}

export interface SetSelectedDate {
	type: CourseActionType.SET_SELECTED_DATE;
	payload: { date: Date };
}

export interface GetCourses {
	type: CourseActionType.GET_COURSES;
}

export interface GetCoursesSuccess {
	type: CourseActionType.GET_COURSES_SUCCESS;
	payload: { courses: Course[] };
}

export interface GetCoursesError {
	type: CourseActionType.GET_COURSES_ERROR;
	payload: { error: string };
}

export interface GetCourse {
	type: CourseActionType.GET_COURSE;
}

export interface GetCourseSuccess {
	type: CourseActionType.GET_COURSE_SUCCESS;
	payload: { course: Course };
}

export interface GetCourseError {
	type: CourseActionType.GET_COURSE_ERROR;
	payload: { error: string };
}

export interface CreateCourse {
	type: CourseActionType.CREATE_COURSE;
}

export interface CreateCourseSuccess {
	type: CourseActionType.CREATE_COURSE_SUCCESS;
	payload: { course: Course };
}

export interface CreateCourseError {
	type: CourseActionType.CREATE_COURSE_ERROR;
	payload: { error: string };
}

export interface ApplyOnCourse {
	type: CourseActionType.APPLY_ON_COURSE;
}

export interface ApplyOnCourseSuccess {
	type: CourseActionType.APPLY_ON_COURSE_SUCCESS;
}

export interface ApplyOnCourseError {
	type: CourseActionType.APPLY_ON_COURSE_ERROR;
	payload: { error: string };
}

export interface DeleteApplication {
	type: CourseActionType.DELETE_APPLICATION;
}

export interface DeleteApplicationSuccess {
	type: CourseActionType.DELETE_APPLICATION_SUCCESS;
}

export interface DeleteApplicationError {
	type: CourseActionType.DELETE_APPLICATION_ERROR;
	payload: { error: string };
}

export interface ConfirmApplication {
	type: CourseActionType.CONFIRM_APPLICATION;
}

export interface ConfirmApplicationSuccess {
	type: CourseActionType.CONFIRM_APPLICATION_SUCCESS;
}

export interface ConfirmApplicationError {
	type: CourseActionType.CONFIRM_APPLICATION_ERROR;
	payload: { error: string };
}

export interface RejectApplication {
	type: CourseActionType.REJECT_APPLICATION;
}

export interface RejectApplicationSuccess {
	type: CourseActionType.REJECT_APPLICATION_SUCCESS;
}

export interface RejectApplicationError {
	type: CourseActionType.REJECT_APPLICATION_ERROR;
	payload: { error: string };
}

export interface DeleteStudent {
	type: CourseActionType.DELETE_STUDENT;
}

export interface DeleteStudentSuccess {
	type: CourseActionType.DELETE_STUDENT_SUCCESS;
}

export interface DeleteStudentError {
	type: CourseActionType.DELETE_STUDENT_ERROR;
	payload: { error: string };
}

export interface DeleteCourse {
	type: CourseActionType.DELETE_COURSE;
}

export interface DeleteCourseSuccess {
	type: CourseActionType.DELETE_COURSE_SUCCESS;
}

export interface DeleteCourseError {
	type: CourseActionType.DELETE_COURSE_ERROR;
	payload: { error: string };
}

export interface LeaveCourse {
	type: CourseActionType.LEAVE_COURSE;
}

export interface LeaveCourseSuccess {
	type: CourseActionType.LEAVE_COURSE_SUCCESS;
}

export interface LeaveCourseError {
	type: CourseActionType.LEAVE_COURSE_ERROR;
	payload: { error: string };
}

export interface CreateNote {
	type: CourseActionType.CREATE_NOTE;
}

export interface CreateNoteSuccess {
	type: CourseActionType.CREATE_NOTE_SUCCESS;
	payload: { notes: NoteResponse[] };
}

export interface CreateNoteError {
	type: CourseActionType.CREATE_NOTE_ERROR;
	payload: { error: string };
}

export interface DeleteNote {
	type: CourseActionType.DELETE_NOTE;
}

export interface DeleteNoteSuccess {
	type: CourseActionType.DELETE_NOTE_SUCCESS;
	payload: { noteId: number };
}

export interface DeleteNoteError {
	type: CourseActionType.DELETE_NOTE_ERROR;
	payload: { error: string };
}

export interface GetAttachment {
	type: CourseActionType.GET_ATTACHMENT;
}

export interface GetAttachmentSuccess {
	type: CourseActionType.GET_ATTACHMENT_SUCCESS;
}

export interface GetAttachmentError {
	type: CourseActionType.GET_ATTACHMENT_ERROR;
	payload: { error: string };
}

export interface AddAttachment {
	type: CourseActionType.ADD_ATTACHMENT;
}

export interface AddAttachmentSuccess {
	type: CourseActionType.ADD_ATTACHMENT_SUCCESS;
	payload: { attachments: AttachmentResponse[] };
}

export interface AddAttachmentError {
	type: CourseActionType.ADD_ATTACHMENT_ERROR;
	payload: { error: string };
}

export interface DeleteAttachment {
	type: CourseActionType.DELETE_ATTACHMENT;
}

export interface DeleteAttachmentSuccess {
	type: CourseActionType.DELETE_ATTACHMENT_SUCCESS;
	payload: { attachmentId: number };
}

export interface DeleteAttachmentError {
	type: CourseActionType.DELETE_ATTACHMENT_ERROR;
	payload: { error: string };
}

export type CourseAction =
	| ResetError
	| SetTab
	| SetSelectedDate
	| GetCourses
	| GetCoursesSuccess
	| GetCoursesError
	| GetCourse
	| GetCourseSuccess
	| GetCourseError
	| CreateCourse
	| CreateCourseSuccess
	| CreateCourseError
	| ApplyOnCourse
	| ApplyOnCourseSuccess
	| ApplyOnCourseError
	| DeleteApplication
	| DeleteApplicationSuccess
	| DeleteApplicationError
	| ConfirmApplication
	| ConfirmApplicationSuccess
	| ConfirmApplicationError
	| RejectApplication
	| RejectApplicationSuccess
	| RejectApplicationError
	| DeleteStudent
	| DeleteStudentSuccess
	| DeleteStudentError
	| DeleteCourse
	| DeleteCourseSuccess
	| DeleteCourseError
	| LeaveCourse
	| LeaveCourseSuccess
	| LeaveCourseError
	| CreateNote
	| CreateNoteSuccess
	| CreateNoteError
	| DeleteNote
	| DeleteNoteSuccess
	| DeleteNoteError
	| GetAttachment
	| GetAttachmentSuccess
	| GetAttachmentError
	| AddAttachment
	| AddAttachmentSuccess
	| AddAttachmentError
	| DeleteAttachment
	| DeleteAttachmentSuccess
	| DeleteAttachmentError;

export const CourseErrorType: Record<string, string> = {
	COURSE_ALREADY_EXISTS: "Курс с таким названием уже существует",
	INVALID_FIELD: "Некорректное поле",
	UNAUTHORIZED: "Некорректные данные",
};
