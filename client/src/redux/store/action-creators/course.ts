import { Dispatch } from "redux";
import {
	CourseAction,
	CourseActionType,
	CourseErrorType,
	CourseTab,
} from "redux/types/courseTypes";
import CourseService, {
	CourseCreateRequest,
	NoteCreateRequest,
} from "services/courseService";
import { closeModal } from "./modal";
import { getCurrentUser } from "./user";

export const resetCourseError = () => ({
	type: CourseActionType.RESET_ERROR,
});

export const setTab = (tab: CourseTab) => ({
	type: CourseActionType.SET_TAB,
	payload: { tab },
});

export const setSelectedDate = (date: Date) => ({
	type: CourseActionType.SET_SELECTED_DATE,
	payload: { date },
});

export const getCourses = () => {
	return async (dispatch: Dispatch<CourseAction>) => {
		try {
			dispatch({ type: CourseActionType.GET_COURSES });
			const response = await CourseService.getCourses();
			dispatch({
				type: CourseActionType.GET_COURSES_SUCCESS,
				payload: { courses: response.data },
			});
		} catch (error: any) {
			dispatch({
				type: CourseActionType.GET_COURSES_ERROR,
				payload: { error: error.message },
			});
		}
	};
};

export const getCourse = (courseId: number) => {
	return async (dispatch: Dispatch<CourseAction>) => {
		try {
			dispatch({ type: CourseActionType.GET_COURSE });
			const response = await CourseService.getCourse(courseId);
			dispatch({
				type: CourseActionType.GET_COURSE_SUCCESS,
				payload: { course: response.data },
			});
		} catch (error: any) {
			dispatch({
				type: CourseActionType.GET_COURSE_ERROR,
				payload: { error: error.message },
			});
		}
	};
};

export const createCourse = (courseCreateRequest: CourseCreateRequest) => {
	return async (dispatch: any) => {
		try {
			dispatch({ type: CourseActionType.CREATE_COURSE });
			const response = await CourseService.createCourse(courseCreateRequest);
			dispatch({
				type: CourseActionType.CREATE_COURSE_SUCCESS,
				payload: { course: response.data },
			});
			dispatch(closeModal());
			await CourseService.getCourses();
			dispatch(getCurrentUser());
		} catch (error: any) {
			dispatch({
				type: CourseActionType.CREATE_COURSE_ERROR,
				payload: { error: getCourseError(error.response.data.type) },
			});
		}
	};
};

export const applyOnCourse = (courseId: number) => {
	return async (dispatch: any) => {
		try {
			dispatch({ type: CourseActionType.APPLY_ON_COURSE });
			const response = await CourseService.applyOnCourse(courseId);
			dispatch({
				type: CourseActionType.APPLY_ON_COURSE_SUCCESS,
			});
			dispatch(getCourse(courseId));
			dispatch(getCurrentUser());
		} catch (error: any) {
			dispatch({
				type: CourseActionType.APPLY_ON_COURSE_ERROR,
				payload: { error: getCourseError(error.response.data.type) },
			});
		}
	};
};

export const deleteApplicationOnCourse = (courseId: number) => {
	return async (dispatch: any) => {
		try {
			dispatch({ type: CourseActionType.DELETE_APPLICATION });
			const response = await CourseService.deleteApplicationOnCourse(courseId);
			dispatch({
				type: CourseActionType.DELETE_APPLICATION_SUCCESS,
			});
			dispatch(getCourse(courseId));
			dispatch(getCurrentUser());
		} catch (error: any) {
			dispatch({
				type: CourseActionType.DELETE_APPLICATION_ERROR,
				payload: { error: getCourseError(error.response.data.type) },
			});
		}
	};
};

export const confirmApplicationOnCourse = (
	courseId: number,
	userId: number
) => {
	return async (dispatch: any) => {
		try {
			dispatch({ type: CourseActionType.CONFIRM_APPLICATION });
			const response = await CourseService.confirmApplicationOnCourse(
				courseId,
				userId
			);
			dispatch({
				type: CourseActionType.CONFIRM_APPLICATION_SUCCESS,
			});
			dispatch(getCourse(courseId));
		} catch (error: any) {
			dispatch({
				type: CourseActionType.CONFIRM_APPLICATION_ERROR,
				payload: { error: getCourseError(error.response.data.type) },
			});
		}
	};
};

export const rejectApplicationOnCourse = (courseId: number, userId: number) => {
	return async (dispatch: any) => {
		try {
			dispatch({ type: CourseActionType.REJECT_APPLICATION });
			const response = await CourseService.rejectApplicationOnCourse(
				courseId,
				userId
			);
			dispatch({
				type: CourseActionType.REJECT_APPLICATION_SUCCESS,
			});
			dispatch(getCourse(courseId));
		} catch (error: any) {
			dispatch({
				type: CourseActionType.REJECT_APPLICATION_ERROR,
				payload: { error: getCourseError(error.response.data.type) },
			});
		}
	};
};

export const deleteStudentFromCourse = (courseId: number, userId: number) => {
	return async (dispatch: any) => {
		try {
			dispatch({ type: CourseActionType.DELETE_STUDENT });
			const response = await CourseService.deleteStudentFromCourse(
				courseId,
				userId
			);
			dispatch({
				type: CourseActionType.DELETE_STUDENT_SUCCESS,
			});
			dispatch(getCourse(courseId));
		} catch (error: any) {
			dispatch({
				type: CourseActionType.DELETE_STUDENT_ERROR,
				payload: { error: getCourseError(error.response.data.type) },
			});
		}
	};
};

export const deleteCourse = (courseId: number) => {
	return async (dispatch: any) => {
		try {
			dispatch({ type: CourseActionType.DELETE_COURSE });
			const response = await CourseService.deleteCourse(courseId);
			dispatch({
				type: CourseActionType.DELETE_COURSE_SUCCESS,
			});
			let a = document.createElement("a");
			a.href = "/";
			a.click();
			dispatch(getCourses());
			dispatch(getCurrentUser());
		} catch (error: any) {
			dispatch({
				type: CourseActionType.DELETE_COURSE_ERROR,
				payload: { error: getCourseError(error.response.data.type) },
			});
		}
	};
};

export const leaveCourse = (courseId: number) => {
	return async (dispatch: any) => {
		try {
			dispatch({ type: CourseActionType.LEAVE_COURSE });
			const response = await CourseService.leaveCourse(courseId);
			dispatch({
				type: CourseActionType.LEAVE_COURSE_SUCCESS,
			});
			let a = document.createElement("a");
			a.href = "/";
			a.click();
			dispatch(getCourses());
			dispatch(getCurrentUser());
		} catch (error: any) {
			dispatch({
				type: CourseActionType.LEAVE_COURSE_ERROR,
				payload: { error: getCourseError(error.response.data.type) },
			});
		}
	};
};

export const createNote = (
	courseId: number,
	noteCreateRequest: NoteCreateRequest
) => {
	return async (dispatch: any) => {
		try {
			dispatch({ type: CourseActionType.CREATE_NOTE });
			const response = await CourseService.createNote(
				courseId,
				noteCreateRequest
			);
			dispatch({
				type: CourseActionType.CREATE_NOTE_SUCCESS,
				payload: { notes: response.data },
			});
		} catch (error: any) {
			dispatch({
				type: CourseActionType.CREATE_NOTE_ERROR,
				payload: { error: getCourseError(error.response.data.type) },
			});
		}
	};
};

export const deleteNote = (courseId: number, noteId: number) => {
	return async (dispatch: any) => {
		try {
			dispatch({ type: CourseActionType.DELETE_NOTE });
			const response = await CourseService.deleteNote(courseId, noteId);
			dispatch({
				type: CourseActionType.DELETE_NOTE_SUCCESS,
				payload: { noteId },
			});
		} catch (error: any) {
			dispatch({
				type: CourseActionType.DELETE_NOTE_ERROR,
				payload: { error: getCourseError(error.response.data.type) },
			});
		}
	};
};

export const getAttachment = (courseId: number, attachmentId: number) => {
	return async (dispatch: any) => {
		try {
			dispatch({ type: CourseActionType.GET_ATTACHMENT });
			const response = await CourseService.getAttachment(
				courseId,
				attachmentId
			).then((response) => {
				let url = window.URL.createObjectURL(new Blob([response?.data]));
				let a = document.createElement("a");
				a.href = url;
				a.download = JSON.parse(
					`"${
						response.headers["content-disposition"].split(
							"attachment; filename*=UTF-8''"
						)[1]
					}"`
				);
				a.click();
			});
			dispatch({
				type: CourseActionType.GET_ATTACHMENT_SUCCESS,
			});
			dispatch(getCourse(courseId));
		} catch (error: any) {
			dispatch({
				type: CourseActionType.GET_ATTACHMENT_ERROR,
				payload: { error: getCourseError(error.response?.data.type) },
			});
		}
	};
};

export const addAttachment = (courseId: number, file: File, date: string) => {
	return async (dispatch: any) => {
		try {
			dispatch({ type: CourseActionType.ADD_ATTACHMENT });
			const response = await CourseService.addAttachment(courseId, file, date);
			dispatch({
				type: CourseActionType.ADD_ATTACHMENT_SUCCESS,
				payload: { attachments: response.data },
			});
		} catch (error: any) {
			dispatch({
				type: CourseActionType.ADD_ATTACHMENT_ERROR,
				payload: { error: getCourseError(error.response?.data.type) },
			});
		}
	};
};

export const deleteAttachment = (courseId: number, attachmentId: number) => {
	return async (dispatch: any) => {
		try {
			dispatch({ type: CourseActionType.DELETE_ATTACHMENT });
			const response = await CourseService.deleteAttachment(
				courseId,
				attachmentId
			);
			dispatch({
				type: CourseActionType.DELETE_ATTACHMENT_SUCCESS,
				payload: { attachmentId },
			});
		} catch (error: any) {
			dispatch({
				type: CourseActionType.DELETE_ATTACHMENT_ERROR,
				payload: { error: getCourseError(error.response.data.type) },
			});
		}
	};
};

const getCourseError = (type: string) => {
	return CourseErrorType[type] || "Ошибка";
};
