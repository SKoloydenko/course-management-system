import { Dispatch } from "redux";
import { ModalActionType } from "redux/types/modalTypes";
import { resetCourseError } from "./course";

export const openModal = () => {
	return (dispatch: any) => {
		dispatch(resetCourseError());
		const body: any = document.querySelector("body");
		body!.style.overflow = "hidden";
		dispatch({
			type: ModalActionType.OPEN_MODAL,
		});
	};
};

export const closeModal = () => {
	return (dispatch: Dispatch) => {
		const body: any = document.querySelector("body");
		body!.style.overflow = "auto";
		dispatch({
			type: ModalActionType.CLOSE_MODAL,
		});
	};
};
