import { Dispatch } from "redux";
import {
	UserAction,
	UserActionType,
	UserErrorType,
} from "redux/types/userTypes";
import UserService from "services/userService";

export const getCurrentUser = () => {
	return async (dispatch: Dispatch<UserAction>) => {
		try {
			dispatch({ type: UserActionType.GET_USER });
			const response = await UserService.getCurrentUser();
			dispatch({
				type: UserActionType.GET_USER_SUCCESS,
				payload: { response: response.data },
			});
		} catch (error: any) {
			dispatch({
				type: UserActionType.GET_USER_ERROR,
				payload: { error: getUserError(error.response.data.type) },
			});
		}
	};
};

export const resetUser = () => ({
	type: UserActionType.RESET_USER,
});

const getUserError = (type: string) => {
	return UserErrorType[type] || "Ошибка";
};
