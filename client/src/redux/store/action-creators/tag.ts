import { Dispatch } from "redux";
import { TagAction, TagActionType } from "redux/types/tagTypes";
import tagService from "services/tagService";

export const getTags = () => {
	return async (dispatch: Dispatch<TagAction>) => {
		try {
			dispatch({ type: TagActionType.GET_TAGS });
			const response = await tagService.getTags();
			dispatch({
				type: TagActionType.GET_TAGS_SUCCESS,
				payload: { tags: response.data },
			});
		} catch (error: any) {
			dispatch({
				type: TagActionType.GET_TAGS_ERROR,
				payload: { error: error.response.data.message },
			});
		}
	};
};
