import { Dispatch } from "redux";
import {
	UniversityAction,
	UniversityActionType,
} from "redux/types/universityTypes";
import UniversityService from "services/universityService";

export const getUniversities = () => {
	return async (dispatch: Dispatch<UniversityAction>) => {
		try {
			dispatch({ type: UniversityActionType.GET_UNIVERSITIES });
			const response = await UniversityService.getUniversities();
			dispatch({
				type: UniversityActionType.GET_UNIVERSITIES_SUCCESS,
				payload: { universities: response.data },
			});
		} catch (error: any) {
			dispatch({
				type: UniversityActionType.GET_UNIVERSITIES_ERROR,
				payload: { error: error.response.data.message },
			});
		}
	};
};
