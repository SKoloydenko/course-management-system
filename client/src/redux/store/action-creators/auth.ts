import { Dispatch } from "redux";
import {
	AuthAction,
	AuthActionType,
	AuthErrorType,
} from "redux/types/authTypes";
import { getCurrentUser, resetUser } from "./user";
import AuthService, {
	LoginRequest,
	RegisterRequest,
} from "services/authService";

export const register = (registerRequest: RegisterRequest) => {
	return async (dispatch: Dispatch<AuthAction>) => {
		try {
			dispatch({ type: AuthActionType.REGISTER });
			await AuthService.register(registerRequest);
			dispatch({
				type: AuthActionType.REGISTER_SUCCESS,
			});
		} catch (error: any) {
			dispatch({
				type: AuthActionType.REGISTER_ERROR,
				payload: { error: getAuthError(error.response.data.type) },
			});
		}
	};
};

export const login = (loginRequest: LoginRequest) => {
	return async (dispatch: any) => {
		try {
			dispatch({ type: AuthActionType.LOGIN });
			await AuthService.login(loginRequest);
			dispatch({
				type: AuthActionType.LOGIN_SUCCESS,
			});
			dispatch(getCurrentUser());
		} catch (error: any) {
			dispatch({
				type: AuthActionType.LOGIN_ERROR,
				payload: { error: getAuthError(error.response.data.type) },
			});
		}
	};
};

export const logout = () => {
	return async (dispatch: any) => {
		try {
			dispatch({ type: AuthActionType.LOGOUT });
			await AuthService.logout();
			dispatch({
				type: AuthActionType.LOGOUT_SUCCESS,
			});
			dispatch(resetUser());
		} catch (error: any) {
			dispatch({
				type: AuthActionType.LOGOUT_ERROR,
				payload: { error: getAuthError(error.response.data.type) },
			});
		}
	};
};

export const resetAuthError = () => ({
	type: AuthActionType.RESET_ERROR,
});

const getAuthError = (type: string) => {
	return AuthErrorType[type] || "Ошибка";
};
