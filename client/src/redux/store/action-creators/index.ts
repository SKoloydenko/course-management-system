import * as authActionCreators from "./auth";
import * as userActionCreators from "./user";
import * as universityActionCreators from "./university";
import * as courseActionCreators from "./course";
import * as tagActionCreators from "./tag";
import * as modalActionCreators from "./modal";

const actionCreators = {
	...authActionCreators,
	...userActionCreators,
	...universityActionCreators,
	...courseActionCreators,
	...tagActionCreators,
	...modalActionCreators,
};

export default actionCreators;
