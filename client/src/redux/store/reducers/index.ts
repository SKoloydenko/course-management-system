import { combineReducers } from "redux";
import { authReducer } from "./authReducer";
import { courseReducer } from "./courseReducer";
import { modalReducer } from "./modalReducer";
import { tagReducer } from "./tagReducer";
import { universityReducer } from "./universityReducer";
import { userReducer } from "./userReducer";

export const rootReducer = combineReducers({
	auth: authReducer,
	user: userReducer,
	university: universityReducer,
	course: courseReducer,
	tag: tagReducer,
	modal: modalReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
