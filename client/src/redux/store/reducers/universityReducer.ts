import {
	UniversityAction,
	UniversityActionType,
	UniversityState,
} from "redux/types/universityTypes";

const initialState = {
	universities: [],
	loading: false,
	error: null,
};

export const universityReducer = (
	state: UniversityState = initialState,
	action: UniversityAction
): UniversityState => {
	switch (action.type) {
		case UniversityActionType.GET_UNIVERSITIES:
			return { ...state, loading: true };
		case UniversityActionType.GET_UNIVERSITIES_SUCCESS:
			return {
				...state,
				universities: action.payload.universities,
				loading: false,
			};
		case UniversityActionType.GET_UNIVERSITIES_ERROR:
			return { ...state, error: action.payload.error, loading: false };
		default:
			return state;
	}
};
