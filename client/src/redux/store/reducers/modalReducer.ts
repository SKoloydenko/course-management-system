import {
	ModalAction,
	ModalActionType,
	ModalState,
} from "redux/types/modalTypes";

const initialState = {
	open: false,
};

export const modalReducer = (
	state: ModalState = initialState,
	action: ModalAction
): ModalState => {
	switch (action.type) {
		case ModalActionType.OPEN_MODAL:
			return { open: true };
		case ModalActionType.CLOSE_MODAL:
			return { open: false };
		default:
			return state;
	}
};
