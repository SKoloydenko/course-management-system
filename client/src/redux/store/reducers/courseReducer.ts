import {
	CourseAction,
	CourseActionType,
	CourseState,
	CourseTab,
} from "redux/types/courseTypes";

const initialState = {
	currentCourse: null,
	courses: [],
	tab: CourseTab.GENERAL,
	selectedDate: new Date(),
	loading: false,
	error: null,
};

export const courseReducer = (
	state: CourseState = initialState,
	action: CourseAction
): CourseState => {
	switch (action.type) {
		case CourseActionType.RESET_ERROR:
			return { ...state, error: null };

		case CourseActionType.SET_TAB:
			return { ...state, tab: action.payload.tab };

		case CourseActionType.SET_SELECTED_DATE:
			return { ...state, selectedDate: action.payload.date };

		case CourseActionType.GET_COURSES:
			return { ...state, loading: true };
		case CourseActionType.GET_COURSES_SUCCESS:
			return {
				...state,
				courses: action.payload.courses,
				loading: false,
			};
		case CourseActionType.GET_COURSES_ERROR:
			return { ...state, error: action.payload.error, loading: false };

		case CourseActionType.GET_COURSE:
			return { ...state, loading: true };
		case CourseActionType.GET_COURSE_SUCCESS:
			return {
				...state,
				currentCourse: {
					...action.payload.course,
					lessonsDays: action.payload.course.lessonsDays?.map((day) =>
						day === 7 ? 0 : day
					),
					notes: action.payload.course.notes?.map((note) => ({
						...note,
						date: new Date(note.date),
					})),
					attachments: action.payload.course.attachments?.map((attachment) => ({
						...attachment,
						date: new Date(attachment.date),
					})),
				},
				loading: false,
			};
		case CourseActionType.GET_COURSE_ERROR:
			return { ...state, error: action.payload.error, loading: false };

		case CourseActionType.CREATE_COURSE:
			return { ...state, loading: true };
		case CourseActionType.CREATE_COURSE_SUCCESS:
			return {
				...state,
				courses: [...state.courses, action.payload.course],
				loading: false,
			};
		case CourseActionType.CREATE_COURSE_ERROR:
			return { ...state, error: action.payload.error, loading: false };

		case CourseActionType.APPLY_ON_COURSE:
			return { ...state, loading: true };
		case CourseActionType.APPLY_ON_COURSE_SUCCESS:
			return {
				...state,
				loading: false,
			};
		case CourseActionType.APPLY_ON_COURSE_ERROR:
			return { ...state, error: action.payload.error, loading: false };

		case CourseActionType.DELETE_APPLICATION:
			return { ...state, loading: true };
		case CourseActionType.DELETE_APPLICATION_SUCCESS:
			return {
				...state,
				loading: false,
			};
		case CourseActionType.DELETE_APPLICATION_ERROR:
			return { ...state, error: action.payload.error, loading: false };

		case CourseActionType.CONFIRM_APPLICATION:
			return { ...state, loading: true };
		case CourseActionType.CONFIRM_APPLICATION_SUCCESS:
			return {
				...state,
				loading: false,
			};
		case CourseActionType.CONFIRM_APPLICATION_ERROR:
			return { ...state, error: action.payload.error, loading: false };

		case CourseActionType.REJECT_APPLICATION:
			return { ...state, loading: true };
		case CourseActionType.REJECT_APPLICATION_SUCCESS:
			return {
				...state,
				loading: false,
			};
		case CourseActionType.REJECT_APPLICATION_ERROR:
			return { ...state, error: action.payload.error, loading: false };

		case CourseActionType.DELETE_STUDENT:
			return { ...state, loading: true };
		case CourseActionType.DELETE_STUDENT_SUCCESS:
			return {
				...state,
				loading: false,
			};
		case CourseActionType.DELETE_STUDENT_ERROR:
			return { ...state, error: action.payload.error, loading: false };

		case CourseActionType.DELETE_COURSE:
			return { ...state, loading: true };
		case CourseActionType.DELETE_COURSE_SUCCESS:
			return {
				...state,
				currentCourse: null,
				loading: false,
			};
		case CourseActionType.DELETE_COURSE_ERROR:
			return { ...state, error: action.payload.error, loading: false };

		case CourseActionType.LEAVE_COURSE:
			return { ...state, loading: true };
		case CourseActionType.LEAVE_COURSE_SUCCESS:
			return {
				...state,
				currentCourse: null,
				loading: false,
			};
		case CourseActionType.LEAVE_COURSE_ERROR:
			return { ...state, error: action.payload.error, loading: false };

		case CourseActionType.CREATE_NOTE:
			return {
				...state,
				// loading: true
			};
		case CourseActionType.CREATE_NOTE_SUCCESS:
			return {
				...state,
				currentCourse: state.currentCourse
					? {
							...state.currentCourse,
							notes: action.payload.notes?.map((note) => ({
								...note,
								date: new Date(note.date),
							})),
					  }
					: null,
				// loading: false,
			};
		case CourseActionType.CREATE_NOTE_ERROR:
			return {
				...state,
				error: action.payload.error,
				// loading: false
			};

		case CourseActionType.DELETE_NOTE:
			return {
				...state,
				// loading: true
			};
		case CourseActionType.DELETE_NOTE_SUCCESS:
			return {
				...state,
				currentCourse: state.currentCourse
					? {
							...state.currentCourse,
							notes: state.currentCourse.notes?.filter(
								(note) => note.noteId !== action.payload.noteId
							),
					  }
					: null,
				// loading: false,
			};
		case CourseActionType.DELETE_NOTE_ERROR:
			return {
				...state,
				error: action.payload.error,
				// loading: false
			};

		case CourseActionType.GET_ATTACHMENT:
			return { ...state, loading: true };
		case CourseActionType.GET_ATTACHMENT_SUCCESS:
			return {
				...state,
				loading: false,
			};
		case CourseActionType.GET_ATTACHMENT_ERROR:
			return { ...state, error: action.payload.error, loading: false };

		case CourseActionType.ADD_ATTACHMENT:
			return { ...state, loading: true };
		case CourseActionType.ADD_ATTACHMENT_SUCCESS:
			return {
				...state,
				currentCourse: state.currentCourse
					? {
							...state.currentCourse,
							attachments: action.payload.attachments?.map((attachment) => ({
								...attachment,
								date: new Date(attachment.date),
							})),
					  }
					: null,
				loading: false,
			};
		case CourseActionType.ADD_ATTACHMENT_ERROR:
			return { ...state, error: action.payload.error, loading: false };

		case CourseActionType.DELETE_ATTACHMENT:
			return { ...state, loading: true };
		case CourseActionType.DELETE_ATTACHMENT_SUCCESS:
			return {
				...state,
				currentCourse: state.currentCourse
					? {
							...state.currentCourse,
							attachments: state.currentCourse.attachments?.filter(
								(attachment) =>
									attachment.fileId !== action.payload.attachmentId
							),
					  }
					: null,
				loading: false,
			};
		case CourseActionType.DELETE_ATTACHMENT_ERROR:
			return { ...state, error: action.payload.error, loading: false };
		default:
			return state;
	}
};
