import {
	Tag,
	TagAction,
	TagActionType,
	TagResponse,
	TagState,
} from "redux/types/tagTypes";

const initialState = {
	tags: [],
	loading: false,
	error: null,
};

const tags = [
	{
		title: "Программирование",
		color: "#e53170",
		icon: "programming.png",
	},
	{
		title: "Математика",
		color: "#ff8e3c",
		icon: "maths.png",
	},
	{
		title: "Биология",
		color: "#2cb67d",
		icon: "biology.png",
	},
	{
		title: "Иностранный язык",
		color: "#3da9fc",
		icon: "foreign.png",
	},
	{
		title: "Химия",
		color: "#007EB8",
		icon: "chemistry.png",
	},
	{
		title: "Физика",
		color: "#00C9AB",
		icon: "physics.png",
	},
	{
		title: "Экономика",
		color: "#C95C60",
		icon: "economics.png",
	},
];

const mergeTag = (res: Tag[], tag: TagResponse) => {
	const mergeTag = tags.find((it) => it.title === tag.title);
	if (mergeTag) {
		res.push({
			tagId: tag.tagId,
			title: tag.title,
			color: mergeTag.color,
			icon: mergeTag.icon,
		});
	}
	return res;
};

export const tagReducer = (
	state: TagState = initialState,
	action: TagAction
): TagState => {
	switch (action.type) {
		case TagActionType.GET_TAGS:
			return { ...state, loading: true };
		case TagActionType.GET_TAGS_SUCCESS:
			return {
				...state,
				tags: action.payload.tags.reduce(
					(res: Tag[], tag: TagResponse) => mergeTag(res, tag),
					[]
				),
				loading: false,
			};
		case TagActionType.GET_TAGS_ERROR:
			return { ...state, error: action.payload.error, loading: false };
		default:
			return state;
	}
};
