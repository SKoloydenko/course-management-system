import axios from "axios";

const API_URL = "/api/universities";

const getUniversities = () => {
	return axios.get(`${API_URL}/`);
};

export default { getUniversities };
