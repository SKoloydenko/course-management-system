import axios from "axios";
import { UpdateRequest } from "redux/types/userTypes";

const API_URL = "/api/users";

const getCurrentUser = () => {
	return axios.get(`${API_URL}/`);
};

const getUser = (id: number) => {
	return axios.get(`${API_URL}/${id}`);
};

const updateCurrentUser = (updateRequest: UpdateRequest) => {
	return axios.put(`${API_URL}/`);
};

const deleteCurrentUser = () => {
	return axios.delete(`${API_URL}/`);
};

export default {
	getCurrentUser,
	getUser,
	updateCurrentUser,
	deleteCurrentUser,
};
