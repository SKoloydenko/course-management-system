import axios from "axios";

export interface LoginRequest {
	email: string;
	password: string;
}

export interface RegisterRequest {
	email: string;
	firstname: string;
	lastname: string;
	password: string;
	universityId: number;
}

const API_URL = "/api/auth";

const register = (registerRequest: RegisterRequest) => {
	return axios.post(`${API_URL}/register`, { ...registerRequest });
};

const login = (loginRequest: LoginRequest) => {
	return axios.post(`${API_URL}/login`, { ...loginRequest });
};

const logout = () => {
	return axios.post(`${API_URL}/logout`);
};

export default {
	register,
	login,
	logout,
};
