import axios from "axios";

const API_URL = "/api/tags";

const getTags = () => {
	return axios.get(`${API_URL}/`);
};

export default { getTags };
