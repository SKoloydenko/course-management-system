import axios from "axios";

enum ActivityType {}

export interface CourseCreateRequest {
	title: string;
	open: boolean;
	startingDate: string;
	finishingDate: string;
	lessonsDays: number[];
	tagId: number;
	description: string;
}

export interface NoteCreateRequest {
	text: string;
	date: string;
}

export interface ActivityCreateRequest {
	title: string;
	type: ActivityType;
	description: string;
	startingTime: string;
	finishingTime: string;
	maxGrade: number;
}

const API_URL = "/api/courses";

const getCourses = () => {
	return axios.get(`${API_URL}/`);
};

const getCourse = (courseId: number) => {
	return axios.get(`${API_URL}/${courseId}`);
};

const createCourse = (createCourseRequest: CourseCreateRequest) => {
	return axios.post(`${API_URL}/`, createCourseRequest);
};

const applyOnCourse = (courseId: number) => {
	return axios.post(`${API_URL}/${courseId}/applications/`);
};

const deleteApplicationOnCourse = (courseId: number) => {
	return axios.delete(`${API_URL}/${courseId}/applications/`);
};

const confirmApplicationOnCourse = (courseId: number, userId: number) => {
	return axios.post(`${API_URL}/${courseId}/applications/${userId}`);
};

const rejectApplicationOnCourse = (courseId: number, userId: number) => {
	return axios.delete(`${API_URL}/${courseId}/applications/${userId}`);
};

const deleteCourse = (courseId: number) => {
	return axios.delete(`${API_URL}/${courseId}`);
};

const leaveCourse = (courseId: number) => {
	return axios.delete(`${API_URL}/${courseId}/student/`);
};

const deleteStudentFromCourse = (courseId: number, userId: number) => {
	return axios.delete(`${API_URL}/${courseId}/student/${userId}`);
};

const createNote = (courseId: number, noteCreateRequest: NoteCreateRequest) => {
	return axios.post(`${API_URL}/${courseId}/notes/`, noteCreateRequest);
};

const deleteNote = (courseId: number, noteId: number) => {
	return axios.delete(`${API_URL}/${courseId}/notes/${noteId}`);
};

const getAttachment = (courseId: number, attachmentId: number) => {
	return axios.get(`${API_URL}/${courseId}/attachments/${attachmentId}`);
};

const addAttachment = (courseId: number, file: File, date: string) => {
	const formData = new FormData();
	formData.append("file", file);
	formData.append("date", date);
	return axios.post(`${API_URL}/${courseId}/attachments/`, formData);
};

const deleteAttachment = (courseId: number, attachmentId: number) => {
	return axios.delete(`${API_URL}/${courseId}/attachments/${attachmentId}`);
};

const createCourseActivity = (
	courseId: number,
	activityCreateRequest: ActivityCreateRequest
) => {
	return axios.post(
		`${API_URL}/${courseId}/activities/`,
		activityCreateRequest
	);
};

const deleteCourseActivity = (courseId: number, activityId: number) => {
	return axios.delete(`${API_URL}/${courseId}/activities/${activityId}`);
};

export default {
	getCourses,
	getCourse,
	createCourse,
	applyOnCourse,
	deleteApplicationOnCourse,
	confirmApplicationOnCourse,
	rejectApplicationOnCourse,
	deleteCourse,
	leaveCourse,
	deleteStudentFromCourse,
	createNote,
	deleteNote,
	getAttachment,
	addAttachment,
	deleteAttachment,
};
