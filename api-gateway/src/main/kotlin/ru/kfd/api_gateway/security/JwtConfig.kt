package ru.kfd.api_gateway.security

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration
data class JwtConfig(
    @Value("\${jwt.name}") val name: String,
    @Value("\${jwt.secret}") val secret: String,
)
