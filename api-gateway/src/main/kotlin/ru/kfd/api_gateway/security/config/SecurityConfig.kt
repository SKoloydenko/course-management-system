package ru.kfd.api_gateway.security.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import ru.kfd.api_gateway.security.JwtAuthenticationEntryPoint
import ru.kfd.api_gateway.security.filters.JwtAuthenticationFilter

@EnableWebSecurity
class SecurityConfig
@Autowired
constructor(
    private val authenticationEntryPoint: JwtAuthenticationEntryPoint,
    private val authenticationFilter: JwtAuthenticationFilter,
) : WebSecurityConfigurerAdapter() {

    val allowedUnauthorizedGetEndpoints =
        arrayOf(
            "/api/universities/**",
            "/api/tags/**",
            "/api/courses/",
            "/api/courses/{courseId}",
            "/",
            "/static/**",
            "/*.png"
        )
    val allowedUnauthorizedPostEndpoints = arrayOf("/api/auth/register", "/api/auth/login")

    override fun configure(http: HttpSecurity) {
        http.csrf()
            .disable()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .exceptionHandling()
            .authenticationEntryPoint(authenticationEntryPoint)
            .and()
            .addFilterAfter(authenticationFilter, UsernamePasswordAuthenticationFilter::class.java)
            .authorizeRequests()
            .antMatchers(HttpMethod.GET, *allowedUnauthorizedGetEndpoints).permitAll()
            .antMatchers(HttpMethod.POST, *allowedUnauthorizedPostEndpoints).permitAll()
            .antMatchers("/api/**").authenticated()
            .anyRequest().permitAll()
    }
}
