package ru.kfd.api_gateway.security.filters

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.filter.OncePerRequestFilter
import org.springframework.web.util.WebUtils
import ru.kfd.api_gateway.security.JwtConfig
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtAuthenticationFilter(private val jwtConfig: JwtConfig) : OncePerRequestFilter() {

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        val cookie = WebUtils.getCookie(request, jwtConfig.name)
        if (cookie == null) {
            filterChain.doFilter(request, response)
            return
        }

        val token = cookie.value
        validateToken(token)

        filterChain.doFilter(request, response)
    }

    private fun validateToken(token: String) {
        try {
            val claims: Claims =
                Jwts.parser().setSigningKey(jwtConfig.secret).parseClaimsJws(token).body

            val userId = claims.subject
            if (userId != null) {
                val auth = UsernamePasswordAuthenticationToken(userId, null, mutableListOf())
                SecurityContextHolder.getContext().authentication = auth
            }
        } catch (e: Exception) {
            SecurityContextHolder.clearContext()
        }
    }
}
