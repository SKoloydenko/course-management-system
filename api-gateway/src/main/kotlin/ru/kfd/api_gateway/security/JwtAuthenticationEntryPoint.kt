package ru.kfd.api_gateway.security

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import ru.kfd.api_gateway.constants.UnauthorizedErrorType
import ru.kfd.api_gateway.dto.responses.ErrorResponseDTO
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtAuthenticationEntryPoint(private val objectMapper: ObjectMapper) :
    AuthenticationEntryPoint {

    override fun commence(
        request: HttpServletRequest,
        response: HttpServletResponse,
        authException: AuthenticationException
    ) {
        response.status = HttpStatus.UNAUTHORIZED.value()
        response.contentType = MediaType.APPLICATION_JSON.toString()

        val body =
            ErrorResponseDTO(
                HttpStatus.UNAUTHORIZED.value(),
                UnauthorizedErrorType.UNAUTHORIZED.name,
                authException.message
            )

        response.outputStream.println(objectMapper.writeValueAsString(body))
    }
}
