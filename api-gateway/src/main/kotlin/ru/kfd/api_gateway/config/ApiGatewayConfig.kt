package ru.kfd.api_gateway.config

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.Resource
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.resource.PathResourceResolver
import ru.kfd.api_gateway.security.JwtAuthenticationEntryPoint
import ru.kfd.api_gateway.security.JwtConfig
import ru.kfd.api_gateway.security.filters.JwtAuthenticationFilter
import java.io.IOException
import java.nio.charset.StandardCharsets


@Configuration
class ApiGatewayConfig
@Autowired
constructor(
    private val objectMapper: ObjectMapper,
    private val jwtConfig: JwtConfig,
) : WebMvcConfigurer {

    override fun addResourceHandlers(registry: ResourceHandlerRegistry) {
        val baseApiPath = "/api"
        registry.addResourceHandler("/**/*.css", "/**/*.html", "/**/*.js", "/**/*.jsx", "/**/*.png", "/**/*.ttf", "/**/*.woff", "/**/*.woff2")
            .setCachePeriod(0)
            .addResourceLocations("classpath:/static/")
        registry.addResourceHandler("/", "/**")
            .setCachePeriod(0)
            .addResourceLocations("classpath:/static/index.html")
            .resourceChain(true)
            .addResolver(object : PathResourceResolver() {
                override fun getResource(resourcePath: String, location: Resource): Resource? {
                    if (resourcePath.startsWith(baseApiPath)
                        || resourcePath.startsWith(baseApiPath.substring(1))) {
                        return null
                    }
                    return if (location.exists() && location.isReadable) location else null
                }
            })
    }

    @Bean fun authenticationEntryPoint() = JwtAuthenticationEntryPoint(objectMapper)

    @Bean fun authenticationFilter() = JwtAuthenticationFilter(jwtConfig)

    @Bean
    fun errorAttributes() =
        object : DefaultErrorAttributes() {
            override fun getErrorAttributes(
                webRequest: WebRequest,
                includeStackTrace: Boolean
            ): MutableMap<String, Any> {
                val errorAttributes = super.getErrorAttributes(webRequest, false)
                errorAttributes.remove("timestamp")
                errorAttributes.remove("error")
                errorAttributes.remove("path")
                return errorAttributes
            }
        }

    override fun configureMessageConverters(converters: MutableList<HttpMessageConverter<*>>) {
        converters.filterIsInstance<MappingJackson2HttpMessageConverter>().first().defaultCharset =
            StandardCharsets.UTF_8
    }
}
