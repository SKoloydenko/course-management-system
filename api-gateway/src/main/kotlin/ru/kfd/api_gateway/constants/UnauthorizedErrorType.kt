package ru.kfd.api_gateway.constants

enum class UnauthorizedErrorType {
    UNAUTHORIZED,
}
