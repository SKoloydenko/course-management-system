package ru.kfd.api_gateway.dto.responses

data class ErrorResponseDTO(
    val status: Int,
    val type: String,
    val message: String?,
)
