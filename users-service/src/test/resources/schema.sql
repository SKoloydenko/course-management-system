drop schema if exists "users-test" cascade;
create schema "users-test";

create table universities
(
    id    bigserial primary key,
    title varchar(255) not null unique
);

create table users
(
    id            bigserial primary key,
    email         varchar(255) not null unique,
    firstname     varchar(255) not null,
    lastname      varchar(255) not null,
    password      varchar(255) not null,
    university_id bigint       not null,
    foreign key (university_id) references universities (id)
);
