set search_path to "users-test";

insert into universities
values (1, 'НИЯУ МИФИ');

insert into universities
values (2, 'МФТИ');

insert into users (email, firstname, lastname, password, university_id)
values ('ii@mail.ru', 'Иван', 'Иванов', '$2a$10$v/TjtBg8m6RlV4eswKTB3etTYsq2I.VkyooRjBKTfemRVYY/Bx/7C', 1);