package ru.kfd.users_service

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.springframework.test.web.servlet.*
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.ext.ScriptUtils
import org.testcontainers.jdbc.JdbcDatabaseDelegate
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import ru.kfd.users_service.clients.CoursesServiceClient
import ru.kfd.users_service.constants.ErrorType
import ru.kfd.users_service.constants.NotFoundErrorType
import ru.kfd.users_service.constants.UnauthorizedErrorType
import ru.kfd.users_service.dto.requests.UserRegisterRequestDTO
import ru.kfd.users_service.dto.requests.UserUpdateRequestDTO
import ru.kfd.users_service.dto.requests.UserValidationRequestDTO
import ru.kfd.users_service.dto.responses.*

@Testcontainers
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
internal class UsersServiceIntegrationTest {

    @Autowired lateinit var mockMvc: MockMvc

    @MockkBean lateinit var coursesServiceClient: CoursesServiceClient

    companion object {
        @Container
        val postgreSQLContainer =
            PostgreSQLContainer<Nothing>("postgres:latest").apply {
                withUsername("root")
                withPassword("root")
                withDatabaseName("users-test")
                withUrlParam("currentSchema", "users-test")
            }

        @JvmStatic
        @DynamicPropertySource
        fun properties(registry: DynamicPropertyRegistry) {
            registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl)
            registry.add("spring.datasource.password", postgreSQLContainer::getPassword)
            registry.add("spring.datasource.username", postgreSQLContainer::getUsername)
        }

        const val universityId = 1L

        const val userId0 = 0L
        const val userId1 = 1L
        val userCourses1 =
            UserCoursesResponseDTO(
                appliedCourses = mutableSetOf(),
                attendedCourses = mutableSetOf(),
                supervisedCourses = mutableSetOf(),
                finishedCourses = mutableSetOf(),
            )
        const val userId2 = 2L
    }

    @BeforeEach
    fun beforeEach() {
        val delegate = JdbcDatabaseDelegate(postgreSQLContainer, "")
        ScriptUtils.runInitScript(delegate, "schema.sql")
        ScriptUtils.runInitScript(delegate, "data.sql")
        every { coursesServiceClient.getUserCourses(userId1) } returns userCourses1
        every { coursesServiceClient.createUserCourses(any()) } returns userCourses1
        every { coursesServiceClient.deleteUserCourses(userId1) } answers {}
    }

    @Nested
    inner class GetCurrentUser {

        @Test
        @WithMockUser(userId1.toString())
        fun `when user exists then get user`() {
            // given
            val userId = userId1
            val response =
                CurrentUserResponseDTO(
                    userId = userId,
                    email = "ii@mail.ru",
                    firstname = "Иван",
                    lastname = "Иванов",
                    universityId = 1,
                    userCourses = userCourses1,
                )
            // when
            mockMvc.get("/api/users/")
                // then
                .andExpect {
                    status { isOk }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        string(jacksonObjectMapper().writeValueAsString(response))
                    }
                }
        }

        @Test
        @WithMockUser(userId0.toString())
        fun `when user does not exist then not found`() {
            // given
            val response =
                ErrorResponseDTO(
                    status = HttpStatus.NOT_FOUND.value(),
                    type = NotFoundErrorType.USER_DOES_NOT_EXIST.name,
                    message = "User with this id does not exist"
                )
            // when
            mockMvc.get("/api/users/")
                // then
                .andExpect {
                    status { isNotFound }
                    content { string(jacksonObjectMapper().writeValueAsString(response)) }
                }
        }
    }

    @Nested
    inner class GetUser {

        @Test
        fun `when user exists then get user`() {
            // given
            val userId = userId1
            val response =
                UserResponseDTO(
                    firstname = "Иван",
                    lastname = "Иванов",
                    universityId = 1,
                )
            // when
            mockMvc.get("/api/users/$userId")
                // then
                .andExpect {
                    status { isOk }
                    content { string(jacksonObjectMapper().writeValueAsString(response)) }
                }
        }

        @Test
        fun `when user does not exist then not found`() {
            // given
            val userId = 0
            val response =
                ErrorResponseDTO(
                    status = HttpStatus.NOT_FOUND.value(),
                    type = NotFoundErrorType.USER_DOES_NOT_EXIST.name,
                    message = "User with this id does not exist"
                )
            // when
            mockMvc.get("/api/users/$userId")
                // then
                .andExpect {
                    status { isNotFound }
                    content { string(jacksonObjectMapper().writeValueAsString(response)) }
                }
        }
    }

    @Nested
    inner class ValidateUser {

        @Test
        fun `when correct credentials then valid`() {
            // given
            val userId = userId1
            val request =
                UserValidationRequestDTO(
                    email = "ii@mail.ru",
                    password = "12345678",
                )
            val response = UserValidationResponseDTO(userId)
            // when
            mockMvc
                .post("/api/users/validation/") {
                    contentType = MediaType.APPLICATION_JSON
                    content = jacksonObjectMapper().writeValueAsString(request)
                    accept = MediaType.APPLICATION_JSON
                }
                // then
                .andExpect {
                    status { isOk }
                    content { string(jacksonObjectMapper().writeValueAsString(response)) }
                }
        }

        @Test
        fun `when incorrect credentials then unauthorized`() {
            // given
            val request =
                UserValidationRequestDTO(
                    email = "ii@mail.ru",
                    password = "87654321",
                )
            val response =
                ErrorResponseDTO(
                    status = HttpStatus.UNAUTHORIZED.value(),
                    type = UnauthorizedErrorType.UNAUTHORIZED.name,
                    message = "Invalid credentials",
                )
            // when
            mockMvc
                .post("/api/users/validation/") {
                    contentType = MediaType.APPLICATION_JSON
                    content = jacksonObjectMapper().writeValueAsString(request)
                    accept = MediaType.APPLICATION_JSON
                }
                // then
                .andExpect {
                    status { isUnauthorized }
                    content { string(jacksonObjectMapper().writeValueAsString(response)) }
                }
        }
    }

    @Nested
    inner class RegisterUser {

        @Test
        fun `when user is unique then register user`() {
            // given
            val userId = userId2
            val request =
                UserRegisterRequestDTO(
                    email = "pp@mail.ru",
                    password = "12345678",
                    firstname = "Петр",
                    lastname = "Петров",
                    universityId = universityId,
                )
            val response =
                CurrentUserResponseDTO(
                    userId = userId,
                    email = "pp@mail.ru",
                    firstname = "Петр",
                    lastname = "Петров",
                    universityId = 1,
                    userCourses = userCourses1,
                )
            // when
            mockMvc
                .post("/api/users/") {
                    contentType = MediaType.APPLICATION_JSON
                    content = jacksonObjectMapper().writeValueAsString(request)
                    accept = MediaType.APPLICATION_JSON
                }
                // then
                .andExpect {
                    status { isCreated }
                    content { string(jacksonObjectMapper().writeValueAsString(response)) }
                }
        }

        @Test
        fun `when user is not unique then bad request`() {
            // given
            val request =
                UserRegisterRequestDTO(
                    email = "ii@mail.ru",
                    password = "12345678",
                    firstname = "Иван",
                    lastname = "Иванов",
                    universityId = universityId,
                )
            val response =
                ErrorResponseDTO(
                    status = HttpStatus.BAD_REQUEST.value(),
                    type = ErrorType.USER_ALREADY_EXISTS.name,
                    message = "User with this email already exists"
                )
            // when
            mockMvc
                .post("/api/users/") {
                    contentType = MediaType.APPLICATION_JSON
                    content = jacksonObjectMapper().writeValueAsString(request)
                    accept = MediaType.APPLICATION_JSON
                }
                // then
                .andExpect {
                    status { isBadRequest }
                    content { string(jacksonObjectMapper().writeValueAsString(response)) }
                }
        }
    }

    @Nested
    inner class UpdateUser {

        @Test
        fun `when user exists then update user`() {
            // given
            val userId = userId1
            val request =
                UserUpdateRequestDTO(
                    email = "ii@mail.ru",
                    password = "12345678",
                    firstname = "Петр",
                    lastname = "Иванов",
                    universityId = universityId,
                )
            val response =
                UserUpdateResponseDTO(
                    email = "ii@mail.ru",
                    firstname = "Петр",
                    lastname = "Иванов",
                    university = "НИЯУ МИФИ",
                )
            // when
            mockMvc
                .put("/api/users/$userId") {
                    contentType = MediaType.APPLICATION_JSON
                    content = jacksonObjectMapper().writeValueAsString(request)
                    accept = MediaType.APPLICATION_JSON
                }
                // then
                .andExpect {
                    status { isOk }
                    content { string(jacksonObjectMapper().writeValueAsString(response)) }
                }
        }

        @Test
        fun `when user does not exist then not found`() {
            // given
            val userId = 0
            val request =
                UserUpdateRequestDTO(
                    email = "ii@mail.ru",
                    password = "12345678",
                    firstname = "Иван",
                    lastname = "Иванов",
                    universityId = universityId,
                )
            val response =
                ErrorResponseDTO(
                    status = HttpStatus.NOT_FOUND.value(),
                    type = NotFoundErrorType.USER_DOES_NOT_EXIST.name,
                    message = "User with this id does not exist"
                )
            // when
            mockMvc
                .put("/api/users/$userId") {
                    contentType = MediaType.APPLICATION_JSON
                    content = jacksonObjectMapper().writeValueAsString(request)
                    accept = MediaType.APPLICATION_JSON
                }
                // then
                .andExpect {
                    status { isNotFound }
                    content { string(jacksonObjectMapper().writeValueAsString(response)) }
                }
        }
    }

    @Nested
    inner class DeleteUser {

        @Test
        fun `when user exists then delete user`() {
            // given
            val userId = userId1
            // when
            mockMvc.delete("/api/users/$userId")
                // then
                .andExpect { status { isOk } }
        }

        @Test
        fun `when user does not exist then not found`() {
            // given
            val userId = 0
            val response =
                ErrorResponseDTO(
                    status = HttpStatus.NOT_FOUND.value(),
                    type = NotFoundErrorType.USER_DOES_NOT_EXIST.name,
                    message = "User with this id does not exist"
                )
            // when
            mockMvc.delete("/api/users/$userId")
                // then
                .andExpect {
                    status { isNotFound }
                    content { string(jacksonObjectMapper().writeValueAsString(response)) }
                }
        }
    }

    @Nested
    inner class GetUniversities {

        @Test
        fun `when user is correct then get universities`() {
            // given
            val response =
                listOf(
                    UniversityResponseDTO(1, "НИЯУ МИФИ"),
                    UniversityResponseDTO(2, "МФТИ"),
                )
            // when
            mockMvc.get("/api/universities/")
                // then
                .andExpect {
                    status { isOk }
                    content { string(jacksonObjectMapper().writeValueAsString(response)) }
                }
        }
    }
}
