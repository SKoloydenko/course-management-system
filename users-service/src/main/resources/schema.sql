drop database if exists users;
create database users;

\c users;

drop schema if exists users;
create schema users;

set search_path to users;

create table universities
(
    id    bigserial primary key,
    title varchar(255) not null unique
);

create table users
(
    id            bigserial primary key,
    email         varchar(255) not null unique,
    firstname     varchar(255) not null,
    lastname      varchar(255) not null,
    password      varchar(255) not null,
    university_id bigint       not null,
    foreign key (university_id) references universities (id)
);
