\c users;

set search_path to users;

insert into universities (title)
values ('НИЯУ МИФИ');

insert into universities (title)
values ('МФТИ');

insert into universities (title)
values ('НИУ МЭИ');

insert into users (email, firstname, lastname, password, university_id)
values ('ia@mail.ru', 'Иван', 'Антонов', '$2a$10$v/TjtBg8m6RlV4eswKTB3etTYsq2I.VkyooRjBKTfemRVYY/Bx/7C', 1);

insert into users (email, firstname, lastname, password, university_id)
values ('ai@mail.ru', 'Антон', 'Иванов', '$2a$10$v/TjtBg8m6RlV4eswKTB3etTYsq2I.VkyooRjBKTfemRVYY/Bx/7C', 2);

insert into users (email, firstname, lastname, password, university_id)
values ('ns@mail.ru', 'Николай', 'Скворцов', '$2a$10$v/TjtBg8m6RlV4eswKTB3etTYsq2I.VkyooRjBKTfemRVYY/Bx/7C', 3);

insert into users (email, firstname, lastname, password, university_id)
values ('petrov@mail.ru', 'Даниил', 'Петров', '$2a$10$v/TjtBg8m6RlV4eswKTB3etTYsq2I.VkyooRjBKTfemRVYY/Bx/7C', 2);

insert into users (email, firstname, lastname, password, university_id)
values ('voron@mail.ru', 'Михаил', 'Воронов', '$2a$10$v/TjtBg8m6RlV4eswKTB3etTYsq2I.VkyooRjBKTfemRVYY/Bx/7C', 1);

insert into users (email, firstname, lastname, password, university_id)
values ('dgr@mail.ru', 'Денис', 'Гришин', '$2a$10$v/TjtBg8m6RlV4eswKTB3etTYsq2I.VkyooRjBKTfemRVYY/Bx/7C', 3);

insert into users (email, firstname, lastname, password, university_id)
values ('bgmt@mail.ru', 'Богдан', 'Матвеев', '$2a$10$v/TjtBg8m6RlV4eswKTB3etTYsq2I.VkyooRjBKTfemRVYY/Bx/7C', 1);
