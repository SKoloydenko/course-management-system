package ru.kfd.users_service.dto.responses

data class UserValidationResponseDTO(
    val id: Long,
)
