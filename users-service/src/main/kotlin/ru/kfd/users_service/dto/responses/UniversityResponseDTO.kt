package ru.kfd.users_service.dto.responses

import ru.kfd.users_service.models.University

data class UniversityResponseDTO(
    val universityId: Long,
    val title: String,
) {

    companion object {
        fun toDTO(university: University) =
            UniversityResponseDTO(
                universityId = university.id as Long,
                title = university.title,
            )
    }
}
