package ru.kfd.users_service.dto.responses

import ru.kfd.users_service.models.User

data class UserResponseDTO(
    val firstname: String,
    val lastname: String,
    val universityId: Long,
) {

    companion object {
        fun toDTO(user: User) =
            UserResponseDTO(
                firstname = user.firstname,
                lastname = user.lastname,
                universityId = user.university.id as Long,
            )
    }
}
