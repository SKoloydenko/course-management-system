package ru.kfd.users_service.dto.responses

data class UserCoursesResponseDTO(
    val appliedCourses: Collection<CourseResponseDTO>,
    val attendedCourses: Collection<CourseResponseDTO>,
    val supervisedCourses: Collection<CourseResponseDTO>,
    val finishedCourses: Collection<CourseResponseDTO>,
)
