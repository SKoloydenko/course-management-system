package ru.kfd.users_service.dto.responses

import ru.kfd.users_service.models.User

data class UserUpdateResponseDTO(
    val email: String,
    val firstname: String,
    val lastname: String,
    val university: String,
) {
    companion object {
        fun toDTO(user: User) =
            UserUpdateResponseDTO(
                email = user.email,
                firstname = user.firstname,
                lastname = user.lastname,
                university = user.university.title,
            )
    }
}
