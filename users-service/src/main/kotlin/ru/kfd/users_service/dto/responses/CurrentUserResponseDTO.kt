package ru.kfd.users_service.dto.responses

import ru.kfd.users_service.models.User

data class CurrentUserResponseDTO(
    val userId: Long,
    val email: String,
    val firstname: String,
    val lastname: String,
    val universityId: Long,
    val userCourses: UserCoursesResponseDTO,
) {
    companion object {
        fun toDTO(user: User, userCourses: UserCoursesResponseDTO) =
            CurrentUserResponseDTO(
                userId = user.id as Long,
                email = user.email,
                firstname = user.firstname,
                lastname = user.lastname,
                universityId = user.university.id as Long,
                userCourses = userCourses,
            )
    }
}
