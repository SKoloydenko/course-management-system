package ru.kfd.users_service.dto.responses

data class CourseResponseDTO(
    val courseId: Long,
    val title: String,
    val supervisor: UserResponseDTO,
    val startingDate: String,
    val finishingDate: String,
    val lessonsDays: Collection<String>,
    val open: Boolean,
    val tagId: Long,
    val description: String,
)
