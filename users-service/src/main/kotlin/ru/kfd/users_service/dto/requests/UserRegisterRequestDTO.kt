package ru.kfd.users_service.dto.requests

import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size
import ru.kfd.users_service.models.University
import ru.kfd.users_service.models.User

data class UserRegisterRequestDTO(
    @field:Email(
        regexp = "^[a-zA-Z0-9_!#\$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+\$",
        message = "Email is incorrect"
    )
    val email: String,
    @field:Size(min = 8, max = 64, message = "Password length must be between 8 and 64 symbols")
    val password: String,
    @field:NotEmpty(message = "Firstname must not be empty") val firstname: String,
    @field:NotEmpty(message = "Lastname must not be empty") val lastname: String,
    @field:NotNull(message = "University must not be empty") val universityId: Long?,
) {
    companion object {
        fun toEntity(userDTO: UserRegisterRequestDTO, university: University): User {
            return User(
                email = userDTO.email,
                password = userDTO.password,
                firstname = userDTO.firstname,
                lastname = userDTO.lastname,
                university = university,
            )
        }
    }
}
