package ru.kfd.users_service.models

import javax.persistence.*

@Entity
@Table(name = "universities")
class University(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
    @Column(unique = true) val title: String,
)
