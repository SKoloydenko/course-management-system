package ru.kfd.users_service.models

import javax.persistence.*

@Entity
@Table(name = "users")
class User(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
    @Column(unique = true) val email: String,
    var password: String,
    var firstname: String,
    var lastname: String,
    @OneToOne @JoinColumn(name = "university_id") var university: University,
) {

    fun update(user: User) {
        password = user.password
        firstname = user.firstname
        lastname = user.lastname
        university = user.university
    }
}
