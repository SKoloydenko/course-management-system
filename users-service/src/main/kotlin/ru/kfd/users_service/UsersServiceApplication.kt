package ru.kfd.users_service

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient

@EnableEurekaClient @SpringBootApplication class UsersServiceApplication

fun main(args: Array<String>) {
    runApplication<UsersServiceApplication>(*args)
}
