package ru.kfd.users_service.constants

enum class ErrorType {
    USER_ALREADY_EXISTS,
    INVALID_FIELD,
}
