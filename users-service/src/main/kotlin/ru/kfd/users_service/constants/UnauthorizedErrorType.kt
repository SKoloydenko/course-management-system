package ru.kfd.users_service.constants

enum class UnauthorizedErrorType {
    UNAUTHORIZED,
}
