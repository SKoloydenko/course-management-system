package ru.kfd.users_service.constants

enum class NotFoundErrorType {
    USER_DOES_NOT_EXIST,
    UNIVERSITY_DOES_NOT_EXIST,
}
