package ru.kfd.users_service.repositories

import org.springframework.data.jpa.repository.JpaRepository
import ru.kfd.users_service.models.University

interface UniversityRepository : JpaRepository<University, Long>
