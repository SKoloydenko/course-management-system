package ru.kfd.users_service.controllers

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.client.HttpStatusCodeException
import ru.kfd.users_service.constants.ErrorType
import ru.kfd.users_service.constants.UnauthorizedErrorType
import ru.kfd.users_service.dto.responses.ErrorResponseDTO
import ru.kfd.users_service.exceptions.DomainException
import ru.kfd.users_service.exceptions.NotFoundException

@RestControllerAdvice
class UserControllerAdvice {

    @ExceptionHandler(DomainException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleDomainException(ex: DomainException) =
        ErrorResponseDTO(HttpStatus.BAD_REQUEST.value(), ex.type.name, ex.message)

    @ExceptionHandler(NotFoundException::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun handleNotFoundException(ex: NotFoundException) =
        ErrorResponseDTO(HttpStatus.NOT_FOUND.value(), ex.type.name, ex.message)

    @ExceptionHandler(BadCredentialsException::class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    fun handleBadCredentialsException(ex: BadCredentialsException) =
        ErrorResponseDTO(
            HttpStatus.UNAUTHORIZED.value(),
            UnauthorizedErrorType.UNAUTHORIZED.name,
            ex.message
        )

    @ExceptionHandler(MethodArgumentNotValidException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleArgumentNotValidException(ex: MethodArgumentNotValidException): ErrorResponseDTO {
        val message = ex.bindingResult.fieldErrors.map { it.defaultMessage }[0].toString()
        return ErrorResponseDTO(
            HttpStatus.BAD_REQUEST.value(),
            ErrorType.INVALID_FIELD.name,
            message
        )
    }

    @ExceptionHandler(HttpStatusCodeException::class)
    fun handleHttpStatusCodeException(ex: HttpStatusCodeException): ResponseEntity<Any> {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        return ResponseEntity.status(ex.statusCode).headers(headers).body(ex.responseBodyAsString)
    }
}
