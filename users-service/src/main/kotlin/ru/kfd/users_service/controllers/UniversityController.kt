package ru.kfd.users_service.controllers

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import ru.kfd.users_service.services.UniversityService

@RestController
@RequestMapping("/api/universities")
class UniversityController
@Autowired
constructor(
    private val universityService: UniversityService,
) {

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    fun getUniversities() = universityService.getUniversities()
}
