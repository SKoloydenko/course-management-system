package ru.kfd.users_service.controllers

import java.security.Principal
import javax.validation.Valid
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import ru.kfd.users_service.dto.requests.UserRegisterRequestDTO
import ru.kfd.users_service.dto.requests.UserUpdateRequestDTO
import ru.kfd.users_service.dto.requests.UserValidationRequestDTO
import ru.kfd.users_service.services.UserService

@RestController
@RequestMapping("/api/users")
class UserController
@Autowired
constructor(
    private val userService: UserService,
) {

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    fun getCurrentUser(principal: Principal) = userService.getCurrentUser(principal.name.toLong())

    @GetMapping("/{userId}")
    @ResponseStatus(HttpStatus.OK)
    fun getUserById(@PathVariable userId: Long) = userService.getUserById(userId)

    @PostMapping("/validation/")
    @ResponseStatus(HttpStatus.OK)
    fun validateCredentials(@Valid @RequestBody userCredentials: UserValidationRequestDTO) =
        userService.validateUser(userCredentials)

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    fun registerUser(@Valid @RequestBody user: UserRegisterRequestDTO) =
        userService.registerUser(user)

    @PutMapping("/")
    @ResponseStatus(HttpStatus.OK)
    fun updateCurrentUser(principal: Principal, @Valid @RequestBody user: UserUpdateRequestDTO) =
        userService.updateUser(principal.name.toLong(), user)

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun updateUserById(@PathVariable id: Long, @Valid @RequestBody user: UserUpdateRequestDTO) =
        userService.updateUser(id, user)

    @DeleteMapping("/")
    @ResponseStatus(HttpStatus.OK)
    fun deleteCurrentUser(principal: Principal) = userService.deleteUser(principal.name.toLong())

    @DeleteMapping("/{userId}")
    @ResponseStatus(HttpStatus.OK)
    fun deleteUserById(@PathVariable userId: Long) = userService.deleteUser(userId)
}
