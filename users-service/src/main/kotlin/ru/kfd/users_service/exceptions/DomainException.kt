package ru.kfd.users_service.exceptions

import ru.kfd.users_service.constants.ErrorType

class DomainException(val type: ErrorType, message: String) : RuntimeException(message)
