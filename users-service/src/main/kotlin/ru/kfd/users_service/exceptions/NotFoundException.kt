package ru.kfd.users_service.exceptions

import ru.kfd.users_service.constants.NotFoundErrorType

class NotFoundException(val type: NotFoundErrorType, message: String) : RuntimeException(message)
