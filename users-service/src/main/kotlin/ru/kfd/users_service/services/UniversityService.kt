package ru.kfd.users_service.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.kfd.users_service.dto.responses.UniversityResponseDTO
import ru.kfd.users_service.repositories.UniversityRepository

@Service
class UniversityService
@Autowired
constructor(
    private val universityRepository: UniversityRepository,
) {

    fun getUniversities(): Collection<UniversityResponseDTO> {
        val universities = universityRepository.findAll()
        return universities.map { UniversityResponseDTO.toDTO(it) }
    }
}
