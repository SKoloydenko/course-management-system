package ru.kfd.users_service.services

import javax.transaction.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import ru.kfd.users_service.clients.CoursesServiceClient
import ru.kfd.users_service.constants.ErrorType
import ru.kfd.users_service.constants.NotFoundErrorType
import ru.kfd.users_service.dto.requests.UserRegisterRequestDTO
import ru.kfd.users_service.dto.requests.UserUpdateRequestDTO
import ru.kfd.users_service.dto.requests.UserValidationRequestDTO
import ru.kfd.users_service.dto.responses.CurrentUserResponseDTO
import ru.kfd.users_service.dto.responses.UserResponseDTO
import ru.kfd.users_service.dto.responses.UserUpdateResponseDTO
import ru.kfd.users_service.dto.responses.UserValidationResponseDTO
import ru.kfd.users_service.exceptions.DomainException
import ru.kfd.users_service.exceptions.NotFoundException
import ru.kfd.users_service.repositories.UniversityRepository
import ru.kfd.users_service.repositories.UserRepository

@Service
class UserService
@Autowired
constructor(
    private val userRepository: UserRepository,
    private val universityRepository: UniversityRepository,
    private val passwordEncoder: PasswordEncoder,
    private val coursesServiceClient: CoursesServiceClient,
) {

    private fun findUser(userId: Long) =
        userRepository.findById(userId).orElseThrow {
            NotFoundException(
                NotFoundErrorType.USER_DOES_NOT_EXIST,
                "User with this id does not exist"
            )
        }

    private fun findUniversity(universityId: Long) =
        universityRepository.findById(universityId).orElseThrow {
            NotFoundException(
                NotFoundErrorType.UNIVERSITY_DOES_NOT_EXIST,
                "University with this id does not exist"
            )
        }

    fun getCurrentUser(userId: Long): CurrentUserResponseDTO {
        val user = findUser(userId)

        // rest call
        val userCourses = coursesServiceClient.getUserCourses(user.id as Long)
        return CurrentUserResponseDTO.toDTO(user, userCourses)
    }

    fun getUserById(userId: Long): UserResponseDTO {
        val user = findUser(userId)
        return UserResponseDTO.toDTO(user)
    }

    fun validateUser(userCredentials: UserValidationRequestDTO): UserValidationResponseDTO {
        val email = userCredentials.email
        val password = userCredentials.password

        val user =
            userRepository.findByEmail(email).orElseThrow {
                NotFoundException(
                    NotFoundErrorType.USER_DOES_NOT_EXIST,
                    "User with this email does not exist"
                )
            }

        if (!passwordEncoder.matches(password, user.password)) {
            throw BadCredentialsException("Invalid credentials")
        }
        return UserValidationResponseDTO(user.id as Long)
    }

    @Transactional
    fun registerUser(userDTO: UserRegisterRequestDTO): CurrentUserResponseDTO {
        val university = findUniversity(userDTO.universityId as Long)
        val applicant = UserRegisterRequestDTO.toEntity(userDTO, university)

        if (userRepository.findByEmail(applicant.email).isPresent) {
            throw DomainException(
                ErrorType.USER_ALREADY_EXISTS,
                "User with this email already exists"
            )
        }

        applicant.password = passwordEncoder.encode(applicant.password)
        val user = userRepository.save(applicant)

        // rest call
        val userCourses = coursesServiceClient.createUserCourses(user.id as Long)

        return CurrentUserResponseDTO.toDTO(user, userCourses)
    }

    @Transactional
    fun updateUser(userId: Long, userDTO: UserUpdateRequestDTO): UserUpdateResponseDTO {
        val university = findUniversity(userDTO.universityId)
        val updatedUser = UserUpdateRequestDTO.toEntity(userDTO, university)
        val user = findUser(userId)
        user.update(updatedUser)
        return UserUpdateResponseDTO.toDTO(user)
    }

    @Transactional
    fun deleteUser(userId: Long) {
        val user = findUser(userId)
        userRepository.delete(user)

        // rest call
        coursesServiceClient.deleteUserCourses(user.id as Long)
    }
}
