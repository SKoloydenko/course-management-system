package ru.kfd.users_service.clients

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.client.*
import ru.kfd.users_service.dto.responses.UserCoursesResponseDTO

@Component
class CoursesServiceClient @Autowired constructor(private val restTemplate: RestTemplate) {

    fun getUserCourses(userId: Long): UserCoursesResponseDTO =
        restTemplate.getForObject("http://COURSES-SERVICE/api/courses/users/$userId")

    fun createUserCourses(userId: Long): UserCoursesResponseDTO =
        restTemplate.postForObject("http://COURSES-SERVICE/api/courses/users/$userId")

    fun deleteUserCourses(userId: Long) =
        restTemplate.delete("http://COURSES-SERVICE/api/courses/users/$userId")
}
